#!/bin/bash
# Shell wrapper that calls wifi-radar using sudo
PATH=/sbin:/usr/sbin:/usr/local/sbin:$PATH
sudo wifi-radar $*
