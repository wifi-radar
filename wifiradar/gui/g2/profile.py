#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   gui/g2/profile.py - profile editor, etc. for PyGTK UI
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import gtk
import logging
import re

from wifiradar.misc import _

# create a logger
logger = logging.getLogger(__name__)


class ProfileEditor(gtk.Dialog, object):
    """ Edit and return an AP profile.
    """
    def __init__(self, parent, profile):
        """ Create a new :class:`ProfileEditor`, with :data:`parent` window,
            to edit :data:`profile`.
        """
        gtk.Dialog.__init__(self, _('WiFi Profile'), parent,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            (gtk.STOCK_CANCEL, False, gtk.STOCK_SAVE, True))

        self.profile = profile.copy()
        self.WIFI_MODES = [_('auto'), _('Managed'), _('Ad-Hoc'), _('Master'),
                           _('Repeater'), _('Secondary'), _('Monitor')]
        self.WEP_MODE = [_('open'), _('restricted')]
        self.WIFI_CHANNELS = [_('auto'), '1', '2', '3', '4', '5', '6', '7',
                              '8', '9', '10', '11', '12', '13', '14']

        self.icon = gtk.gdk.pixbuf_new_from_file('pixmaps/wifi-radar.png')
        self.set_icon(self.icon)
        self.set_resizable(False)
        #self.set_size_request(400, 400)
        #################
        # build everything in a tabbed notebook
        self.profile_notebook = gtk.Notebook()

        self.general_table = gtk.Table()
        self.general_table.set_row_spacings(3)
        self.general_table.set_col_spacings(3)
        # The essid labels
        #                        (widget,                       l, r, t, b, xopt,                yopt, xpad, ypad)
        self.general_table.attach(gtk.Label(_('Network Name')), 0, 1, 0, 1, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        # The essid textboxes
        self.essid_entry = gtk.Entry(32)
        self.essid_entry.set_text(self.profile['essid'])
        self.general_table.attach(self.essid_entry,             1, 2, 0, 1, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        # Add the essid table to the dialog
        self.essid_entry.set_tooltip_text(_('The name of the network with '
            'which to connect.'))

        # The bssid labels
        self.general_table.attach(gtk.Label(_('Network Address')), 0, 1, 1, 2, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        # The bssid textboxes
        self.bssid_entry = gtk.Entry(32)
        self.bssid_entry.set_text(self.profile['bssid'])
        self.bssid_entry.set_sensitive(not self.profile['roaming'])
        # Add the bssid table to the dialog
        self.general_table.attach(self.bssid_entry,             1, 2, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.bssid_entry.set_tooltip_text(_('The address of the network '
            'with which to connect.'))
        # Add the roaming checkbox
        self.roaming_cb = gtk.CheckButton(_('Roaming'))
        self.roaming_cb.set_active(self.profile['roaming'])
        self.roaming_cb.connect('toggled', self.toggle_roaming)
        self.general_table.attach(self.roaming_cb,              1, 2, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.roaming_cb.set_tooltip_text(_('Use the AP in this network '
            'which provides strongest signal?'))

        self.general_table.attach(gtk.Label(_('Mode')), 0, 1, 3, 4, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.mode_combo = gtk.combo_box_new_text()
        for mode in self.WIFI_MODES:
            self.mode_combo.append_text(mode)
        try:
            self.mode_combo.set_active(self.WIFI_MODES.index(self.profile['mode']))
        except ValueError:
            # If the preferred mode is not recognized,
            #   do not set an active item.
            self.mode_combo.set_active(-1)
        self.general_table.attach(self.mode_combo,              1, 2, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.mode_combo.set_tooltip_text(_('Method to use for connection.  '
            'You probably want auto mode.'))

        self.general_table.attach(gtk.Label(_('Channel')), 0, 1, 4, 5, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.channel_combo = gtk.combo_box_new_text()
        for channel in self.WIFI_CHANNELS:
            self.channel_combo.append_text(channel)
        try:
            self.channel_combo.set_active(self.WIFI_CHANNELS.index(self.profile['channel']))
        except ValueError:
            # If the preferred channel is not recognized,
            #   do not set an active item.
            self.channel_combo.set_active(-1)
        self.general_table.attach(self.channel_combo,           1, 2, 4, 5, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.channel_combo.set_tooltip_text(_('Channel the network uses.  '
            'You probably want auto mode.'))

        self.general_table.attach(gtk.HSeparator(),             0, 2, 5, 6, gtk.FILL|gtk.EXPAND, 0,    5,    10)

        self.security_setting = gtk.Label(_('Security: {SEC}').format(
            SEC=self.profile['security'].upper()))
        self.general_table.attach(self.security_setting,  0, 1, 6, 7, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        if self.profile['use_dhcp']:
            self.ip_config_setting = gtk.Label(_('IP Configuration: DHCP'))
        else:
            self.ip_config_setting = gtk.Label(_('IP Configuration: Manual'))
        self.general_table.attach(self.ip_config_setting, 1, 2, 6, 7, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.profile_notebook.append_page(self.general_table, gtk.Label(_('General')))

        # create the WiFi security page
        self.security_table = gtk.Table()
        self.security_table.set_row_spacings(3)
        self.security_table.set_col_spacings(3)

        self.security_radio_none = gtk.RadioButton(label=_('None'))
        self.security_radio_wep = gtk.RadioButton(self.security_radio_none, label=_('WEP'))
        self.security_radio_wpa = gtk.RadioButton(self.security_radio_none, label=_('WPA'))
        self.security_radio_none.connect('toggled', self.toggle_security)
        self.security_radio_wep.connect('toggled', self.toggle_security)
        self.security_radio_wpa.connect('toggled', self.toggle_security)
        if self.profile['security'] == 'none':
            self.security_radio_none.set_active(True)
        if self.profile['security'] == 'wep':
            self.security_radio_wep.set_active(True)
        if self.profile['security'] == 'wpa':
            self.security_radio_wpa.set_active(True)
        #                         (widget,                   l, r, t, b, xopt,                yopt, xpad, ypad)
        self.security_table.attach(self.security_radio_none, 0, 1, 0, 1, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.security_table.attach(self.security_radio_wep,  0, 1, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)

        # ToggleButton to select ASCII/hex WEP key type
        self.wep_key_cb = gtk.CheckButton(_('Hex Key'))
        if self.profile['key'].startswith('s:'):
            self.wep_key_cb.set_active(False)
            #self.wep_key_cb.set_label(_('ASCII Key'))
            self.profile['key'] = self.profile['key'][2:]
        else:
            self.wep_key_cb.set_active(True)
        self.security_table.attach(self.wep_key_cb,          1, 2, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        #self.wep_key_cb.connect('toggled', self.toggle_wep_key_type)
        self.key_entry = gtk.Entry(64)
        self.key_entry.set_text(self.profile['key'])
        self.security_table.attach(self.key_entry,           2, 3, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.key_entry.set_tooltip_text(_('WEP key: Plain language or hex '
            'string to use for encrypted communication with the network.'))

        self.wep_mode_label = gtk.Label(_('WEP Mode'))
        self.wep_mode_label.set_alignment(1, 0.5)
        self.security_table.attach(self.wep_mode_label,      1, 2, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.wep_mode_combo = gtk.combo_box_new_text()
        for security in self.WEP_MODE:
            self.wep_mode_combo.append_text(security)
        try:
            self.wep_mode_combo.set_active(self.WEP_MODE.index(self.profile['security']))
        except ValueError:
            # If the preferred security mode is not recognized,
            #   do not set an active item.
            self.wep_mode_combo.set_active(-1)
        self.security_table.attach(self.wep_mode_combo,      2, 3, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.wep_mode_combo.set_tooltip_text(_('Use Open to allow '
            'unencrypted communication and Restricted to force '
            'encrypted-only communication.'))

        self.security_table.attach(self.security_radio_wpa,  0, 1, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        # The labels
        self.psk_label = gtk.Label(_('PSK'))
        self.psk_label.set_alignment(1, 0.5)
        self.security_table.attach(self.psk_label,           1, 2, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        # The text boxes
        self.wpa_psk_entry = gtk.Entry()
        self.wpa_psk_entry.set_text(self.profile['wpa_psk'])
        self.security_table.attach(self.wpa_psk_entry,       2, 3, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.profile_notebook.append_page(self.security_table, gtk.Label(_('Security')))

        # create the network config page
        self.net_config_page = gtk.VBox()
        # DHCP toggle
        self.dhcp_cb = gtk.CheckButton(_('Automatic Network Configuration '
            '(DHCP)'))
        self.dhcp_cb.set_active(self.profile['use_dhcp'])
        self.dhcp_cb.set_alignment(0.5, 0.5)
        self.dhcp_cb.connect('toggled', self.toggle_dhcp)
        self.net_config_page.pack_start(self.dhcp_cb, True, True, 5)

        # Non-DHCP table
        self.ip_table = gtk.Table()
        self.ip_table.set_row_spacings(3)
        self.ip_table.set_col_spacings(3)
        # static IP
        #                    (widget,                   l, r, t, b, xopt,                yopt, xpad, ypad)
        self.ip_table.attach(gtk.Label(_('IP')), 0, 1, 1, 2, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.ip_entry = gtk.Entry(15)
        self.ip_entry.set_text(self.profile['ip'])
        self.ip_table.attach(self.ip_entry,             1, 2, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.ip_table.attach(gtk.Label(_('Netmask')), 0, 1, 2, 3, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.netmask_entry = gtk.Entry(15)
        self.netmask_entry.set_text(self.profile['netmask'])
        self.ip_table.attach(self.netmask_entry,        1, 2, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.ip_table.attach(gtk.Label(_('Gateway')), 0, 1, 3, 4, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.gw_entry = gtk.Entry(15)
        self.gw_entry.set_text(self.profile['gateway'])
        self.ip_table.attach(self.gw_entry,             1, 2, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.ip_table.attach(gtk.Label(_('Domain')), 0, 1, 4, 5, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.domain_entry = gtk.Entry(32)
        self.domain_entry.set_text(self.profile['domain'])
        self.ip_table.attach(self.domain_entry,         1, 2, 4, 5, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.ip_table.attach(gtk.Label(_('First DNS')), 0, 1, 5, 6, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.dns1_entry = gtk.Entry(15)
        self.dns1_entry.set_text(self.profile['dns1'])
        self.ip_table.attach(self.dns1_entry,           1, 2, 5, 6, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.ip_table.attach(gtk.Label(_('Second DNS')), 0, 1, 6, 7, gtk.FILL|gtk.EXPAND, 0, 5, 0)
        self.dns2_entry = gtk.Entry(15)
        self.dns2_entry.set_text(self.profile['dns2'])
        self.ip_table.attach(self.dns2_entry,           1, 2, 6, 7, gtk.FILL|gtk.EXPAND, 0,    5,    0)

        self.net_config_page.pack_start(self.ip_table, False, False, 0)
        self.profile_notebook.append_page(self.net_config_page, gtk.Label(_('IP Configuration')))

        # create the scripting page
        con_pp_table = gtk.Table()
        con_pp_table.set_row_spacings(3)
        con_pp_table.set_col_spacings(3)
        # The labels
        self.conn_before = gtk.Label(_('Before Connection'))
        self.conn_before.set_alignment(1, 0.5)
        #                    (widget,                   l, r, t, b, xopt,                yopt, xpad, ypad)
        con_pp_table.attach(self.conn_before,           0, 1, 0, 1, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.conn_after = gtk.Label(_('After Connection'))
        self.conn_after.set_alignment(1, 0.5)
        con_pp_table.attach(self.conn_after,            0, 1, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        # The text boxes
        self.con_prescript_entry = gtk.Entry()
        self.con_prescript_entry.set_text(self.profile['con_prescript'])
        con_pp_table.attach(self.con_prescript_entry,   1, 2, 0, 1, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.con_prescript_entry.set_tooltip_text(_('The local command to '
            'execute before trying to connect to the network.'))
        self.con_postscript_entry = gtk.Entry()
        self.con_postscript_entry.set_text(self.profile['con_postscript'])
        con_pp_table.attach(self.con_postscript_entry,  1, 2, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.con_postscript_entry.set_tooltip_text(_('The local command to '
            'execute after connecting to the network.'))
        # The labels
        self.dis_before = gtk.Label(_('Before Disconnection'))
        self.dis_before.set_alignment(1, 0.5)
        con_pp_table.attach(self.dis_before,            0, 1, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.dis_after = gtk.Label(_('After Disconnection'))
        self.dis_after.set_alignment(1, 0.5)
        con_pp_table.attach(self.dis_after,             0, 1, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        # The text boxes
        self.dis_prescript_entry = gtk.Entry()
        self.dis_prescript_entry.set_text(self.profile['dis_prescript'])
        con_pp_table.attach(self.dis_prescript_entry,   1, 2, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.dis_prescript_entry.set_tooltip_text(_('The local command to '
            'execute before disconnecting from the network.'))
        self.dis_postscript_entry = gtk.Entry()
        self.dis_postscript_entry.set_text(self.profile['dis_postscript'])
        con_pp_table.attach(self.dis_postscript_entry,  1, 2, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.dis_postscript_entry.set_tooltip_text(_('The local command to '
            'execute after disconnecting from the network.'))
        self.profile_notebook.append_page(con_pp_table, gtk.Label(_('Scripting')))

        self.vbox.pack_start(self.profile_notebook, False, False, 5)

    def run(self):
        """ Display profile dialog and return an edited profile or None.
        """
        self.set_security_sensitivity(self.profile['security'])
        self.ip_table.set_sensitive(not self.profile['use_dhcp'])
        self.show_all()
        error = True
        while error:
            error = False
            result = gtk.Dialog.run(self)
            if result is not None:
                if self.essid_entry.get_text().strip() == '':
                    raise ValueError
                self.profile['known'] = True
                self.profile['essid'] = self.essid_entry.get_text().strip()
                if self.roaming_cb.get_active():
                    self.profile['bssid'] = ''
                else:
                    self.profile['bssid'] = self.bssid_entry.get_text().strip()
                self.profile['roaming'] = self.roaming_cb.get_active()
                if self.wep_key_cb.get_active():
                    self.profile['key'] = ''    # hex WEP key
                    if re.compile('[^0-9a-fA-F]').search(self.key_entry.get_text().strip()):
                        dlg = gtk.MessageDialog(self,
                            gtk.DIALOG_DESTROY_WITH_PARENT | gtk.DIALOG_MODAL,
                            gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,
                            _('Invalid hex WEP key.'))
                        dlg.run()
                        dlg.destroy()
                        del dlg
                        error = True
                else:
                    self.profile['key'] = 's:'    # ASCII WEP key
                self.profile['key'] = self.profile['key'] + self.key_entry.get_text().strip()
                if self.mode_combo.get_active_text() is None:
                    self.profile['mode'] = _('auto')
                else:
                    self.profile['mode'] = self.mode_combo.get_active_text()
                self.profile['encrypted'] = (self.profile['security'] != '')
                if self.channel_combo.get_active_text() is None:
                    self.profile['channel'] = _('auto')
                else:
                    self.profile['channel'] = self.channel_combo.get_active_text()
                self.profile['protocol'] = 'g'
                self.profile['available'] = (self.profile['signal'] > 0)
                self.profile['con_prescript'] = self.con_prescript_entry.get_text().strip()
                self.profile['con_postscript'] = self.con_postscript_entry.get_text().strip()
                self.profile['dis_prescript'] = self.dis_prescript_entry.get_text().strip()
                self.profile['dis_postscript'] = self.dis_postscript_entry.get_text().strip()
                # security
                if self.wep_mode_combo.get_active_text() is None:
                    self.profile['wep_mode'] = _('none')
                else:
                    self.profile['wep_mode'] = self.wep_mode_combo.get_active_text()
                self.profile['wpa_psk'] = self.wpa_psk_entry.get_text().strip()
                # dhcp
                self.profile['use_dhcp'] = self.dhcp_cb.get_active()
                self.profile['ip'] = self.ip_entry.get_text().strip()
                self.profile['netmask'] = self.netmask_entry.get_text().strip()
                self.profile['gateway'] = self.gw_entry.get_text().strip()
                self.profile['domain'] = self.domain_entry.get_text().strip()
                self.profile['dns1'] = self.dns1_entry.get_text().strip()
                self.profile['dns2'] = self.dns2_entry.get_text().strip()
            else:
                return None
        return self.profile

    def toggle_roaming(self, roaming_toggle, data=None):
        """ Respond to a roaming checkbox toggle by activating or de-activating
            the BSSID text entry.  :data:`roaming_toggle` is the
            :class:`gtk.CheckButton` sending the signal.  :data:`data` is a
            (not used) list of arbitrary arguments.
        """
        self.bssid_entry.set_sensitive(not roaming_toggle.get_active())

    def toggle_security(self, security_radio, data=None):
        """ Respond to security radio toggles by activating or de-activating
            the various security text entries.  :data:`security_radio` is the
            :class:`gtk.RadioButton` sending the signal.  :data:`data` is a
            (not used) list of arbitrary arguments.
        """
        if security_radio == self.security_radio_none:
            self.profile['security'] = 'none'
        elif security_radio == self.security_radio_wep:
            self.profile['security'] = 'wep'
        elif security_radio == self.security_radio_wpa:
            self.profile['security'] = 'wpa'
        self.set_security_sensitivity(self.profile['security'])
        self.security_setting.set_text(_('Security: {SEC}').format(
            SEC=self.profile['security'].upper()))

    def set_security_sensitivity(self, security_mode):
        """ Set the sensitivity of the various security text entries.
            :data:`security_mode` is the security mode: 'none', 'wep', or
            'wpa'.
        """
        if security_mode == 'none':
            self.wep_key_cb.set_sensitive(False)
            self.key_entry.set_sensitive(False)
            self.wep_mode_label.set_sensitive(False)
            self.wep_mode_combo.set_sensitive(False)
            self.psk_label.set_sensitive(False)
            self.wpa_psk_entry.set_sensitive(False)
        elif security_mode == 'wep':
            self.wep_key_cb.set_sensitive(True)
            self.key_entry.set_sensitive(True)
            self.wep_mode_label.set_sensitive(True)
            self.wep_mode_combo.set_sensitive(True)
            self.psk_label.set_sensitive(False)
            self.wpa_psk_entry.set_sensitive(False)
        elif security_mode == 'wpa':
            self.wep_key_cb.set_sensitive(False)
            self.key_entry.set_sensitive(False)
            self.wep_mode_label.set_sensitive(False)
            self.wep_mode_combo.set_sensitive(False)
            self.psk_label.set_sensitive(True)
            self.wpa_psk_entry.set_sensitive(True)

    def toggle_dhcp(self, widget=None, data=None):
        """ Respond to clicking the DHCP check button.  :data:`widget`
            is the widget sending the signal and :data:`data` is a list
            of arbitrary arguments, both are ignored.
        """
        self.ip_table.set_sensitive(not self.dhcp_cb.get_active())


# Make so we can be imported
if __name__ == '__main__':
    pass
