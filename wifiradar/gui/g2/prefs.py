#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   gui/g2/prefs.py - preferences editor, etc. for PyGTK UI
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2012 Anari Jalakas <anari.jalakas@gmail.com>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import errno
import gtk
import logging
from subprocess import call, Popen, PIPE, STDOUT

from wifiradar.misc import _
from . import transients

# create a logger
logger = logging.getLogger(__name__)


class PreferencesEditor(gtk.Dialog, object):
    """ The preferences dialog.  Edits non-profile sections of the config file.
    """
    def __init__(self, parent, config):
        """ Create a new PreferencesEditor, with :data:`parent` window,
            to edit :data:`config`.
        """
        gtk.Dialog.__init__(self, _('WiFi Radar Preferences'), parent,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
             gtk.STOCK_SAVE, gtk.RESPONSE_APPLY))

        self.config = config
        # Get raw strings from configuration.
        self.config.raw = True

        self.icon = gtk.gdk.pixbuf_new_from_file('pixmaps/wifi-radar.png')
        self.set_icon(self.icon)
        self.set_resizable(True)
        #
        # set up preferences widgets
        #
        # build everything in a tabbed notebook
        self.prefs_notebook = gtk.Notebook()

        ### General tab
        self.general_page = gtk.VBox()
        # auto detect wireless device
        self.w_auto_detect = gtk.CheckButton(_('Auto-detect wireless device'))

        self.w_auto_detect.set_active(self.config.get_opt('GENERAL', 'interface') == _('auto_detect'))
        self.w_auto_detect.connect('toggled', self.toggle_auto_detect)
        self.w_auto_detect.set_tooltip_text(_('Automatically select wireless '
            'device to configure'))
        self.general_page.pack_start(self.w_auto_detect, False, False, 5)

        # network interface selecter
        self.w_interface = gtk.combo_box_entry_new_text()
        try:
            iwconfig_info = Popen([self.config.get_opt('GENERAL', 'iwconfig_command')], stdout=PIPE, stderr=STDOUT).stdout
        except OSError as e:
            if e.errno == 2:
                logger.critical(_('iwconfig command not found, please set '
                    'this in the preferences.'))
                iwconfig_info = ''
        wireless_devices = [(x[0:x.find(' ')]) for x in iwconfig_info if ('ESSID' in x)]
        for device in wireless_devices:
            if device != self.config.get_opt('GENERAL', 'interface'):
                self.w_interface.append_text(device)
        if self.config.get_opt('GENERAL', 'interface') != _('auto_detect'):
            self.w_interface.prepend_text(self.config.get_opt('GENERAL', 'interface'))
        self.w_interface.set_active(0)
        self.w_interface_label = gtk.Label(_('Wireless device'))
        self.w_hbox1 = gtk.HBox(False, 0)
        self.w_hbox1.pack_start(self.w_interface_label, False, False, 5)
        self.w_hbox1.pack_start(self.w_interface, True, True, 0)
        self.w_interface.set_sensitive(self.config.get_opt('GENERAL', 'interface') != _('auto_detect'))
        self.general_page.pack_start(self.w_hbox1, False, False, 5)

        # scan timeout (spin button of integers from 1 to 100)
        #self.time_in_seconds = gtk.Adjustment(self.config.get_opt_as_int('GENERAL', 'scan_timeout'),1,100,1,1,0)
        #self.w_scan_timeout = gtk.SpinButton(self.time_in_seconds, 1, 0)
        #self.w_scan_timeout.set_update_policy(gtk.UPDATE_IF_VALID)
        #self.w_scan_timeout.set_numeric(True)
        #self.w_scan_timeout.set_snap_to_ticks(True)
        #self.w_scan_timeout.set_wrap(False)
        #self.w_scan_timeout.set_tooltip_text(_('How long should WiFi Radar scan for access points?'))
        #self.w_scan_timeout_label = gtk.Label(_('Scan timeout (seconds)'))
        #self.w_hbox2 = gtk.HBox(False, 0)
        #self.w_hbox2.pack_start(self.w_scan_timeout_label, False, False, 5)
        #self.w_hbox2.pack_start(self.w_scan_timeout, True, True, 0)
        #self.general_page.pack_start(self.w_hbox2, False, False, 5)

        # commit required
        self.w_commit_required = gtk.CheckButton(_('Commit required'))
        self.w_commit_required.set_active(self.config.get_opt_as_bool('GENERAL', 'commit_required'))
        self.w_commit_required.set_tooltip_text(_('Check this box if your '
            'card requires a "commit" command with iwconfig'))
        self.general_page.pack_start(self.w_commit_required, False, False, 5)

        # ifup required
        self.w_ifup_required = gtk.CheckButton(_('Ifup required'))
        self.w_ifup_required.set_active(self.config.get_opt_as_bool('GENERAL', 'ifup_required'))
        self.w_ifup_required.set_tooltip_text(_('Check this box if your '
            'system requires the interface to be brought up first'))
        self.general_page.pack_start(self.w_ifup_required, False, False, 5)

        self.prefs_notebook.append_page(self.general_page, gtk.Label(_('General')))
        ### End of General tab

        ### Advanced tab
        # table to use for layout of following command configurations
        self.cmds_table = gtk.Table()

        # ifconfig command
        self.ifconfig_cmd = gtk.Entry()
        self.ifconfig_cmd.set_width_chars(32)
        self.ifconfig_cmd.set_tooltip_text(_('The command to use to configure '
            'the network card'))
        self.ifconfig_cmd.set_text(self.config.get_opt('GENERAL', 'ifconfig_command'))
        self.ifconfig_cmd_label = gtk.Label(_('Network interface configure command'))
        self.ifconfig_cmd_button = transients.FileBrowseButton(self, self.ifconfig_cmd)
        #                     (widget,                   l, r, t, b, xopt,                yopt, xpad, ypad)
        self.cmds_table.attach(self.ifconfig_cmd_label,  1, 2, 1, 2, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.cmds_table.attach(self.ifconfig_cmd,        2, 3, 1, 2, gtk.FILL|gtk.EXPAND, 0,    0,    0)
        self.cmds_table.attach(self.ifconfig_cmd_button, 3, 4, 1, 2, gtk.FILL|gtk.EXPAND, 0,    0,    0)

        # iwconfig command
        self.iwconfig_cmd = gtk.Entry()
        self.iwconfig_cmd.set_width_chars(32)
        self.iwconfig_cmd.set_tooltip_text(_('The command to use to configure '
            'the wireless connection'))
        self.iwconfig_cmd.set_text(self.config.get_opt('GENERAL', 'iwconfig_command'))
        self.iwconfig_cmd_label = gtk.Label(_('Wireless connection configure command'))
        self.iwconfig_cmd_button = transients.FileBrowseButton(self, self.iwconfig_cmd)
        #                     (widget,                   l, r, t, b, xopt,                yopt, xpad, ypad)
        self.cmds_table.attach(self.iwconfig_cmd_label,  1, 2, 2, 3, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.cmds_table.attach(self.iwconfig_cmd,        2, 3, 2, 3, gtk.FILL|gtk.EXPAND, 0,    0,    0)
        self.cmds_table.attach(self.iwconfig_cmd_button, 3, 4, 2, 3, gtk.FILL|gtk.EXPAND, 0,    0,    0)

        # iwlist command
        self.iwlist_cmd = gtk.Entry()
        self.iwlist_cmd.set_width_chars(32)
        self.iwlist_cmd.set_tooltip_text(_('The command to use to scan for '
            'access points'))
        self.iwlist_cmd.set_text(self.config.get_opt('GENERAL', 'iwlist_command'))
        self.iwlist_cmd_label = gtk.Label(_('Wireless scanning command'))
        self.iwlist_cmd_button = transients.FileBrowseButton(self, self.iwlist_cmd)
        #                     (widget,                 l, r, t, b, xopt,                yopt, xpad, ypad)
        self.cmds_table.attach(self.iwlist_cmd_label,  1, 2, 3, 4, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.cmds_table.attach(self.iwlist_cmd,        2, 3, 3, 4, gtk.FILL|gtk.EXPAND, 0,    0,    0)
        self.cmds_table.attach(self.iwlist_cmd_button, 3, 4, 3, 4, gtk.FILL|gtk.EXPAND, 0,    0,    0)

        # route command
        self.route_cmd = gtk.Entry()
        self.route_cmd.set_width_chars(32)
        self.route_cmd.set_tooltip_text(_('The command to use to configure '
            'the network routing'))
        self.route_cmd.set_text(self.config.get_opt('GENERAL', 'route_command'))
        self.route_cmd_label = gtk.Label(_('Network route configure command'))
        self.route_cmd_button = transients.FileBrowseButton(self, self.route_cmd)
        #                     (widget,                l, r, t, b, xopt,                yopt, xpad, ypad)
        self.cmds_table.attach(self.route_cmd_label,  1, 2, 4, 5, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.cmds_table.attach(self.route_cmd,        2, 3, 4, 5, gtk.FILL|gtk.EXPAND, 0,    0,    0)
        self.cmds_table.attach(self.route_cmd_button, 3, 4, 4, 5, gtk.FILL|gtk.EXPAND, 0,    0,    0)

        # log file
        self.logfile_entry = gtk.Entry()
        self.logfile_entry.set_width_chars(32)
        self.logfile_entry.set_tooltip_text(_('The file in which to save '
            'logging info'))
        self.logfile_entry.set_text(self.config.get_opt('GENERAL', 'logfile'))
        self.logfile_label = gtk.Label(_('Log file'))
        self.logfile_button = transients.FileBrowseButton(self, self.logfile_entry)
        #                     (widget,              l, r, t, b, xopt,                yopt, xpad, ypad)
        self.cmds_table.attach(self.logfile_label,  1, 2, 5, 6, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.cmds_table.attach(self.logfile_entry,  2, 3, 5, 6, gtk.FILL|gtk.EXPAND, 0,    0,    0)
        self.cmds_table.attach(self.logfile_button, 3, 4, 5, 6, gtk.FILL|gtk.EXPAND, 0,    0,    0)

        # log level (spin button of integers from 0 to 50 by 5's)
        self.loglevel = gtk.SpinButton(gtk.Adjustment(self.config.get_opt_as_int('GENERAL', 'loglevel'), 1, 50, 5, 5, 0), 1, 0)
        self.loglevel.set_update_policy(gtk.UPDATE_IF_VALID)
        self.loglevel.set_numeric(True)
        self.loglevel.set_snap_to_ticks(True)
        self.loglevel.set_wrap(False)
        self.loglevel.set_tooltip_text(_('How much detail to save in the log '
            'file.  Larger numbers provide less detail and smaller numbers, '
            'more detail.'))
        self.loglevel.set_text(self.config.get_opt('GENERAL', 'loglevel'))
        self.loglevel_label = gtk.Label(_('Log level'))
        #                     (widget,              l, r, t, b, xopt,                yopt, xpad, ypad)
        self.cmds_table.attach(self.loglevel_label, 1, 2, 6, 7, gtk.FILL|gtk.EXPAND, 0,    5,    0)
        self.cmds_table.attach(self.loglevel,       2, 3, 6, 7, gtk.FILL|gtk.EXPAND, 0,    0,    0)

        self.prefs_notebook.append_page(self.cmds_table, gtk.Label(_('Advanced')))
        ### End of Advanced tab

        ### DHCP tab
        # table to use for layout of DHCP prefs
        self.dhcp_table = gtk.Table()

        self.dhcp_cmd = gtk.Entry()
        self.dhcp_cmd.set_width_chars(32)
        self.dhcp_cmd.set_tooltip_text(_('The command to use for automatic '
            'network configuration'))
        self.dhcp_cmd.set_text(self.config.get_opt('DHCP', 'command'))
        self.dhcp_cmd_label = gtk.Label(_('Command'))
        self.dhcp_cmd_button = transients.FileBrowseButton(self, self.dhcp_cmd)
        self.dhcp_table.attach(self.dhcp_cmd_label, 1, 2, 1, 2, True, False, 5, 0)
        self.dhcp_table.attach(self.dhcp_cmd, 2, 3, 1, 2, True, False, 0, 0)
        self.dhcp_table.attach(self.dhcp_cmd_button, 3, 4, 1, 2, False, False, 0, 0)

        self.dhcp_args = gtk.Entry()
        self.dhcp_args.set_width_chars(32)
        self.dhcp_args.set_tooltip_text(_('The start-up arguments to the '
            'DHCP command'))
        self.dhcp_args.set_text(self.config.get_opt('DHCP', 'args'))
        self.dhcp_args_label = gtk.Label(_('Arguments'))
        self.dhcp_table.attach(self.dhcp_args_label, 1, 2, 2, 3, True, False, 5, 0)
        self.dhcp_table.attach(self.dhcp_args, 2, 3, 2, 3, True, False, 0, 0)

        self.dhcp_kill_args = gtk.Entry()
        self.dhcp_kill_args.set_width_chars(32)
        self.dhcp_kill_args.set_tooltip_text(_('The shutdown arguments to '
            'the DHCP command'))
        self.dhcp_kill_args.set_text(self.config.get_opt('DHCP', 'kill_args'))
        self.dhcp_kill_args_label = gtk.Label(_('Kill arguments'))
        self.dhcp_table.attach(self.dhcp_kill_args_label, 1, 2, 3, 4, True, False, 5, 0)
        self.dhcp_table.attach(self.dhcp_kill_args, 2, 3, 3, 4, True, False, 0, 0)

        self.dhcp_timeout = gtk.Entry()
        self.dhcp_timeout.set_width_chars(32)
        self.dhcp_timeout.set_tooltip_text(_('The amount of time DHCP will '
            'spend trying to connect'))
        self.dhcp_timeout.set_text(self.config.get_opt('DHCP', 'timeout'))
        self.dhcp_timeout_label = gtk.Label(_('DHCP connect timeout (seconds)'))
        self.dhcp_table.attach(self.dhcp_timeout_label, 1, 2, 4, 5, True, False, 5, 0)
        self.dhcp_table.attach(self.dhcp_timeout, 2, 3, 4, 5, True, False, 0, 0)

        self.dhcp_pidfile = gtk.Entry()
        self.dhcp_pidfile.set_width_chars(32)
        self.dhcp_pidfile.set_tooltip_text(_('The file DHCP uses to store '
            'its PID'))
        self.dhcp_pidfile.set_text(self.config.get_opt('DHCP', 'pidfile'))
        self.dhcp_pidfile_label = gtk.Label(_('PID file'))
        self.dhcp_table.attach(self.dhcp_pidfile_label, 1, 2, 5, 6, True, False, 5, 0)
        self.dhcp_table.attach(self.dhcp_pidfile, 2, 3, 5, 6, True, False, 0, 0)

        self.prefs_notebook.append_page(self.dhcp_table, gtk.Label(_('DHCP')))
        ### End of DHCP tab

        ### WPA tab
        # table to use for layout of DHCP prefs
        self.wpa_table = gtk.Table()

        self.wpa_cmd = gtk.Entry()
        self.wpa_cmd.set_width_chars(32)
        self.wpa_cmd.set_tooltip_text(_('The command to use for WPA encrypted '
            'connections'))
        self.wpa_cmd.set_text(self.config.get_opt('WPA', 'command'))
        self.wpa_cmd_label = gtk.Label(_('Command'))
        self.wpa_cmd_button = transients.FileBrowseButton(self, self.wpa_cmd)
        self.wpa_table.attach(self.wpa_cmd_label, 1, 2, 1, 2, True, False, 5, 0)
        self.wpa_table.attach(self.wpa_cmd, 2, 3, 1, 2, True, False, 0, 0)
        self.wpa_table.attach(self.wpa_cmd_button, 3, 4, 1, 2, False, False, 0, 0)

        self.wpa_args = gtk.Entry()
        self.wpa_args.set_width_chars(32)
        self.wpa_args.set_tooltip_text(_('The start-up arguments to the WPA '
            'command'))
        self.wpa_args.set_text(self.config.get_opt('WPA', 'args'))
        self.wpa_args_label = gtk.Label(_('Arguments'))
        self.wpa_table.attach(self.wpa_args_label, 1, 2, 2, 3, True, False, 5, 0)
        self.wpa_table.attach(self.wpa_args, 2, 3, 2, 3, True, False, 0, 0)

        self.wpa_kill_args = gtk.Entry()
        self.wpa_kill_args.set_width_chars(32)
        self.wpa_kill_args.set_tooltip_text(_('The shutdown arguments to the '
            'WPA command'))
        self.wpa_kill_args.set_text(self.config.get_opt('WPA', 'kill_command'))
        self.wpa_kill_args_label = gtk.Label(_('Kill command'))
        self.wpa_table.attach(self.wpa_kill_args_label, 1, 2, 3, 4, True, False, 5, 0)
        self.wpa_table.attach(self.wpa_kill_args, 2, 3, 3, 4, True, False, 0, 0)

        self.wpa_config = gtk.Entry()
        self.wpa_config.set_width_chars(32)
        self.wpa_config.set_tooltip_text(_('The WPA configuration file to use'))
        self.wpa_config.set_text(self.config.get_opt('WPA', 'configuration'))
        self.wpa_config_label = gtk.Label(_('Configuration file'))
        self.wpa_table.attach(self.wpa_config_label, 1, 2, 4, 5, True, False, 5, 0)
        self.wpa_table.attach(self.wpa_config, 2, 3, 4, 5, True, False, 0, 0)

        self.wpa_driver = gtk.Entry()
        self.wpa_driver.set_width_chars(32)
        self.wpa_driver.set_tooltip_text(_('The WPA driver to use'))
        self.wpa_driver.set_text(self.config.get_opt('WPA', 'driver'))
        self.wpa_driver_label = gtk.Label(_('Driver'))
        self.wpa_table.attach(self.wpa_driver_label, 1, 2, 5, 6, True, False, 5, 0)
        self.wpa_table.attach(self.wpa_driver, 2, 3, 5, 6, True, False, 0, 0)

        self.wpa_pidfile = gtk.Entry()
        self.wpa_pidfile.set_width_chars(32)
        self.wpa_pidfile.set_tooltip_text(_('The file WPA uses to store its '
            'PID'))
        self.wpa_pidfile.set_text(self.config.get_opt('WPA', 'pidfile'))
        self.wpa_pidfile_label = gtk.Label(_('PID file'))
        self.wpa_table.attach(self.wpa_pidfile_label, 1, 2, 6, 7, True, False, 5, 0)
        self.wpa_table.attach(self.wpa_pidfile, 2, 3, 6, 7, True, False, 0, 0)

        self.prefs_notebook.append_page(self.wpa_table, gtk.Label(_('WPA')))
        ### End of WPA tab

        self.vbox.pack_start(self.prefs_notebook, False, False, 5)

    def toggle_auto_detect(self, auto_detect_toggle, data=None):
        """ Respond to Auto-detect checkbox toggle by activating or
            de-activating the interface ComboBox.  :data:`auto_detect_toggle`
            is the checkbox sending the signal.  :data:`data` is a list
            of arbitrary arguments, it is ignored.
        """
        self.w_interface.set_sensitive(not auto_detect_toggle.get_active())

    def run(self):
        """ Display preferences dialog and operate until canceled or okayed.
            :func:`run` returns a tuple with a :data:`GTK Response Type Constant`
            and the possibly added :class:`ConfigManger`.
        """
        self.show_all()
        response = gtk.Dialog.run(self)
        if response == gtk.RESPONSE_APPLY:
            self.apply()
        return (response, self.config)

    def apply(self):
        """ Apply the values in the UI to the configuration object.
        """
        if self.w_auto_detect.get_active():
            self.config.set_opt('GENERAL', 'interface', _('auto_detect'))
        else:
            interface = _('auto_detect')
            if self.w_interface.get_active_text() != '':
                interface = self.w_interface.get_active_text()
            self.config.set_opt('GENERAL', 'interface', interface)
        #self.config.set_float_opt('GENERAL', 'scan_timeout',
            #self.w_scan_timeout.get_value().strip())
        self.config.set_bool_opt('GENERAL', 'commit_required',
            self.w_commit_required.get_active())
        self.config.set_bool_opt('GENERAL', 'ifup_required',
            self.w_ifup_required.get_active())
        self.config.set_opt('GENERAL', 'ifconfig_command',
            self.ifconfig_cmd.get_text().strip())
        self.config.set_opt('GENERAL', 'iwconfig_command',
            self.iwconfig_cmd.get_text().strip())
        self.config.set_opt('GENERAL', 'iwlist_command',
            self.iwlist_cmd.get_text().strip())
        self.config.set_opt('GENERAL', 'route_command',
            self.route_cmd.get_text().strip())
        self.config.set_opt('GENERAL', 'logfile',
            self.logfile_entry.get_text().strip())
        self.config.set_int_opt('GENERAL', 'loglevel',
            int(self.loglevel.get_value()))
        self.config.set_opt('DHCP', 'command',
            self.dhcp_cmd.get_text().strip())
        self.config.set_opt('DHCP', 'args',
            self.dhcp_args.get_text().strip())
        self.config.set_opt('DHCP', 'kill_args',
            self.dhcp_kill_args.get_text().strip())
        self.config.set_opt('DHCP', 'timeout',
            self.dhcp_timeout.get_text().strip())
        self.config.set_opt('DHCP', 'pidfile',
            self.dhcp_pidfile.get_text().strip())
        self.config.set_opt('WPA', 'command',
            self.wpa_cmd.get_text().strip())
        self.config.set_opt('WPA', 'args',
            self.wpa_args.get_text().strip())
        self.config.set_opt('WPA', 'kill_command',
            self.wpa_kill_args.get_text().strip())
        self.config.set_opt('WPA', 'configuration',
            self.wpa_config.get_text().strip())
        self.config.set_opt('WPA', 'driver',
            self.wpa_driver.get_text().strip())
        self.config.set_opt('WPA', 'pidfile',
            self.wpa_pidfile.get_text().strip())


# Make so we can be imported
if __name__ == '__main__':
    pass
