#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   gui/g2/transients.py - supporting dialogs for PyGTK UI
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2012 Anari Jalakas <anari.jalakas@gmail.com>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import gtk
import gobject
import logging

from wifiradar.misc import _, WIFI_RADAR_VERSION

# create a logger
logger = logging.getLogger(__name__)


class AboutDialog(gtk.AboutDialog, object):
    """ Manage a GTK About Dialog.
    """
    def __init__(self):
        """ Create an AboutDialog filled with WiFi Radar information.
        """
        gtk.AboutDialog.__init__(self)
        self.set_authors(['Ahmad Baitalmal <ahmad@baitalmal.com>',
            'Brian Elliott Finley <brian@thefinleys.com>',
            'Sean Robinson <robinson@tuxfamily.org>', '',
            'Contributors', 'Douglas Breault', 'Nicolas Brouard',
            'Jon Collette',  'David Decotigny', 'Simon Gerber',
            'Joey Hurst',  'Anari Jalakas', 'Ante Karamatic',
            'Richard Monk', 'Kevin Otte', 'Nathanael Rebsch',
            'Andrea Scarpino', 'Prokhor Shuchalov', 'Patrick Winnertz'])
        self.set_comments(_('WiFi connection manager'))
        self.set_copyright(_('Copyright 2004-2014 by various authors '
            'and contributors\nCurrent Maintainer: '
            'Sean Robinson <robinson@tuxfamily.org>'))
        self.set_documenters(['Gary Case'])
        license = _("""
            This program is free software; you can redistribute it and/or modify
            it under the terms of the GNU General Public License as published by
            the Free Software Foundation; either version 2 of the License, or
            (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.

            You should have received a copy of the GNU General Public License
            along with this program; if not, write to:
            Free Software Foundation, Inc.
            51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA""")
        self.set_license(license)
        self.icon = gtk.gdk.pixbuf_new_from_file('pixmaps/wifi-radar.png')
        self.set_logo(self.icon)
        self.set_name('WiFi Radar')
        self.set_version(WIFI_RADAR_VERSION)
        self.set_website('http://wifi-radar.tuxfamily.org/')


class ErrorDialog(gtk.MessageDialog, object):
    """ Simple dialog to report an error to the user.
    """
    def __init__(self, parent, message):
        """ Manage the complete life-cycle of an error message dialog.
        """
        gtk.MessageDialog.__init__(self, parent,
            gtk.DIALOG_DESTROY_WITH_PARENT | gtk.DIALOG_MODAL,
            gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, message)
        self.run()
        self.destroy()


class FileBrowseButton(gtk.Button, object):
    """ Button to allow user to choose a file and put value into specified gtk.Entry.
    """
    def __init__(self, parent, entry):
        """ Create a button to simulate a File/Open.
        """
        gtk.Button.__init__(self, _('Browse'), None)
        self.entry = entry
        self.browser_dialog = gtk.FileChooserDialog(None, parent,
            gtk.FILE_CHOOSER_ACTION_OPEN,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
             gtk.STOCK_OPEN, gtk.RESPONSE_OK), None)
        self.connect('clicked', self.browse_files)

    def browse_files(self, widget=None):
        """ Show a file chooser dialog, get the user selection, and update
            the text entry value associated with the :class:`FileBrowseButton`.
            :data:`widget` is the widget sending the signal, but it is
            ignored.
        """
        self.browser_dialog.set_filename(self.entry.get_text())
        self.browser_dialog.run()
        filename = self.browser_dialog.get_filename()
        if filename:
            self.entry.set_text(filename)
        self.browser_dialog.destroy()


class StatusWindow(gtk.Dialog, object):
    """ A simple class for putting up a "Please wait" dialog so the user
        doesn't think we've forgotten about them.  Implements the status
        interface.  Status interface requires show(), update_message(message),
        and hide() methods.
    """
    def __init__(self, parent):
        """ Create a new StatusWindow under :data:`parent`, the ancestor
            widget of the instance of :class:`StatusWindow`.
        """
        gtk.Dialog.__init__(self, _('Working'), parent,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT)
        self.icon = gtk.gdk.pixbuf_new_from_file('pixmaps/wifi-radar.png')
        self.set_icon(self.icon)
        self.label = gtk.Label(_('Please wait...'))
        self.bar = gtk.ProgressBar()
        self.vbox.pack_start(self.label)
        self.vbox.pack_start(self.bar)
        self.cancel_button = self.add_button(gtk.STOCK_CANCEL, gtk.BUTTONS_CANCEL)
        self.timer = None

    def update_message(self, message):
        """ Change the :data:`message` string displayed to the user.
        """
        self.label.set_text(message)

    def update_window(self):
        """ Update the :class:`StatusWindow` progress bar.  Always return True
            so a timer will reschedule this method.
        """
        self.bar.pulse()
        return True

    def run(self):
        """ Display and operate the StatusWindow.
        """
        pass

    def show(self):
        """ Show all the widgets of the StatusWindow.
        """
        self.show_all()
        self.timer = gobject.timeout_add(250, self.update_window)
        return False

    def hide(self):
        """ Hide all the widgets of the StatusWindow.
        """
        if self.timer:
            gobject.source_remove(self.timer)
        self.timer = None
        self.hide_all()
        return False

    def destroy(self):
        """ Remove the StatusWindow.
        """
        if self.timer:
            gobject.source_remove(self.timer)


# Make so we can be imported
if __name__ == '__main__':
    pass

