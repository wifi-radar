#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   gui/g2/__init__.py - collection of classes for main UI with PyGTK
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2012 Anari Jalakas <anari.jalakas@gmail.com>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import errno
import logging
import signal
import sys

import glib
import gtk

from wifiradar.config import make_section_name
import wifiradar.connections as connections
from wifiradar.misc import _, PipeError, get_new_profile
from wifiradar.pubsub import Message
from . import prefs
from . import profile as profile_ed
from . import transients

# create a logger
logger = logging.getLogger(__name__)


# Create a bunch of icons from files in the package.
known_profile_icon = gtk.gdk.pixbuf_new_from_file('pixmaps/known_profile.png')
unknown_profile_icon = gtk.gdk.pixbuf_new_from_file('pixmaps/unknown_profile.png')
signal_none_pb = gtk.gdk.pixbuf_new_from_file('pixmaps/signal_none.xpm')
signal_low_pb = gtk.gdk.pixbuf_new_from_file('pixmaps/signal_low.xpm')
signal_barely_pb = gtk.gdk.pixbuf_new_from_file('pixmaps/signal_barely.xpm')
signal_ok_pb = gtk.gdk.pixbuf_new_from_file('pixmaps/signal_ok.xpm')
signal_best_pb = gtk.gdk.pixbuf_new_from_file('pixmaps/signal_best.xpm')

def pixbuf_from_known(known):
    """ Return a :class:`gtk.gdk.Pixbuf` icon to represent :data:`known`.
        Any true :data:`known` value returns the icon showing previous
        familiarity.
    """
    if known:
        return known_profile_icon
    return unknown_profile_icon

def pixbuf_from_signal(signal):
    """ Return a :class:`gtk.gdk.Pixbuf` icon to indicate the :data:`signal`
        level.  :data:`signal` is as reported by iwlist (may be arbitrary
        scale in 0-100 or -X dBm)
    """
    signal = int(signal)
    # Shift signal up by 80 to convert dBm scale to arbitrary scale.
    if signal < 0:
        signal = signal + 80
    # Find an icon...
    if signal < 3:
        return signal_none_pb
    elif signal < 12:
        return signal_low_pb
    elif signal < 20:
        return signal_barely_pb
    elif signal < 35:
        return signal_ok_pb
    elif signal >= 35:
        return signal_best_pb

def start(ui_pipe):
    """ Function to boot-strap the UI.  :data:`ui_pipe` is one half of a
        :class:`multiprocessing.Pipe` which will be given to the main UI
        element for intra-app communication.
    """
    # Reset SIGINT handler so that Ctrl+C in launching terminal
    # will kill the application.
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    gtk.gdk.threads_init()
    ui = RadarWindow(ui_pipe)
    ui.run()
    with gtk.gdk.lock:
        gtk.main()


class RadarWindow(gtk.Dialog, object):
    def __init__(self, msg_pipe):
        """ Create a new RadarWindow wanting to communicate through
            :data:`msg_pipe`, a :class:`multiprocessing.Connection`.
        """
        gtk.Dialog.__init__(self, 'WiFi Radar', None, gtk.DIALOG_MODAL)
        self.msg_pipe = msg_pipe

        self.icon = gtk.gdk.pixbuf_new_from_file('pixmaps/wifi-radar.png')

        self.set_icon(self.icon)
        self.set_border_width(10)
        self.set_size_request(550, 300)
        self.connect('delete_event', self.delete_event)
        # let's create all our widgets
        self.current_network = gtk.Label()
        self.current_network.set_property('justify', gtk.JUSTIFY_CENTER)
        self.current_network.show()
        self.close_button = gtk.Button(_('Close'), gtk.STOCK_CLOSE)
        self.close_button.show()
        self.close_button.connect('clicked', self.delete_event, None)
        self.about_button = gtk.Button(_('About'), gtk.STOCK_ABOUT)
        self.about_button.show()
        self.about_button.connect('clicked', self.show_about_info, None)
        self.preferences_button = gtk.Button(_('Preferences'), gtk.STOCK_PREFERENCES)
        self.preferences_button.show()
        self.preferences_button.connect('clicked', self.request_preferences_edit)
        #                           essid  bssid  known_icon      known  available  wep_icon  signal_level    mode  protocol  channel
        self.pstore = gtk.ListStore(str,   str,   gtk.gdk.Pixbuf, bool,  bool,      str,      gtk.gdk.Pixbuf, str,  str,      str)
        self.plist = gtk.TreeView(self.pstore)
        # The icons column, known and encryption
        self.pix_cell = gtk.CellRendererPixbuf()
        self.wep_cell = gtk.CellRendererPixbuf()
        self.icons_cell = gtk.CellRendererText()
        self.icons_col = gtk.TreeViewColumn()
        self.icons_col.pack_start(self.pix_cell, False)
        self.icons_col.pack_start(self.wep_cell, False)
        self.icons_col.add_attribute(self.pix_cell, 'pixbuf', 2)
        self.icons_col.add_attribute(self.wep_cell, 'stock-id', 5)
        self.plist.append_column(self.icons_col)
        # The AP column
        self.ap_cell = gtk.CellRendererText()
        self.ap_col = gtk.TreeViewColumn(_('Access Point'))
        self.ap_col.pack_start(self.ap_cell, True)
        self.ap_col.set_cell_data_func(self.ap_cell, self._set_ap_col_value)
        self.plist.append_column(self.ap_col)
        # The signal column
        self.sig_cell = gtk.CellRendererPixbuf()
        self.signal_col = gtk.TreeViewColumn(_('Signal'))
        self.signal_col.pack_start(self.sig_cell, True)
        self.signal_col.add_attribute(self.sig_cell, 'pixbuf', 6)
        self.plist.append_column(self.signal_col)
        # The mode column
        self.mode_cell = gtk.CellRendererText()
        self.mode_col = gtk.TreeViewColumn(_('Mode'))
        self.mode_col.pack_start(self.mode_cell, True)
        self.mode_col.add_attribute(self.mode_cell, 'text', 7)
        self.plist.append_column(self.mode_col)
        # The protocol column
        self.prot_cell = gtk.CellRendererText()
        self.protocol_col = gtk.TreeViewColumn('802.11')
        self.protocol_col.pack_start(self.prot_cell, True)
        self.protocol_col.add_attribute(self.prot_cell, 'text', 8)
        self.plist.append_column(self.protocol_col)
        # The channel column
        self.channel_cell = gtk.CellRendererText()
        self.channel_col = gtk.TreeViewColumn(_('Channel'))
        self.channel_col.pack_start(self.channel_cell, True)
        self.channel_col.add_attribute(self.channel_cell, 'text', 9)
        self.plist.append_column(self.channel_col)
        # DnD Ordering
        self.plist.set_reorderable(True)
        # detect d-n-d of AP in round-about way, since rows-reordered does not work as advertised
        self.pstore.connect('row-deleted', self.update_auto_profile_order)
        # enable/disable buttons based on the selected network
        self.selected_network = self.plist.get_selection()
        self.selected_network.connect('changed', self.on_network_selection, None)
        # the list scroll bar
        sb = gtk.ScrolledWindow()
        sb.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.plist.show()
        # Add New button
        self.new_button = gtk.Button(_('_New'))
        self.new_button.connect('clicked', self.create_new_profile)
        self.new_button.show()
        # Add Configure button
        self.edit_button = gtk.Button(_('C_onfigure'))
        self.edit_button.connect('clicked', self.request_profile_edit)
        self.edit_button.show()
        self.edit_button.set_sensitive(False)
        # Add Delete button
        self.delete_button = gtk.Button(_('_Delete'))
        self.delete_button.connect('clicked', self.request_profile_delete)
        self.delete_button.show()
        self.delete_button.set_sensitive(False)
        # Add Connect button
        self.connect_button = gtk.Button(_('Co_nnect'))
        self.connect_button.connect('clicked', self.connect_profile, None)
        # Add Disconnect button
        self.disconnect_button = gtk.Button(_('D_isconnect'))
        self.disconnect_button.connect('clicked', self.disconnect_profile, None)
        # lets add our widgets
        rows = gtk.VBox(False, 3)
        net_list = gtk.HBox(False, 0)
        listcols = gtk.HBox(False, 0)
        prows = gtk.VBox(False, 0)
        # lets start packing
        # the network list
        net_list.pack_start(sb, True, True, 0)
        sb.add(self.plist)
        # the rows level
        rows.pack_start(net_list , True, True, 0)
        rows.pack_start(self.current_network, False, True, 0)
        # the list columns
        listcols.pack_start(rows, True, True, 0)
        listcols.pack_start(prows, False, False, 5)
        # the list buttons
        prows.pack_start(self.new_button, False, False, 2)
        prows.pack_start(self.edit_button, False, False, 2)
        prows.pack_start(self.delete_button, False, False, 2)
        prows.pack_end(self.connect_button, False, False, 2)
        prows.pack_end(self.disconnect_button, False, False, 2)

        self.action_area.pack_start(self.about_button)
        self.action_area.pack_start(self.preferences_button)
        self.action_area.pack_start(self.close_button)

        rows.show()
        prows.show()
        listcols.show()
        self.vbox.add(listcols)
        self.vbox.set_spacing(3)
        self.show_all()
        #
        # Now, immediately hide these two.  The proper one will be
        # displayed later, based on interface state. -BEF-
        self.disconnect_button.hide()
        self.connect_button.hide()
        self.connect_button.set_sensitive(False)

        # set up status window for later use
        self.status_window = transients.StatusWindow(self)
        self.status_window.cancel_button.connect('clicked', self.disconnect_profile, 'cancel')

        self._running = True
        # Check for incoming messages every 25 ms, a.k.a. 40 Hz.
        glib.timeout_add(25, self.run)

    def run(self):
        """ Watch for incoming messages.
        """
        if self.msg_pipe.poll():
            try:
                msg = self.msg_pipe.recv()
            except (EOFError, IOError) as e:
                # This is bad, really bad.
                logger.critical(_('read on closed Pipe ({PIPE}), '
                    'failing...').format(PIPE=self.msg_pipe))
                raise PipeError(e)
            else:
                self._check_message(msg)
        # Update the UI before returning.
        self.update_network_info()
        self.update_connect_buttons()
        return self._running

    def _check_message(self, msg):
        """ Process incoming messages.
        """
        if msg.topic == 'EXIT':
            self.delete_event()
        elif msg.topic == 'CONFIG-UPDATE':
            # Replace configuration manager with the one in msg.details.
            self.config = msg.details
        elif msg.topic == 'PROFILE-EDIT':
            with gtk.gdk.lock:
                self.edit_profile(msg.details)
        elif msg.topic == 'PROFILE-UPDATE':
            with gtk.gdk.lock:
                self.update_profile(msg.details)
        elif msg.topic == 'PROFILE-UNLIST':
            with gtk.gdk.lock:
                self.delete_profile(msg.details)
        elif msg.topic == 'PROFILE-MOVE':
            new_position, profile = msg.details
            with gtk.gdk.lock:
                if profile['roaming']:
                    old_position = self.get_row_by_ap(profile['essid'])
                else:
                    old_position = self.get_row_by_ap(profile['essid'],
                        profile['bssid'])
                self.pstore.move_before(old_position, self.pstore[new_position].iter)
        elif msg.topic == 'PREFS-EDIT':
            with gtk.gdk.lock:
                self.edit_preferences(msg.details)
        elif msg.topic == 'ERROR':
            with gtk.gdk.lock:
                transients.ErrorDialog(self, msg.details)
        else:
            logger.warning(_('unrecognized Message: "{MSG}"').format(MSG=msg))

    def destroy(self, widget=None):
        """ Quit the Gtk event loop.  :data:`widget` is the widget
            sending the signal, but it is ignored.
        """
        if self.status_window:
            self.status_window.destroy()
        gtk.main_quit()

    def delete_event(self, widget=None, data=None):
        """ Shutdown the application.  :data:`widget` is the widget sending
            the signal and :data:`data` is a list of arbitrary arguments,
            both are ignored.  Always returns False to not propigate the
            signal which called :func:`delete_event`.
        """
        self._running = False
        self.msg_pipe.send(Message('EXIT', ''))
        self.msg_pipe.close()
        self.hide()
        # process GTK events so that window hides more quickly
        if sys.modules.has_key('gtk'):
            while gtk.events_pending():
                gtk.main_iteration(False)
        self.destroy()
        return False

    def update_network_info(self, profile=None, ip=None):
        """ Update the current ip and essid shown to the user.
        """
        if (profile is None) and (ip is None):
            self.current_network.set_text(_('Not Connected.'))
        else:
            self.current_network.set_text(_('Connected to {PROFILE}\n'
                'IP Address {IP}').format(PROFILE=profile, IP=ip))

    def update_connect_buttons(self, connected=False):
        """ Set the state of connect/disconnect buttons to reflect the
            current connected state.
        """
        if connected:
            self.connect_button.hide()
            self.disconnect_button.show()
        else:
            self.disconnect_button.hide()
            self.connect_button.show()

    def _set_ap_col_value(self, column, cell, model, iter):
        """ Set the text attribute of :data:`column` to the first two
            :data:`model` values joined by a newline.  This is for
            displaying the :data:`essid` and :data:`bssid` in a single
            cell column.
        """
        essid = model.get_value(iter, 0)
        bssid = model.get_value(iter, 1)
        cell.set_property('text', '\n'.join([essid, bssid]))

    def get_row_by_ap(self, essid, bssid=_('  Multiple APs')):
        """ Returns a :class:`gtk.TreeIter` for the row which holds
            :data:`essid` and :data:`bssid`.

            :data:`bssid` is optional.  If not given, :func:`get_row_by_ap`
            will try to match a roaming profile with the given :data:`essid`.

            If no match is found, it returns None.
        """
        for row in self.pstore:
            if (row[0] == essid) and (row[1] == bssid):
                return row.iter
        return None

    def on_network_selection(self, widget=None, data=None):
        """ Enable/disable buttons based on the selected network.
            :data:`widget` is the widget sending the signal and :data:`data`
            is a list of arbitrary arguments, both are ignored.
        """
        store, selected_iter = self.selected_network.get_selected()
        if selected_iter is None:
            # No row is selected, disable all buttons except New.
            # This occurs after a drag-and-drop.
            self.edit_button.set_sensitive(False)
            self.delete_button.set_sensitive(False)
            self.connect_button.set_sensitive(False)
        else:
            # One row is selected, so enable or disable buttons.
            self.connect_button.set_sensitive(True)
            if store.get_value(selected_iter, 3):
                # Known profile.
                self.edit_button.set_sensitive(True)
                self.delete_button.set_sensitive(True)
            else:
                # Unknown profile.
                self.edit_button.set_sensitive(True)
                self.delete_button.set_sensitive(False)

    def show_about_info(self, widget=None, data=None):
        """ Handle the life-cycle of the About dialog.  :data:`widget` is
            the widget sending the signal and :data:`data` is a list of
            arbitrary arguments, both are ignored.
        """
        about = transients.AboutDialog()
        about.run()
        about.destroy()

    def request_preferences_edit(self, widget=None, data=None):
        """ Respond to a request to edit the application preferences.
            :data:`widget` is the widget sending the signal and :data:`data`
            is a list of arbitrary arguments, both are ignored.
        """
        self.msg_pipe.send(Message('PREFS-EDIT-REQUEST', ''))

    def edit_preferences(self, config):
        """ Allow the user to edit :data:`config`.
        """
        prefs_editor = prefs.PreferencesEditor(self, config)
        response, config_copy = prefs_editor.run()
        if response == gtk.RESPONSE_APPLY:
            self.msg_pipe.send(Message('PREFS-UPDATE', config_copy))
        prefs_editor.destroy()

    def update_profile(self, profile):
        """ Updates the display of :data:`profile`.
        """
        if profile['roaming']:
            prow_iter = self.get_row_by_ap(profile['essid'])
        else:
            prow_iter = self.get_row_by_ap(profile['essid'], profile['bssid'])

        if prow_iter is None:
            # the AP is not in the list of APs on the screen
             self._add_profile(profile)
        else:
            # the AP is in the list of APs on the screen
            self._update_row(profile, prow_iter)

    def _add_profile(self, profile):
        """ Add :data:`profile` to the list of APs shown to the user.
        """
        if profile['roaming']:
            profile['bssid'] = _('  Multiple APs')

        wep = None
        if profile['encrypted']:
            wep = gtk.STOCK_DIALOG_AUTHENTICATION

        self.pstore.append([profile['essid'], profile['bssid'],
            known_profile_icon, profile['known'], profile['available'],
            wep, signal_none_pb, profile['mode'], profile['protocol'],
            profile['channel']])

    def _update_row(self, profile, row_iter):
        """ Change the values displayed in :data:`row_iter` (a
            :class:`gtk.TreeIter`) using :data:`profile`.
        """
        wep = None
        if profile['encrypted']:
            wep = gtk.STOCK_DIALOG_AUTHENTICATION
        # Update the Gtk objects.
        self.pstore.set_value(row_iter, 2, pixbuf_from_known(profile['known']))
        self.pstore.set_value(row_iter, 3, profile['known'])
        self.pstore.set_value(row_iter, 4, profile['available'])
        self.pstore.set_value(row_iter, 5, wep)
        self.pstore.set_value(row_iter, 6, pixbuf_from_signal(profile['signal']))
        self.pstore.set_value(row_iter, 7, profile['mode'])
        self.pstore.set_value(row_iter, 8, profile['protocol'])
        self.pstore.set_value(row_iter, 9, profile['channel'])

    def create_new_profile(self, widget=None, profile=None, data=None):
        """ Respond to a user request to create a new AP profile.
            :data:`widget` is the widget sending the signal.  :data:profile`
            is an AP profile to use as the basis for the new profile.  It
            is likely empty or mostly empty.  :data:`data` is a list of
            arbitrary arguments.  :data:`widget` and "data"`data` are both
            ignored.

            The order of parameters is important.  Because when this method
            is called from a signal handler, :data:`widget` is always the
            first argument.
        """
        if profile is None:
            profile = get_new_profile()

        profile_editor = profile_ed.ProfileEditor(self, profile)
        try:
            edited_profile = profile_editor.run()
        except ValueError:
            self.msg_pipe.send(Message('ERROR', _('Cannot save empty ESSID')))
        else:
            if profile:
                self.msg_pipe.send(Message('PROFILE-EDITED', (edited_profile, profile)))
        finally:
            profile_editor.destroy()

    def request_profile_edit(self, widget=None, data=None):
        """ Respond to a request to edit an AP profile.  :data:`widget`
            is the widget sending the signal and :data:`data` is a list
            of arbitrary arguments, both are ignored.
        """
        store, selected_iter = self.plist.get_selection().get_selected()
        if selected_iter is not None:
            essid = self.pstore.get_value(selected_iter, 0)
            bssid = self.pstore.get_value(selected_iter, 1)
            if bssid == _('  Multiple APs'):
                # AP list says this is a roaming profile
                bssid = ''
            self.msg_pipe.send(Message('PROFILE-EDIT-REQUEST', (essid, bssid)))

    def edit_profile(self, profile):
        """ Allow the user to edit :data:`profile`.
        """
        profile_editor = profile_ed.ProfileEditor(self, profile)
        edited_profile = profile_editor.run()
        profile_editor.destroy()

        if edited_profile is not None:
            # Replace old profile.
            self.msg_pipe.send(Message('PROFILE-EDITED',
                (edited_profile, profile)))

    def request_profile_delete(self, widget=None, data=None):
        """ Respond to a request to delete an AP profile (i.e. make the
            profile unknown).  Trying to delete an AP which is not configured
            is a NOOP.  Check with the user before deleting the profile.
            :data:`widget` is the widget sending the signal and :data:`data`
            is a list of arbitrary arguments, both are ignored.
        """
        store, selected_iter = self.plist.get_selection().get_selected()
        if selected_iter is not None:
            if store.get_value(selected_iter, 3):
                # The selected AP is configured (a.k.a. 'known').
                essid = self.pstore.get_value(selected_iter, 0)
                bssid = self.pstore.get_value(selected_iter, 1)
                if bssid == _('  Multiple APs'):
                    # AP list says this is a roaming profile
                    bssid = ''
                    profile_name = essid
                else:
                    profile_name = '{ESSID} ({BSSID})'.format(ESSID=essid,
                        BSSID=bssid)

                dialog = gtk.MessageDialog(self,
                    gtk.DIALOG_DESTROY_WITH_PARENT | gtk.DIALOG_MODAL,
                    gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO,
                    _('Are you sure you want to delete the '
                    '{NAME} profile?').format(NAME=profile_name))

                result = dialog.run()
                dialog.destroy()
                del dialog

                if result == gtk.RESPONSE_YES:
                    apname = make_section_name(essid, bssid)
                    self.msg_pipe.send(Message('PROFILE-REMOVE', apname))

    def delete_profile(self, profile):
        """ Remove :data:`profile` from the list of APs shown to the user.
        """
        if profile['roaming']:
            prow_iter = self.get_row_by_ap(profile['essid'])
        else:
            prow_iter = self.get_row_by_ap(profile['essid'], profile['bssid'])
        if prow_iter is not None:
            self.pstore.remove(prow_iter)

    def connect_profile(self, widget, profile, data=None):
        """ Respond to a request to connect to an AP.

        Parameters:

            'widget' -- gtk.Widget - The widget sending the event.

            'profile' -- dictionary - The AP profile to which to connect.

            'data' -- tuple - list of arbitrary arguments (not used)

        Returns:

            nothing
        """
        store, selected_iter = self.plist.get_selection().get_selected()
        if selected_iter is None:
            return
        essid = self.pstore.get_value(selected_iter, 0)
        bssid = self.pstore.get_value(selected_iter, 1)
        known = store.get_value(selected_iter, 3)
        if not known:
            dlg = gtk.MessageDialog(self,
                gtk.DIALOG_DESTROY_WITH_PARENT | gtk.DIALOG_MODAL,
                gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO,
                _('This network does not have a profile configured.\n\n'
                'Would you like to create one now?'))
            res = dlg.run()
            dlg.destroy()
            del dlg
            if res == gtk.RESPONSE_NO:
                return
            profile = get_new_profile()
            profile['essid'] = essid
            profile['bssid'] = bssid
            if not self.create_new_profile(widget, profile, data):
                return
        else:
            # Check for roaming profile.
            ap_name = make_section_name(essid, '')
            profile = self.config.get_profile(ap_name)
            if not profile:
                # Check for normal profile.
                ap_name = make_section_name(essid, bssid)
                profile = self.config.get_profile(ap_name)
                if not profile:
                    # No configured profile
                    return
            profile['bssid'] = self.access_points[ap_name]['bssid']
            profile['channel'] = self.access_points[ap_name]['channel']
        self.msg_pipe.send(Message('CONNECT', profile))

    def disconnect_profile(self, widget=None, data=None):
        """ Respond to a request to disconnect by sending a message to
            ConnectionManager.  :data:`widget` is the widget sending the
            signal and :data:`data` is a list of arbitrary arguments, both
            are ignored.
        """
        self.msg_pipe.send(Message('DISCONNECT', ''))
        if data == 'cancel':
            self.status_window.update_message(_('Canceling connection...'))
            if sys.modules.has_key('gtk'):
                while gtk.events_pending():
                    gtk.main_iteration(False)

    def profile_order_updater(self, model, path, iter, auto_profile_order):
        """
        """
        if model.get_value(iter, 3) is True:
            essid = self.pstore.get_value(iter, 0)
            bssid = self.pstore.get_value(iter, 1)
            if bssid == _('  Multiple APs'):
                bssid = ''
            apname = make_section_name(essid, bssid)
            auto_profile_order.append(apname)

    def update_auto_profile_order(self, widget=None, data=None, data2=None):
        """ Update the config file auto profile order from the on-screen
            order.  :data:`widget` is the widget sending the signal and
            :data:`data` and :data:`data2` is a list of arbitrary arguments,
            all are ignored.
        """
        # recreate the auto_profile_order
        auto_profile_order = []
        self.pstore.foreach(self.profile_order_updater, auto_profile_order)
        self.msg_pipe.send(Message('PROFILE-ORDER-UPDATE', auto_profile_order))


# Make so we can be imported
if __name__ == '__main__':
    pass

