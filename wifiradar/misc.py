#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   misc.py - collection of miscellaneous functions and classes
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import collections
import errno
import gettext
import logging
import os
import sys

from subprocess import check_call, Popen, PIPE, STDOUT

WIFI_RADAR_VERSION = '0.0.0'

# create a logger
logger = logging.getLogger(__name__)

__all__ = ['_', 'PYVERSION', 'WIFI_RADAR_VERSION', 'DeviceError',
           'NoDeviceError', 'PipeError', 'KeySortedDict',
           'get_new_profile', 'shellcmd']

PYVERSION = sys.version_info[0]

# Look for string translations in the default location, then in the po
# directory under the current directory.  This second location is useful
# during development.
for locale_dir in [gettext.bindtextdomain(None), os.path.realpath('./po/')]:
    try:
        trans = gettext.translation('wifi-radar', locale_dir)
    except IOError as e:
        logger.warning("{EXC} in '{DIR}'".format(EXC=e, DIR=locale_dir))
    else:
        logger.info("Translation file found in '{DIR}'".format(DIR=locale_dir))
        break
else:
    logger.warning('Translation disabled.  Please report this '
        'at http://wifi-radar.tuxfamily.org/')
    trans = gettext.NullTranslations()
# Return a reference to the unicode version of gettext, no matter
# the Python version.
if PYVERSION < 3:
    _ = trans.ugettext
else:
    _ = trans.gettext

class DeviceError(EnvironmentError):
    """ DeviceError will be raised when a problem is found while changing
        a wireless device's configuration (e.g. when 'ifconfig wlan0 up'
        fails).
    """
    pass


class NoDeviceError(EnvironmentError):
    """ NoDeviceError will be raised when a wireless device cannot be
        found.
    """
    pass


class PipeError(IOError):
    """ PipeError is raised when a communication Pipe closes unexpectedly.
    """
    pass


generic_formatter =  logging.Formatter(
    fmt='%(asctime)s.%(msecs)d %(levelname)s %(name)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')


class KeySortedDict(collections.MutableMapping):
    """ :class:`KeySortedDict` provides a dictionary whose keys are always
        returned in sorted order.  Internal storage is not affected by the
        sorting.
    """
    def __init__(self, *args, **kwargs):
        self._key_list = list()
        self._backing_dict = dict()
        self.update(*args, **kwargs)

    def __setitem__(self, key, value):
        """ ksd.__setitem__(k, v) <==> ksd[k] = v """
        if key in self:
            # Update in place.
            self._backing_dict[key] = value
        else:
            self._key_list.append(key)
            self._backing_dict[key] = value

    def __getitem__(self, key):
        """ ksd.__getitem__(k) <==> ksd[k] """
        return self._backing_dict[key]

    def __delitem__(self, key):
        """ ksd.__delitem__(k) <==> del ksd[k] """
        del self._backing_dict[key]
        self._key_list.remove(key)

    def __iter__(self):
        """ ksd.__iter__() <==> iter(ksd) """
        self._key_list.sort()
        for key in self._key_list:
            yield key

    def __len__(self):
        """ ksd.__len__ <==> len(ksd) """
        return len(self._key_list)

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, self.items())

    def copy(self):
        return self._backing_dict.copy()


def get_new_profile():
    """ Return a blank profile.

    Parameters:

        none

    Returns:

        dictionary -- An AP profile with defaults set.
    """
    return { 'known': False,
             'available': False,
             'encrypted': False,
             'essid': '',
             'bssid': '',
             'roaming': False,
             'protocol': 'g',
             'signal': -193,
             'channel': 'auto',
             'con_prescript': '',
             'con_postscript': '',
             'dis_prescript': '',
             'dis_postscript': '',
             'key': '',
             'mode': 'auto',
             'security': 'none',
             'wep_mode': '',
             'wpa_psk': '',
             'use_dhcp': True,
             'ip': '',
             'netmask': '',
             'gateway': '',
             'domain': '',
             'dns1': '',
             'dns2': ''
            }

def shellcmd(command, environment=None):
    """ Run 'command' through the shell, where 'command' is a tuple of
        the program to execute and its arguments.  'environment' is a
        dictionary of shell environment variables (as keys) and their
        values.  Returns 0 on success, otherwise, it raises
        CalledProcessError.
    """
    env_tmp = os.environ
    env_tmp.update(environment)
    return check_call(command, shell=True, env=env_tmp)


# Make so we can be imported
if __name__ == '__main__':
    pass

