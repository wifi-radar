# -*- coding: utf-8 -*-
#
#   __init__.py - main logic for operating WiFi Radar
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

from configparser import NoOptionError, NoSectionError
import logging
import logging.handlers
from multiprocessing import Pipe, Process
import signal
from threading import Thread

from wifiradar.config import (make_section_name, ConfigManager,
                              ConfigFileError, ConfigFileManager)
from wifiradar.connections import ConnectionManager, scanner
from wifiradar.pubsub import Dispatcher, Message
from wifiradar.misc import (_, PYVERSION, WIFI_RADAR_VERSION,
                            PipeError, generic_formatter, get_new_profile)
import wifiradar.gui.g2 as ui
import wifiradar.gui.g2.transients as transients

# Set up a logging framework.
logger = logging.getLogger(__name__)


class Main(object):
    """ The primary component of WiFi Radar.
    """
    def __init__(self, conf_file):
        """ Create WiFi Radar app using :data:`config` for configuration.
        """
        dispatcher = Dispatcher()
        scanner_pipe = dispatcher.subscribe(['ALL'])
        ui_pipe = dispatcher.subscribe(['ALL'])
        self.msg_pipe = dispatcher.subscribe(['ALL'])

        try:
            self.config, self.config_file_man = self.make_config(conf_file)
        except ConfigFileError as e:
            self.msg_pipe.send(Message('ERROR', e))
            logger.critical(e)
        except IOError as e:
            self.msg_pipe.send(Message('ERROR', e))
            logger.critical(e)
            self.shutdown([], dispatcher)
        else:
            if not __debug__:
                logger.setLevel(self.config.get_opt_as_int('GENERAL',
                    'loglevel'))

            try:
                fileLogHandler = logging.handlers.RotatingFileHandler(
                    self.config.get_opt('GENERAL', 'logfile'),
                    maxBytes=64*1024, backupCount=5)
            except IOError as e:
                self.msg_pipe.send(Message('ERROR',
                    _('Cannot open log file for writing: {ERR}.\n\n'
                    'WiFi Radar will work, but a log file will not '
                    'be recorded.').format(ERR=e.strerror)))
            else:
                fileLogHandler.setFormatter(generic_formatter)
                logger.addHandler(fileLogHandler)

            scanner_thread = Thread(name='scanner', target=scanner,
                args=(self.config.copy(), scanner_pipe))
            scanner_thread.start()

            ui_proc = Process(name='ui', target=ui.start, args=(ui_pipe,))
            ui_proc.start()

            # Reset SIGINT handler so that Ctrl+C in launching terminal
            # will kill the application.
            signal.signal(signal.SIGINT, signal.SIG_DFL)

            # This is the first run (or, at least, no config file was present),
            # so pop up the preferences window
            try:
                if self.config.get_opt_as_bool('GENERAL', 'new_file'):
                    self.config.remove_option('GENERAL', 'new_file')
                    config_copy = self.config.copy()
                    self.msg_pipe.send(Message('PREFS-EDIT', config_copy))
            except NoOptionError:
                pass
            # Add our known profiles in order.
            for profile_name in self.config.auto_profile_order:
                profile = self.config.get_profile(profile_name)
                self.msg_pipe.send(Message('PROFILE-UPDATE', profile))
            self.running = True
            try:
                self.run()
            finally:
                self.shutdown([ui_proc, scanner_thread], dispatcher)

    def make_config(self, conf_file):
        """ Returns a tuple with a :class:`ConfigManager` and a
            :class:`ConfigFileManager`.  These objects are built by reading
            the configuration data in :data:`conf_file`.
        """
        # Create a file manager ready to read configuration information.
        config_file_manager = ConfigFileManager(conf_file)
        try:
            config = config_file_manager.read()
        except (NameError, SyntaxError) as e:
            error_message = _('A configuration file from a pre-2.0 '
                'version of WiFi Radar was found at {FILE}.\n\nWiFi '
                'Radar v2.0.x does not read configuration files from '
                'previous versions.  ')
            if isinstance(e, NameError):
                error_message += _('Because {FILE} may contain '
                    'information that you might wish to use when '
                    'configuring WiFi Radar {VERSION}, rename this '
                    'file and run the program again.')
            elif isinstance(e, SyntaxError):
                error_message += _('The old configuration file is '
                    'probably empty and can be removed.  Rename '
                    '{FILE} if you want to be very careful.  After '
                    'removing or renaming {FILE}, run this program '
                    'again.')
            error_message = error_message.format(FILE=conf_file,
                VERSION=WIFI_RADAR_VERSION)
            raise ConfigFileError(error_message)
        except IOError as e:
            if e.errno == 2:
                # Missing user configuration file, so read the configuration
                #  defaults file.  Then setup the file manager to write to
                #  the user file.
                defaults_file = conf_file.replace('.conf', '.defaults')
                # If conf_file == defaults_file, then this is not the first
                #  time through the recursion and we should fail loudly.
                if conf_file != defaults_file:
                    config, _cfm = self.make_config(defaults_file)
                    config.set_bool_opt('GENERAL', 'new_file', True)
                    return config, ConfigFileManager(conf_file)
            # Something went unrecoverably wrong.
            raise e
        else:
            return config, config_file_manager

    def run(self):
        """ Watch for incoming messages and dispatch to subscribers.
        """
        while self.running:
            try:
                msg = self.msg_pipe.recv()
            except (EOFError, IOError) as e:
                # This is bad, really bad.
                logger.critical(_('read on closed Pipe ({PIPE}), '
                    'failing...').format(PIPE=self.msg_pipe))
                raise PipeError(e)
            else:
                self._check_message(msg)

    def shutdown(self, joinables, dispatcher):
        """ Join processes and threads in the :data:`joinables` list,
            then close :data:`dispatcher`.
        """
        for joinable in joinables:
            joinable.join()
        dispatcher.close()

    def _check_message(self, msg):
        """
        """
        if msg.topic == 'EXIT':
            self.msg_pipe.close()
            self.running = False
        elif msg.topic == 'PROFILE-ORDER-UPDATE':
            self._profile_order_update(msg.details)
        elif msg.topic == 'PROFILE-EDIT-REQUEST':
            essid, bssid = msg.details
            self._profile_edit_request(essid, bssid)
        elif msg.topic == 'PROFILE-EDITED':
            new_profile, old_profile = msg.details
            self._profile_replace(new_profile, old_profile)
        elif msg.topic == 'PROFILE-REMOVE':
            self._profile_remove(msg.details)
        elif msg.topic == 'PREFS-EDIT-REQUEST':
            self._preferences_edit_request()
        elif msg.topic == 'PREFS-UPDATE':
            self._preferences_update(msg.details)
        else:
            logger.warning(_('unrecognized Message: "{MSG}"').format(MSG=msg))

    def _profile_order_update(self, profile_order):
        """ Update the auto profile order in the configuration.
            :data:`profile_order` is a list of profile names, in order.
        """
        self.config.auto_profile_order = profile_order
        try:
            self.config_file_man.write(self.config)
        except IOError as e:
            self.msg_pipe.send(Message('ERROR',
                _('Could not save configuration file:\n'
                '{FILE}\n\n{ERR}').format(
                FILE=self.config_file_man.filename, ERR=e.strerror)))

    def _profile_edit_request(self, essid, bssid):
        """ Send a message with a profile to be edited.  If a profile with
            :data:`essid` and :data:`bssid` is found in the list of known
            profiles, that profile is sent for editing.  Otherwise, a new
            profile is sent.
        """
        apname = make_section_name(essid, bssid)
        try:
            profile = self.config.get_profile(apname)
        except NoSectionError:
            logger.info(_('The profile "{NAME}" does not exist, '
                'creating a new profile.').format(NAME=apname))
            profile = get_new_profile()
            profile['essid'] = essid
            profile['bssid'] = bssid
        self.msg_pipe.send(Message('PROFILE-EDIT', profile))

    def _profile_replace(self, new_profile, old_profile):
        """ Update :data:`old_profile` with :data:`new_profile`.
        """
        new_apname = make_section_name(new_profile['essid'],
                new_profile['bssid'])
        old_apname = make_section_name(old_profile['essid'],
                old_profile['bssid'])
        if old_apname == new_apname:
            # Simple update of old_profile with new_profile.
            self.config.set_section(new_apname, new_profile)
            self.msg_pipe.send(Message('PROFILE-UPDATE', new_profile))
        else:
            # Replace old_profile with new_profile.
            old_position = self._profile_remove(old_apname)
            # Add the updated profile like it's new...
            self.config.set_section(new_apname, new_profile)
            self.msg_pipe.send(Message('PROFILE-UPDATE', new_profile))
            if old_position is not None:
                # ..., but in the old position.
                self.config.auto_profile_order.insert(old_position, new_apname)
                self.msg_pipe.send(Message('PROFILE-MOVE', (old_position, new_profile)))
        if old_profile['known'] is False and new_profile['known'] is True:
            # The profile has been upgraded from scanned to configured.
            self.config.auto_profile_order.insert(0, new_apname)
            self.msg_pipe.send(Message('PROFILE-MOVE', (0, new_profile)))
        try:
            self.config_file_man.write(self.config)
        except IOError as e:
            self.msg_pipe.send(Message('ERROR',
                _('Could not save configuration file:\n'
                '{FILE}\n\n{ERR}').format(
                FILE=self.config_file_man.filename, ERR=e.strerror)))

    def _profile_remove(self, apname):
        """ Remove the profile named in :data:`apname`.  This method returns
            the index in the auto-profile order at which the name was found,
            or None if not matched.
        """
        try:
            position = self.config.auto_profile_order.index(apname)
        except ValueError:
            return None
        else:
            profile = self.config.get_profile(apname)
            self.config.remove_section(apname)
            self.config.auto_profile_order.remove(apname)
            self.msg_pipe.send(Message('PROFILE-UNLIST', profile))
            try:
                self.config_file_man.write(self.config)
            except IOError as e:
                self.msg_pipe.send(Message('ERROR',
                    _('Could not save configuration file:\n'
                    '{FILE}\n\n{ERR}').format(
                    FILE=self.config_file_man.filename, ERR=e.strerror)))
            return position

    def _preferences_edit_request(self):
        """ Pass a :class:`ConfigManager` to the UI for editing.
        """
        config_copy = self.config.copy()
        self.msg_pipe.send(Message('PREFS-EDIT', config_copy))

    def _preferences_update(self, config):
        """ Update configuration with :data:`config`.
        """
        self.config.update(config)
        try:
            self.config_file_man.write(self.config)
        except IOError as e:
            self.msg_pipe.send(Message('ERROR',
                _('Could not save configuration file:\n'
                '{FILE}\n\n{ERR}').format(
                FILE=self.config_file_man.filename, ERR=e.strerror)))
