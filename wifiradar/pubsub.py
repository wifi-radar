# -*- coding: utf-8 -*-
#
#   pubsub.py - publish/subscribe support
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2014 Sean Robinson <robinson@tuxfamily.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

from collections import namedtuple
import logging
from multiprocessing import Event, Pipe, RLock
import select
import threading
import time
from types import StringTypes

from wifiradar.misc import _

# create a logger
logger = logging.getLogger(__name__)


class Message(namedtuple('Message', 'topic, details, ts')):
    """ Message to be passed between WiFi Radar components.
    """
    __slots__ = ()

    def __new__(_cls, topic, details, ts=None):
        """ Build a Message to pass.
        """
        topic = topic.upper()
        ts = time.time()
        return tuple.__new__(_cls, (topic, details, ts))


def bridge(local, foreign):
    """ :func:`bridge` helps link two :class:`Dispatcher` objects.
    """
    a, b = Pipe()
    local.add_connector(a)
    foreign.add_connector(b)
    return (a, b)


class Dispatcher(object):
    """ Dispatcher provides the base infrastruture for the WiFi Radar
        publish/subscribe pattern.  One Dispatcher should run in each
        process.
    """
    def __init__(self, auto_start=True):
        """ Create an empty Dispatcher.
        """
        self.pipes = dict()
        self._pairs = dict()
        self._thread = None
        self._pipes_lock = RLock()
        self._watching = Event()
        self._watching.set()
        if auto_start:
            self.start()

    def __del__(self):
        """ Close all pipes when the Dispatcher object is garbage
            collected.
        """
        self.close()

    def close(self):
        """ End the Dispatcher.  :func:`close` joins the message processing
            thread, which may delay the return.
        """
        # Stop the run loop before shutting everything down.
        self._watching.clear()
        # Work on a pre-built list of the keys, because we are deleting keys.
        for pipe in list(self.pipes):
            self._close_connection(pipe)
        self.join()

    def subscribe(self, topics=None):
        """ Subscribe to messages with a topic in :data:`topics`.
            :data:`topics` is a **unicode** (for one topic) or an **iterable**
            (for more than one topic).
        """
        if topics is None:
            topics = list()
        if isinstance(topics, StringTypes):
            topics = [topics]
        topics = list(topics)
        topics.append('EXIT')

        a, b = Pipe()
        self._pairs[b] = a
        with self._pipes_lock:
            self.pipes[a] = topics
        return b

    def unsubscribe(self, connection):
        """ Close the subscribed pipe, :param connection:.
        """
        with self._pipes_lock:
            self._close_connection(self._pairs[connection])
            self._close_connection(connection)
            del self._pairs[connection]

    def add_connector(self, connection):
        """ Provide one side of a link between two :class:`Dispatcher`s.
            It is assumed the :func:`add_connector` method will be called
            on the other :class:`Dispatcher` with the other side of the
            Pipe.
        """
        with self._pipes_lock:
            self.pipes[connection] = ['ALL']

    def remove_connector(self, connection):
        """ Remove the :data:`connection` to another :class:`Dispatcher`.
        """
        try:
            connection.send(Message('PIPE-CLOSE', ''))
        except IOError:
            # This pipe may have been closed already.
            logger.warning('attempted to send on closed Pipe '
                '({PIPE}), continuing...'.format(PIPE=connection))
        self._close_connection(connection)

    def _close_connection(self, connection):
        """ Close the :class:`Connection` object passed in
            :data:`connection`.
        """
        with self._pipes_lock:
            connection.close()
            if connection in self.pipes:
                del self.pipes[connection]

    def _check_message(self, msg, connection):
        """ Process :data:`msg` that arrived on :data:`connection`.
        """
        if msg.topic == 'PIPE-CLOSE':
            self._close_connection(connection)

    def _run(self):
        """ Watch for incoming messages and dispatch to subscribers.
        """
        while self._watching.is_set():
            with self._pipes_lock:
                pipes = self.pipes.keys()
            rlist, wlist, xlist = select.select(pipes, [], [], 0.05)
            for rfd in rlist:
                try:
                    msg = rfd.recv()
                except (EOFError, IOError):
                    logger.warning(_('read on closed Pipe '
                        '({FD}), continuing...').format(FD=rfd))
                    self._close_connection(rfd)
                else:
                    self._check_message(msg, rfd)
                    for p,t in self.pipes.items():
                        if ((rfd is not p) and
                                ((msg.topic in t) or ('ALL' in t)) and
                                (not msg.topic.startswith('PIPE-'))):
                            p.send(msg)

    def start(self):
        """ Start running the Dispatcher's event loop in a thread.
        """
        # Only allow one event loop thread.
        if self._thread is None:
            self._thread = threading.Thread(None, self._run,
                'dispatcher_event_loop:{NAME}'.format(NAME=self), ())
            self._thread.start()

    def join(self):
        """ Stop the Dispatcher's event loop thread.
        """
        if self._thread is not None:
            self._thread.join()
            self._thread = None
