#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   config.py - support for WiFi Radar configuration
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import codecs
import configparser
import io
import logging
import os
import re
import tempfile
from shutil import move
from subprocess import Popen, PIPE, STDOUT
from types import *

from wifiradar.misc import *

if PYVERSION < 3:
    str = unicode

# create a logger
logger = logging.getLogger(__name__)


def make_section_name(essid, bssid):
    """ Returns the combined :data:`essid` and :data:`bssid` to make a
        config file section name.  :data:`essid` and :data:`bssid` are
        strings.
    """
    return essid + ':' + bssid


class ConfigFileError(StandardError):
    pass


class ConfigManager(object):
    """ Manage configuration options, grouped into sections.
        :class:`ConfigManager` is based on the standard library's
        configparser API and is meant as a less capable replacement.
    """
    def __init__(self, dict_type=KeySortedDict,
            interpolation=configparser.ExtendedInterpolation,
            defaults=None):
        """ Create a new configuration manager.

            :data:`dict_type` is the dictionary class to be used for
            internal storage.  The default is :class:`KeySortedDict`,
            but other options (such as :class:`OrderedDict`) may be used.

            :data:`interpolation` is the configparser module interpolator
            to use on configuration values.  The default and preferred
            interpolator is :class:`ExtendedInterpolation`.

            :data:`defaults` is a dictionary of options to put into the
            DEFAULT section.
        """
        self.PROFILE_RE = re.compile(r'[^\'\"\:]+(?::[\w]{2}){6}')
        self._dict_type = dict_type
        self._dict = self._dict_type()
        self._interpolation = interpolation()
        self.raw = False
        if defaults is not None:
            # The DEFAULT section is not treated specially.
            self.set_section('DEFAULT', defaults)

    def get_network_device(self):
        """ Return the network device name.

            If a device is specified in the configuration file,
            :meth:`get_network_device` returns that value.  If the
            configuration is set to "auto-detect", this method returns the
            first WiFi device as returned by iwconfig.
            :meth:`get_network_device` raises
            :exc:`wifiradar.misc.NoDeviceError` if no wireless device can
            be found in auto-detect mode.
        """
        device = self.get_opt('GENERAL', 'interface')
        if device == 'auto_detect':
            # auto detect network device
            iwconfig_command = [
                    self.get_opt('GENERAL', 'iwconfig_command'),
                    device]
            try:
                iwconfig_info = Popen(iwconfig_command, stdout=PIPE,
                                      stderr=STDOUT).stdout
                wireless_devices = list()
                for line in iwconfig_info:
                    if '802.11' in line:
                        name = line[0:line.find(' ')]
                        wireless_devices.append(name)
                # return the first device in the list
                return wireless_devices[0]
            except OSError as e:
                logger.critical(_('problem auto-detecting wireless '
                    'device using iwconfig: {EXC}').format(EXC=e))
            except IndexError:
                logger.critical(_('No WiFi device found, '
                    'please set this in the preferences.'))
                raise NoDeviceError('No WiFi device found.')
        else:
            # interface has been manually specified in configuration
            return device

    def add_section(self, section):
        """ Add :data:`section` to the configuration.
        """
        self._dict[section] = self._dict_type()

    def remove_section(self, section):
        """ Remove :data:`section` from the configuration.
        """
        del self._dict[section]

    def sections(self):
        """ Return a list of sections.
        """
        return self._dict.keys()

    def set_section(self, section, dictionary):
        """ Set the options of :data:`section` to values from
            :data:`dictionary`.

            :data:`section` will be created if it does not exist.  The keys
            of :data:`dictionary` are the options and its values are the
            option values.
        """
        for option, value in dictionary.items():
            if isinstance(value, BooleanType):
                self.set_bool_opt(section, option, value)
            elif isinstance(value, IntType):
                self.set_int_opt(section, option, value)
            elif isinstance(value, FloatType):
                self.set_float_opt(section, option, value)
            else:
                self.set_opt(section, option, value)

    def items(self, section=None, raw=False):
        """ Return a dict of the options and their values in :data:`section`.
            If :data:`section` is not given, this method returns a list of
            section names.  If :data:`raw` is `False` (default) the values
            are interpolated before being returned.
        """
        if section is None:
            return self.sections()
        section_items = dict()
        for option in self._dict[section].keys():
            section_items[option] = self.get_opt(section, option, raw)
        return section_items

    def get_profile(self, section):
        """ Return the profile values in :data:`section` as a dictionary.

            :meth:`get_profile` raises :exc:`NoSectionError` if the profile
            does not exist.
        """
        str_types = ['bssid', 'channel', 'essid', 'protocol',
                     'con_prescript', 'con_postscript', 'dis_prescript',
                     'dis_postscript', 'key', 'mode', 'security',
                     'wpa_driver', 'ip', 'netmask', 'gateway', 'domain',
                     'dns1', 'dns2']
        bool_types = ['known', 'available', 'roaming', 'encrypted',
                      'use_wpa', 'use_dhcp']
        int_types = ['signal']
        profile = get_new_profile()
        for option in bool_types:
            try:
                profile[option] = self.get_opt_as_bool(section, option)
            except configparser.NoOptionError:
                # A missing option means the default will be used.
                pass
        for option in int_types:
            try:
                profile[option] = self.get_opt_as_int(section, option)
            except configparser.NoOptionError:
                # A missing option means the default will be used.
                pass
        for option in str_types:
            try:
                profile[option] = self.get_opt(section, option)
            except configparser.NoOptionError:
                # A missing option means the default will be used.
                pass
        return profile

    def get(self, section, option, raw=None):
        """ The basis of all :meth:`get_opt*` methods.  Do not use this
            directly.
        """
        # This method is required for interpolation.
        if section not in self._dict:
            raise configparser.NoSectionError(section)
        if option not in self._dict[section]:
            raise configparser.NoOptionError(option, section)
        value = self._dict[section][option]
        if raw is None:
            raw = self.raw
        if raw:
            return value
        else:
            return self._interpolation.before_get(self, section, option,
                value, self._dict[section])

    def get_opt(self, section, option, raw=None):
        """ Return the value of :data:`option` in :data:`section`, as
            a string.

            :data:`section` and :data:`option` must be strings.

            :meth:`get_opt` raises :exc:`NoSectionError` when
            :data:`section` is unknown and :exc:`NoOptionError` when
            :data:`option` in unknown.
        """
        # False means to use interpolation when retrieving the value.
        return self.get(section, option, raw)

    def get_opt_as_bool(self, section, option):
        """ Return the value of :data:`option` in :data:`section`, as a
            boolean.

            :meth:`get_opt_as_bool` calls :meth:`get_opt` before converting
            the string to a boolean and raises :exc:`ValueError` if the
            conversion is not possible.
        """
        value = self.get(section, option)
        if value == 'True':
            return True
        elif value == 'False':
            return False
        raise ValueError('value was not True or False')

    def get_opt_as_int(self, section, option):
        """ Return the value of :data:`option` in :data:`section`, as an
            integer.

            :meth:`get_opt_as_int` calls :meth:`get_opt` before converting
            the string to an integer and raises :exc:`ValueError` if the
            conversion is not possible.
        """
        value = self.get(section, option)
        return int(value)

    def get_opt_as_float(self, section, option):
        """ Return the value of :data:`option` in :data:`section`, as a
            float.

            :meth:`get_opt_as_float` calls :meth:`get_opt` before converting
            the string to a float and raises :exc:`ValueError` if the
            conversion is not possible.
        """
        value = self.get(section, option)
        return float(value)

    def set(self, section, option, value):
        """ The basis of all :meth:`set_opt*` methods.  Do not use this
            directly.
        """
        if section not in self._dict:
            raise configparser.NoSectionError(section)
        if not isinstance(value, StringTypes):
            raise TypeError('value must be a string')
        self._dict[section][option] = value

    def set_opt(self, section, option, value):
        """ Set :data:`option` to :data:`value` in :data:`section`.

            If :data:`section`, does not exist in the configuration, it is
            created.  If :data:`section` and :data:`option` are not strings,
            each will be turned into one before being added.   Raises
            :exc:`TypeError` if :data:`value` is not a string.
        """
        section, option = str(section), str(option)
        try:
            self.set(section, option, value)
        except configparser.NoSectionError:
            self.add_section(section)
            self.set_opt(section, option, value)

    def set_bool_opt(self, section, option, value):
        """ Set :data:`option` to boolean :data:`value` in :data:`section`.

            :meth:`set_bool_opt` calls :meth:`set_opt` after converting
            :data:`value` to a string.  Raises :exc:`ValueError` if
            :data:`value` is not a boolean, where a boolean may be True,
            'True', or a number greater than 0; or False, 'False', or 0.
        """
        if isinstance(value, BooleanType):
            # use False = 0 and True = 1 to return index into tuple
            value = ('False', 'True')[value]
        elif isinstance(value, IntType):
            if value < 0:
                raise ValueError(_('boolean value must be >= 0'))
            # use False = 0 and True = 1 to return index into tuple
            value = ('False', 'True')[value > 0]
        elif isinstance(value, StringTypes):
            # convert to title case (i.e. capital first letter, only)
            value = value.title()
            if value not in ('False', 'True'):
                raise ValueError(_('value must be "True" or "False"'))
        else:
            raise ValueError(_('value cannot be converted to string'))
        self.set_opt(section, option, value)

    def set_int_opt(self, section, option, value):
        """ Set :data:`option` to integer :data:`value` in :data:`section`.

            :meth:`set_int_opt` calls :meth:`set_opt` after converting
            :data:`value` to a string.  Raises :exc:`TypeError` if
            :data:`value` is not an integer.
        """
        if not isinstance(value, IntType):
            raise TypeError(_('value is not an integer'))
        self.set_opt(section, option, str(value))

    def set_float_opt(self, section, option, value):
        """ Set :data:`option` to float :data:`value` in :data:`section`.

            :meth:`set_float_opt` calls :meth:`set_opt` after converting
            :data:`value` to a string.  Raises :exc:`TypeError` if
            :data:`value` is not a float.
        """
        if not isinstance(value, (FloatType, IntType)):
            raise TypeError(_('value is not a float or integer'))
        self.set_opt(section, option, str(float(value)))

    def profiles(self):
        """ Return a list of the section names which denote AP profiles.

            :meth:`profiles` does not return non-AP sections.
        """
        profile_list = []
        for section in self.sections():
            if ':' in section:
                profile_list.append(section)
        return profile_list

    def update(self, config_manager):
        """ Update internal configuration information using
            :data:`config_manager`.  This replaces the contents of any
            existing section and adds any sections in :data:`config_manager`
            that are not in :data:`self`; including DEFAULT and profiles.
        """
        for section in config_manager.sections():
            self.set_section(section, dict(config_manager.items(section)))
        # Make sure profiles are in auto_profile_order and vice-versa.
        auto_profile_order_copy = self.auto_profile_order[:]
        for ap in self.profiles():
            if ap not in auto_profile_order_copy:
                auto_profile_order_copy.append(ap)
        for ap in self.auto_profile_order[:]:
            if ap not in self.profiles():
                auto_profile_order_copy.remove(ap)
        self.auto_profile_order = auto_profile_order_copy

    def copy(self, profiles=True):
        """ Return a copy of the :class:`ConfigManager` object.
        """
        config_copy = ConfigManager()
        config_copy._dict = self._dict_type(self._dict)
        # If not needed, remove the profiles from the new copy.
        if not profiles:
            for section in config_copy.profiles():
                config_copy.remove_section(section)
        return config_copy

    def optionxform(self, option):
        """ Return a lowercase version of :data:`option`.  This is the same
            as :class:`configparser.ConfigParser`.
        """
        return option.lower()

    @property
    def auto_profile_order(self):
        """ Get the profile order as a list.
        """
        if 'GENERAL' in self.sections():
            return self.PROFILE_RE.findall(self.get_opt('GENERAL',
                'auto_profile_order'))
        # Return an empty list if the 'GENERAL' section does not exist.
        return list()

    @auto_profile_order.setter
    def auto_profile_order(self, profile_list):
        """ Set the profile order from :data:`profile_list`.
        """
        if isinstance(profile_list, list):
            if 'GENERAL' not in self.sections():
                self.add_section('GENERAL')
            self.set_opt('GENERAL', 'auto_profile_order', str(profile_list))

    @auto_profile_order.deleter
    def auto_profile_order(self):
        """ Clear the profile order.
        """
        if 'GENERAL' in self.sections():
            self.set_opt('GENERAL', 'auto_profile_order', '[]')


class ConfigFileManager(configparser.ConfigParser):
    """ Manage the configuration file.
    """
    def __init__(self, filename, defaults=None):
        """ Create a new configuration file at :data:`filename` with DEFAULT
            options and values in the 'defaults' dictionary.
        """
        super(ConfigFileManager, self).__init__(defaults)
        self.filename = filename

    def read(self, configuration=ConfigManager):
        """ Read configuration file from disk into instance variables.
        """
        with codecs.open(self.filename, 'r', encoding='utf8') as f:
            self.read_file(f, self.filename)
        new_config = configuration()
        new_config.update(self)
        # All profiles written to the configuration file are known.
        for profile in new_config.profiles():
            new_config.set_bool_opt(profile, 'known', True)
        return new_config

    def read_string(self, str_, configuration=ConfigManager):
        """ Read configuration data from a string into instance variables.
        """
        self.read_file(io.StringIO(str_), '')
        new_config = configuration()
        new_config.update(self)
        # All profiles written to the configuration file are known.
        for profile in new_config.profiles():
            new_config.set_bool_opt(profile, 'known', True)
        return new_config

    def write(self, config_manager):
        """ Write configuration file to disk from instance variables.

            Copied from configparser and modified to write options in
            specific order.
        """
        config_manager.set('GENERAL', 'version', WIFI_RADAR_VERSION)
        # Safely create a temporary file, ...
        (fd, tempfilename) = tempfile.mkstemp(prefix='wifi-radar.conf.')
        # ....close the file descriptor to the temporary file, ...
        os.close(fd)
        # ....and re-open the temporary file with a codec filter.
        with codecs.open(tempfilename, 'w', encoding='utf8') as fp:
            # Write a byte-encoding note on the first line.
            fp.write('# -*- coding: utf-8 -*-\n')
            # Write non-profile sections.
            for section in config_manager.sections():
                if section not in config_manager.profiles():
                    fp.write('[{SECT}]\n'.format(SECT=section))
                    for key in config_manager._dict[section].keys():
                        if key != '__name__':
                            fp.write('{KEY} = {VALUE}\n'.format(KEY=key,
                                VALUE=str(config_manager._dict[section][key]
                                            ).replace('\n', '\n\t')))
                    fp.write('\n')
            # Write profile sections.
            for section in config_manager.profiles():
                fp.write('[{SECT}]\n'.format(SECT=section))
                for key in config_manager._dict[section].keys():
                    if key != '__name__':
                        fp.write('{KEY} = {VALUE}\n'.format(KEY=key,
                            VALUE=str(config_manager._dict[section][key]
                                        ).replace('\n', '\n\t')))
                fp.write('\n')
        move(tempfilename, self.filename)


# Make so we can be imported
if __name__ == '__main__':
    pass

