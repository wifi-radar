#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#   connections.py - collection of classes for supporting WiFi connections
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2004-2005 Ahmad Baitalmal <ahmad@baitalmal.com>
#   Copyright (C) 2005 Nicolas Brouard <nicolas.brouard@mandrake.org>
#   Copyright (C) 2005-2009 Brian Elliott Finley <brian@thefinleys.com>
#   Copyright (C) 2006 David Decotigny <com.d2@free.fr>
#   Copyright (C) 2006 Simon Gerber <gesimu@gmail.com>
#   Copyright (C) 2006-2007 Joey Hurst <jhurst@lucubrate.org>
#   Copyright (C) 2012 Anari Jalakas <anari.jalakas@gmail.com>
#   Copyright (C) 2006, 2009 Ante Karamatic <ivoks@ubuntu.com>
#   Copyright (C) 2009-2010,2014 Sean Robinson <robinson@tuxfamily.org>
#   Copyright (C) 2010 Prokhor Shuchalov <p@shuchalov.ru>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

from configparser import NoOptionError, NoSectionError
import logging
import os
import re
from signal import SIGTERM
from subprocess import CalledProcessError, Popen, PIPE, STDOUT
from threading import Event
from time import sleep

from wifiradar.config import make_section_name, ConfigManager
from wifiradar.misc import *
from wifiradar.pubsub import Message

# create a logger
logger = logging.getLogger(__name__)


def get_enc_mode(use_wpa, key):
    """ Return the WiFi encryption mode based on the combination of
        'use_wpa' and 'key'.  Possible return values are 'wpa', 'wep',
        and 'none'.
    """
    if use_wpa:
        return 'wpa'
    elif key == '':
        return 'none'
    else:
        return 'wep'


def scanner(config_manager, msg_pipe):
    """ Scan for access point information and send a profile for each.
        :data:`config_manager` is a :class:`ConfigManager` object with
        minimal configuration information (i.e. device and iwlist command).
        :data:`msg_pipe` is a :class:`multiprocessing.Connection` object
        used to receive commands and report AP profiles.
    """
    # Setup our pattern matchers.  The important value must be the second
    # regular expression group in the pattern.
    patterns = dict()
    patterns['essid'] = re.compile("ESSID\s*(:|=)\s*\"([^\"]+)\"", re.I | re.M  | re.S)
    patterns['bssid'] = re.compile("Address\s*(:|=)\s*([a-zA-Z0-9:]+)", re.I | re.M  | re.S)
    patterns['protocol'] = re.compile("Protocol\s*(:|=)\s*IEEE 802.11\s*([abgn]+)", re.I | re.M  | re.S)
    patterns['mode'] = re.compile("Mode\s*(:|=)\s*([^\n]+)", re.I | re.M  | re.S)
    patterns['channel'] = re.compile("Channel\s*(:|=)*\s*(\d+)", re.I | re.M  | re.S)
    patterns['encrypted'] = re.compile("Encryption key\s*(:|=)\s*(on|off)", re.I | re.M  | re.S)
    patterns['signal'] = re.compile("Signal level\s*(:|=)\s*(-?[0-9]+)", re.I | re.M  | re.S)

    trans_enc = dict(on=True, off=False)

    running = True
    scan = True
    while running:
        if msg_pipe.poll(1):
            try:
                msg = msg_pipe.recv()
            except (EOFError, IOError) as e:
                # This is bad, really bad.
                logger.critical(_('read on closed Pipe '
                    '({PIPE}), failing...').format(PIPE=msg_pipe))
                raise PipeError(e)
            else:
                if msg.topic == 'EXIT':
                    msg_pipe.close()
                    break
                elif msg.topic == 'SCAN-START':
                    scan = True
                elif msg.topic == 'SCAN-STOP':
                    scan = False
        try:
            device = config_manager.get_network_device()
        except NoDeviceError as e:
            logger.critical(_('Wifi device not found, '
                'please set this in the preferences.'))
            scan = False
            running = False

        if scan:
            # update the signal strengths
            iwlist_command = [
                    config_manager.get_opt('GENERAL', 'iwlist_command'),
                    device, 'scan']
            try:
                scandata = Popen(iwlist_command, stdout=PIPE,
                                        stderr=STDOUT).stdout
            except OSError as e:
                logger.critical(_('iwlist command not found, '
                    'please set this in the preferences.'))
                running = False
                scan = False
            else:
                # Start with a blank profile to fill in.
                profile = get_new_profile()
                # It's cleaner to code the gathering of AP profiles
                # from bottom to top with the iwlist output.
                for line in reversed(list(scandata)):
                    line = line.strip()
                    for pattern in patterns:
                        m = patterns[pattern].search(line)
                        if m is not None:
                            profile[pattern] = m.group(2)
                            # Stop looking for more matches on this line.
                            break

                    if line.startswith('Cell '):
                        # Each AP starts with the keyword "Cell",
                        # which mean we now have one whole profile.
                        # But, first translate the 'encrypted'
                        # property to a boolean value.
                        profile['encrypted'] = trans_enc[profile['encrypted']]
                        msg_pipe.send(Message('ACCESSPOINT', profile))
                        # Restart with a blank profile to fill in.
                        profile = get_new_profile()


class ConnectionManager(object):
    """ Manage a connection; including reporting connection state,
    connecting/disconnecting from an AP, and returning current IP, ESSID, and BSSID.
    """
    def __init__(self, msg_pipe):
        """ Create a new connection manager which communicates with a
            controlling process or thread through 'msg_pipe' (a Pipe
            object).
        """
        self.msg_pipe = msg_pipe
        self._watching = Event()
        self._watching.set()

    def run(self):
        """ Watch for incoming messages.
        """
        while self._watching.is_set():
            try:
                msg = self.msg_pipe.recv()
            except (EOFError, IOError) as e:
                # This is bad, really bad.
                logger.critical(_('read on closed Pipe '
                    '({PIPE}), failing...').format(PIPE=self.msg_pipe))
                self._watching.clear()
                raise PipeError(e)
            else:
                self._check_message(msg)

    def _check_message(self, msg):
        """ Process incoming messages.
        """
        if msg.topic == 'EXIT':
            self.msg_pipe.close()
            self._watching.clear()
        elif msg.topic == 'CONFIG-UPDATE':
            # Replace configuration manager with the one in msg.details.
            self.set_config(msg.details)
        elif msg.topic == 'CONNECT':
            # Try to connect to the profile in msg.details.
            self.connect(msg.details)
        elif msg.topic == 'DISCONNECT':
            # Try to disconnect from the profile in msg.details.
            self.disconnect(msg.details)
        elif msg.topic == 'IF-CHANGE':
            # Try to connect to the profile in msg.details.
            try:
                self.if_change(msg.details)
            except DeviceError as e:
                self.msg_pipe.send(Message('ERROR', e))
            except ValueError as e:
                logger.warning(e)
        elif msg.topic == 'QUERY-IP':
            # Send out a message with the current IP address.
            self.msg_pipe.send(Message('ANN-IP', self.get_current_ip()))
        elif msg.topic == 'QUERY-ESSID':
            # Send out a message with the current ESSID.
            self.msg_pipe.send(Message('ANN-ESSID', self.get_current_essid()))
        elif msg.topic == 'QUERY-BSSID':
            # Send out a message with the current BSSID.
            self.msg_pipe.send(Message('ANN-BSSID', self.get_current_bssid()))
        else:
            logger.warning(_('unrecognized Message: "{MSG}"').format(MSG=msg))

    def set_config(self, config_manager):
        """ Set the configuration manager to 'config_manager'.  This method
            must be called before any other public method or a caller will
            likely receive an AttributeError about a missing 'config_manager'
            attribute.

            Raises TypeError if 'config_manager' is not a ConfigManager object.
        """
        if not isinstance(config_manager, ConfigManager):
            raise TypeError(_('config must be a ConfigManager object'))
        self.config = config_manager

    def _run_script(self, script_name, profile, device):
        """ Run the script (e.g. connection prescript) in 'profile' which
            is named in 'script_name'.
        """
        if profile[script_name]:
            logger.info('running {NAME}'.format(NAME=script_name))
            profile_name = make_section_name(profile['essid'], profile['bssid'])
            enc_mode = get_enc_mode(profile['use_wpa'], profile['key'])
            custom_env = {
                'WIFIRADAR_IP': self.get_current_ip(),
                'WIFIRADAR_ESSID': self.get_current_essid(),
                'WIFIRADAR_BSSID': self.get_current_bssid(),
                'WIFIRADAR_PROFILE': profile_name,
                'WIFIRADAR_ENCMODE': enc_mode,
                'WIFIRADAR_SECMODE': profile['security'],
                'WIFIRADAR_IF': device}
            try:
                shellcmd(profile[script_name].split(' '), custom_env)
            except CalledProcessError as e:
                logger.error(_('script "{NAME}" failed: {EXC}').format(
                    NAME=script_name, EXC=e))
                self.msg_pipe.send(Message('ERROR',
                    _('script "{NAME}" failed: {EXC}').format(
                    NAME=script_name, EXC=e)))

    def _prepare_nic(self, profile, device):
        """ Configure the NIC for upcoming connection.
        """
        # Start building iwconfig command line.
        iwconfig_command = [
                self.config.get_opt('GENERAL', 'iwconfig_command'),
                device,
                'essid', "'{ESSID}'".format(ESSID=profile['essid']),
                ]

        # Setting key
        iwconfig_command.append('key')
        if (not profile['key']) or (profile['key'] == 's:'):
            iwconfig_command.append('off')
        else:
            # Setting this stops association from working, so remove it for now
            #if profile['security'] != '':
                #iwconfig_command.append(profile['security'])
            iwconfig_command.append("'{KEY}'".format(KEY=profile['key']))
            #iwconfig_commands.append( "key %s %s" % ( profile['security'], profile['key'] ) )

        # Set mode.
        profile['mode'] = profile['mode'].lower()
        if profile['mode'] == 'master' or profile['mode'] == 'auto':
            profile['mode'] = 'managed'
        iwconfig_command.extend(['mode', profile['mode']])

        # Set channel.
        if 'channel' in profile:
            iwconfig_command.extend(['channel', profile['channel']])

        # Set AP address (do this last since iwconfig seems to want it only there).
        iwconfig_command.extend(['ap', profile['bssid']])

        # Some cards require a commit
        if self.config.get_opt_as_bool('GENERAL', 'commit_required'):
            iwconfig_command.append('commit')

        logger.debug('iwconfig_command: {IWC}'.format(IWC=iwconfig_command))

        try:
            shellcmd(iwconfig_command)
        except CalledProcessError as e:
            logger.error('Failed to prepare NIC: {EXC}'.format(EXC=e))
            self.msg_pipe.send(Message('ERROR',
                _('Failed to prepare NIC: {EXC}').format(EXC=e)))
            raise DeviceError(_('Could not configure wireless options.'))

    def _stop_dhcp(self, device):
        """ Stop any DHCP client daemons running with our 'device'.
        """
        logger.info(_('Stopping any DHCP clients on "{DEV}"').format(DEV=device))
        if os.access(self.config.get_opt('DHCP', 'pidfile'), os.R_OK):
            if self.config.get_opt('DHCP', 'kill_args'):
                dhcp_command = [self.config.get_opt('DHCP', 'command')]
                dhcp_command.extend(self.config.get_opt('DHCP', 'kill_args').split(' '))
                dhcp_command.append(device)
                logger.info(_('DHCP command: {DHCP}').format(DHCP=dhcp_command))

                # call DHCP client command and wait for return
                logger.info(_('Stopping DHCP with kill_args'))
                try:
                    shellcmd(dhcp_command)
                except CalledProcessError as e:
                    logger.error(_('Attempt to stop DHCP failed: '
                        '{EXC}').format(EXC=e))
            else:
                logger.info(_('Stopping DHCP manually...'))
                os.kill(int(open(self.config.get_opt('DHCP', 'pidfile'), mode='r').readline()), SIGTERM)

    def _start_dhcp(self, device):
        """ Start a DHCP client daemon on 'device'.
        """
        logger.debug('Starting DHCP command on "{DEV}"'.format(DEV=device))
        self.msg_pipe.send(Message('STATUS', _('Acquiring IP Address (DHCP)')))
        dhcp_command = [self.config.get_opt('DHCP', 'command')]
        dhcp_command.extend(self.config.get_opt('DHCP', 'args').split(' '))
        dhcp_command.append(device)
        logger.info(_('dhcp_command: {DHCP}').format(DHCP=dhcp_command))
        try:
            dhcp_proc = Popen(dhcp_command, stdout=None, stderr=None)
        except OSError as e:
            if e.errno == 2:
                logger.critical(_('DHCP client not found, '
                    'please set this in the preferences.'))
        else:
            # The DHCP client daemon should timeout on its own, hence
            # the +3 seconds on timeout so we don't cut the daemon off
            # while it is finishing up.
            timeout = self.config.get_opt_as_int('DHCP', 'timeout') + 3
            tick = 0.25
            dhcp_status = dhcp_proc.poll()
            while dhcp_status is None:
                if timeout < 0:
                    dhcp_proc.terminate()
                else:
                    timeout = timeout - tick
                    sleep(tick)
                dhcp_status = dhcp_proc.poll()
            if self.get_current_ip() is not None:
                self.msg_pipe.send(Message('STATUS',
                    _('Got IP address. Done.')))
            else:
                self.msg_pipe.send(Message('STATUS',
                    _('Could not get IP address!')))

    def _stop_wpa(self):
        """ Stop all WPA supplicants.
        """
        logger.info(_('Kill off any existing WPA supplicants running...'))
        if os.access(self.config.get_opt('WPA', 'pidfile'), os.R_OK):
            logger.info(_('Killing existing WPA supplicant...'))
            try:
                if self.config.get_opt('WPA', 'kill_command'):
                    wpa_command = [self.config.get_opt('WPA', 'kill_command').split(' ')]
                    try:
                        shellcmd(wpa_command)
                    except CalledProcessError as e:
                        logger.error(_('Attempt to stop WPA supplicant '
                            'failed: {EXC}').format(EXC=e))
                else:
                    os.kill(int(open(self.config.get_opt('WPA', 'pidfile'), mode='r').readline()), SIGTERM)
            except OSError:
                logger.info(_('Failed to kill WPA supplicant.'))

    def _start_wpa(self):
        """ Start WPA supplicant and let it associate with the AP.
        """
        self.msg_pipe.send(Message('STATUS', _('WPA supplicant starting')))

        wpa_command = [self.config.get_opt('WPA', 'command')]
        wpa_command.extend(self.config.get_opt('WPA', 'args').split(' '))
        try:
            shellcmd(wpa_command)
        except CalledProcessError as e:
            logger.error(_('WPA supplicant failed to start: '
                '{EXC}').format(EXC=e))

    def _start_manual_network(self, profile, device):
        """ Manually configure network settings after association.
        """
        # Bring down the interface before trying to make changes.
        ifconfig_command = [
            self.config.get_opt('GENERAL', 'ifconfig_command'),
            device, 'down']
        try:
            shellcmd(ifconfig_command)
        except CalledProcessError as e:
            logger.error(_('Device "{DEV}" failed to go down: '
                '{EXC}').format(DEV=device, EXC=e))
        # Bring the interface up with our manual IP.
        ifconfig_command = [
            self.config.get_opt('GENERAL', 'ifconfig_command'),
            device, profile['ip'],
            'netmask', profile['netmask']]
        try:
            shellcmd(ifconfig_command)
        except CalledProcessError as e:
            logger.error(_('Device "{DEV}" failed to configure: '
                '{EXC}').format(DEV=device, EXC=e))
        # Configure routing information.
        route_command = [
            self.config.get_opt('GENERAL', 'route_command'),
            'add', 'default', 'gw', profile['gateway']]
        try:
            shellcmd(route_command)
        except CalledProcessError as e:
            logger.error(_('Failed to configure routing information: '
                '{EXC}').format(EXC=e))
        # Build the /etc/resolv.conf file, if needed.
        resolv_contents = ''
        if profile['domain']:
            resolv_contents += 'domain {DOM}\n'.format(DOM=profile['domain'])
        if profile['dns1']:
            resolv_contents += 'nameserver {NS1}\n'.format(NS1=profile['dns1'])
        if profile['dns2']:
            resolv_contents += 'nameserver {NS2}\n'.format(NS2=profile['dns2'])
        if resolv_contents:
            with open('/etc/resolv.conf', 'w') as resolv_file:
                resolv_file.write(resolv_contents)

    def connect(self, profile):
        """ Connect to the access point specified by 'profile'.
        """
        if not profile['bssid']:
            raise ValueError('missing BSSID')
        logger.info(_('Connecting to the {ESSID} ({BSSID}) network').format(
            ESSID=profile['essid'], BSSID=profile['bssid']))

        device = self.config.get_network_device()

        # Ready to dance...
        self.msg_pipe.send(Message('STATUS', 'starting con_prescript'))
        self._run_script('con_prescript', profile, device)
        self.msg_pipe.send(Message('STATUS', 'con_prescript has run'))

        self._prepare_nic(profile, device)

        self._stop_dhcp(device)
        self._stop_wpa()

        logger.debug(_('Disable scan while connection attempt in progress...'))
        self.msg_pipe.send(Message('SCAN-STOP', ''))

        if profile['use_wpa'] :
            self._start_wpa()

        if profile['use_dhcp'] :
            self._start_dhcp(device)
        else:
            self._start_manual_network(profile, device)

        # Begin scanning again
        self.msg_pipe.send(Message('SCAN-START', ''))

        # Run the connection postscript.
        self.msg_pipe.send(Message('STATUS', 'starting con_postscript'))
        self._run_script('con_postscript', profile, device)
        self.msg_pipe.send(Message('STATUS', 'con_postscript has run'))

        self.msg_pipe.send(Message('STATUS', 'connected'))

    def disconnect(self, profile):
        """ Disconnect from the AP with which a connection has been
            established/attempted.
        """
        logger.info(_('Disconnecting...'))

        # Pause scanning while manipulating card
        self.msg_pipe.send(Message('SCAN-STOP', ''))

        device = self.config.get_network_device()

        self.msg_pipe.send(Message('STATUS', 'starting dis_prescript'))
        self._run_script('dis_prescript', profile, device)
        self.msg_pipe.send(Message('STATUS', 'dis_prescript has run'))

        self._stop_dhcp(device)

        self._stop_wpa()

        # Clear out the wireless stuff.
        iwconfig_command = [
            self.config.get_opt('GENERAL.iwconfig_command'),
            device,
            'essid', 'any',
            'key', 'off',
            'mode', 'managed',
            'channel', 'auto',
            'ap', 'off']
        try:
            shellcmd(iwconfig_command)
        except CalledProcessError as e:
            logger.error(_('Failed to clean up wireless configuration: '
                '{EXC}').format(EXC=e))

        # Since it may be brought back up by the next scan, unset its IP.
        ifconfig_command = [
            self.config.get_opt('GENERAL.ifconfig_command'),
            device,
            '0.0.0.0']
        try:
            shellcmd(ifconfig_command)
        except CalledProcessError as e:
            logger.error(_('Failed to unset IP address: {EXC}').format(EXC=e))

        # Now take the interface down.  Taking down the interface too
        # quickly can crash my system, so pause a moment.
        sleep(0.25)
        self.if_change('down')

        self.msg_pipe.send(Message('STATUS', 'starting dis_postscript'))
        self._run_script('dis_postscript', profile, device)
        self.msg_pipe.send(Message('STATUS', 'dis_postscript has run'))

        logger.info(_('Disconnect complete.'))

        # Begin scanning again
        self.msg_pipe.send(Message('SCAN-START', ''))

    def if_change(self, state):
        """ Change the interface to 'state', i.e. 'up' or 'down'.

            'if_change' raises ValueError if 'state' is not recognized,
            raises OSError if there is a problem running ifconfig, and
            raises DeviceError if ifconfig reports the change failed.
        """
        state = state.lower()
        if ((state == 'up') or (state == 'down')):
            device = self.config.get_network_device()
            if device:
                ifconfig_command = [
                        self.config.get_opt('GENERAL', 'ifconfig_command'),
                        device, state]
                try:
                    logger.info('changing interface {DEV} '
                        'state to {STATE}'.format(DEV=device, STATE=state))
                    ifconfig_info = Popen(ifconfig_command, stdout=PIPE,
                                          stderr=STDOUT).stdout
                except OSError as e:
                    if e.errno == 2:
                        logger.critical(_('ifconfig command not found, '
                            'please set this in the preferences.'))
                    raise e
                else:
                    for line in ifconfig_info:
                        if len(line) > 0:
                            raise DeviceError(_('Could not change '
                                'device state: {ERR}').format(ERR=line))
        else:
            raise ValueError(_('unrecognized state for device: '
                '{STATE}').format(STATE=state))

    def get_current_ip(self):
        """ Return the current IP address as a string or None.
        """
        device = self.config.get_network_device()
        if device:
            ifconfig_command = [
                    self.config.get_opt('GENERAL', 'ifconfig_command'),
                    device]
            try:
                ifconfig_info = Popen(ifconfig_command, stdout=PIPE).stdout
                # Be careful to the language (inet adr: in French for example)
                #
                # Hi Brian
                #
                # I'm using wifi-radar on a system with German translations (de_CH-UTF-8).
                # There the string in ifconfig is inet Adresse for the IP which isn't
                # found by the current get_current_ip function in wifi-radar. I changed
                # the according line (#289; gentoo, v1.9.6-r1) to
                # >ip_re = re.compile(r'inet [Aa]d?dr[^.]*:([^.]*\.[^.]*\.[^.]*\.[0-9]*)')
                # which works on my system (LC_ALL=de_CH.UTF-8) and still works with LC_ALL=C.
                #
                # I'd be happy if you could incorporate this small change because as now
                # I've got to change the file every time it is updated.
                #
                # Best wishes
                #
                # Simon
                ip_re = re.compile(r'inet [Aa]d?dr[^.]*:([^.]*\.[^.]*\.[^.]*\.[0-9]*)')
                line = ifconfig_info.read()
                if ip_re.search(line):
                    return ip_re.search(line).group(1)
            except OSError as e:
                if e.errno == 2:
                    logger.critical(_('ifconfig command not found, '
                        'please set this in the preferences.'))
        return None

    def get_current_essid(self):
        """ Return the current ESSID as a string or None.
        """
        device = self.config.get_network_device()
        if device:
            iwconfig_command = [
                    self.config.get_opt('GENERAL', 'iwconfig_command'),
                    device]
            try:
                iwconfig_info = Popen(iwconfig_command, stdout=PIPE, stderr=STDOUT).stdout
                essid_re = re.compile(r'ESSID\s*(:|=)\s*"([^"]+)"',
                    re.I | re.M  | re.S)
                line = iwconfig_info.read()
                if essid_re.search(line):
                    return essid_re.search(line).group(2)
            except OSError as e:
                if e.errno == 2:
                    logger.critical(_('iwconfig command not found, '
                        'please set this in the preferences.'))
        return None

    def get_current_bssid(self):
        """ Return the current BSSID as a string or None.
        """
        device = self.config.get_network_device()
        if device:
            iwconfig_command = [
                    self.config.get_opt('GENERAL', 'iwconfig_command'),
                    device]
            try:
                iwconfig_info = Popen(iwconfig_command, stdout=PIPE, stderr=STDOUT).stdout
                bssid_re = re.compile(r'Access Point\s*(:|=)\s*([a-fA-F0-9:]{17})',
                    re.I | re.M  | re.S )
                line = iwconfig_info.read()
                if bssid_re.search(line):
                    return bssid_re.search(line).group(2)
            except OSError as e:
                if e.errno == 2:
                    logger.critical(_('iwconfig command not found, '
                        'please set this in the preferences.'))
        return None


# Make so we can be imported
if __name__ == '__main__':
    pass
