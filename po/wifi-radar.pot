# Message strings for WiFi Radar
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: wifi-radar-2.1.x\n"
"POT-Creation-Date: 2014-04-13 07:43+MST\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: UTF-8\n"
"Generated-By: pygettext.py 1.5\n"


#: wifi-radar:132
msgid ""
"A configuration file from a pre-2.0 version of WiFi Radar was found at {FILE}.\n"
"\n"
"WiFi Radar v2.0.x does not read configuration files from previous versions.  "
msgstr ""

#: wifi-radar:136
msgid "Because {FILE} may contain information that you might wish to use when configuring WiFi Radar {VERSION}, rename this file and run the program again."
msgstr ""

#: wifi-radar:140
msgid "The old configuration file is probably empty and can be removed.  Rename {FILE} if you want to be very careful.  After removing or renaming {FILE}, run this program again."
msgstr ""

#: wifi-radar:159
msgid "WiFi Radar version {VERSION}"
msgstr ""

#: wifi-radar:161
msgid ""
"WiFi Radar version {VERSION}\n"
"For help, check man pages for wifi-radar and wifi-radar.conf,\n"
"or visit http://wifi-radar.tuxfamily.org/"
msgstr ""

#: wifiradar/__init__.py:74
msgid ""
"Cannot open log file for writing: {ERR}.\n"
"\n"
"WiFi Radar will work, but a log file will not be recorded."
msgstr ""

#: wifiradar/__init__.py:119 wifiradar/connections.py:99
#: wifiradar/connections.py:177 wifiradar/gui/g2/__init__.py:263
msgid "read on closed Pipe ({PIPE}), failing..."
msgstr ""

#: wifiradar/__init__.py:146 wifiradar/connections.py:217
#: wifiradar/gui/g2/__init__.py:306
msgid "unrecognized Message: \"{MSG}\""
msgstr ""

#: wifiradar/__init__.py:157 wifiradar/__init__.py:207
#: wifiradar/__init__.py:229 wifiradar/__init__.py:248
msgid ""
"Could not save configuration file:\n"
"{FILE}\n"
"\n"
"{ERR}"
msgstr ""

#: wifiradar/__init__.py:171
msgid "The profile \"{NAME}\" does not exist, creating a new profile."
msgstr ""

#: wifiradar/config.py:112
msgid "problem auto-detecting wireless device using iwconfig: {EXC}"
msgstr ""

#: wifiradar/config.py:115
msgid "No WiFi device found, please set this in the preferences."
msgstr ""

#: wifiradar/config.py:228
msgid "boolean value must be >= 0"
msgstr ""

#: wifiradar/config.py:235
msgid "value must be 'True' or 'False'"
msgstr ""

#: wifiradar/config.py:237
msgid "value cannot be converted to string"
msgstr ""

#: wifiradar/config.py:247
msgid "value is not an integer"
msgstr ""

#: wifiradar/config.py:257
msgid "value is not a float or integer"
msgstr ""

#: wifiradar/connections.py:113
msgid "Wifi device not found, please set this in the preferences."
msgstr ""

#: wifiradar/connections.py:127
msgid "iwlist command not found, please set this in the preferences."
msgstr ""

#: wifiradar/connections.py:228
msgid "config must be a ConfigManager object"
msgstr ""

#: wifiradar/connections.py:301
msgid "Failed to prepare NIC: {EXC}"
msgstr ""

#: wifiradar/connections.py:302
msgid "Could not configure wireless options."
msgstr ""

#: wifiradar/connections.py:307
msgid "Stopping any DHCP clients on \"{DEV}\""
msgstr ""

#: wifiradar/connections.py:313
msgid "DHCP command: {DHCP}"
msgstr ""

#: wifiradar/connections.py:316
msgid "Stopping DHCP with kill_args"
msgstr ""

#: wifiradar/connections.py:320
msgid "Attempt to stop DHCP failed: {EXC}"
msgstr ""

#: wifiradar/connections.py:323
msgid "Stopping DHCP manually..."
msgstr ""

#: wifiradar/connections.py:330
msgid "Acquiring IP Address (DHCP)"
msgstr ""

#: wifiradar/connections.py:334
msgid "dhcp_command: {DHCP}"
msgstr ""

#: wifiradar/connections.py:339
msgid "DHCP client not found, please set this in the preferences."
msgstr ""

#: wifiradar/connections.py:357
msgid "Got IP address. Done."
msgstr ""

#: wifiradar/connections.py:360
msgid "Could not get IP address!"
msgstr ""

#: wifiradar/connections.py:365
msgid "Kill off any existing WPA supplicants running..."
msgstr ""

#: wifiradar/connections.py:367
msgid "Killing existing WPA supplicant..."
msgstr ""

#: wifiradar/connections.py:374
msgid "Attempt to stop WPA supplicant failed: {EXC}"
msgstr ""

#: wifiradar/connections.py:379
msgid "Failed to kill WPA supplicant."
msgstr ""

#: wifiradar/connections.py:384
msgid "WPA supplicant starting"
msgstr ""

#: wifiradar/connections.py:391
msgid "WPA supplicant failed to start: {EXC}"
msgstr ""

#: wifiradar/connections.py:404
msgid "Device \"{DEV}\" failed to go down: {EXC}"
msgstr ""

#: wifiradar/connections.py:414
msgid "Device \"{DEV}\" failed to configure: {EXC}"
msgstr ""

#: wifiradar/connections.py:423
msgid "Failed to configure routing information: {EXC}"
msgstr ""

#: wifiradar/connections.py:442
msgid "Connecting to the {ESSID} ({BSSID}) network"
msgstr ""

#: wifiradar/connections.py:457
msgid "Disable scan while connection attempt in progress..."
msgstr ""

#: wifiradar/connections.py:482
msgid "Disconnecting..."
msgstr ""

#: wifiradar/connections.py:509
msgid "Failed to clean up wireless configuration: {EXC}"
msgstr ""

#: wifiradar/connections.py:520
msgid "Failed to unset IP address: {EXC}"
msgstr ""

#: wifiradar/connections.py:531
msgid "Disconnect complete."
msgstr ""

#: wifiradar/connections.py:557 wifiradar/connections.py:602
msgid "ifconfig command not found, please set this in the preferences."
msgstr ""

#: wifiradar/connections.py:563
msgid "Could not change device state: {ERR}"
msgstr ""

#: wifiradar/connections.py:566
msgid "unrecognized state for device: {STATE}"
msgstr ""

#: wifiradar/connections.py:623 wifiradar/connections.py:644
#: wifiradar/gui/g2/prefs.py:88
msgid "iwconfig command not found, please set this in the preferences."
msgstr ""

#: wifiradar/gui/g2/__init__.py:125
msgid "Close"
msgstr ""

#: wifiradar/gui/g2/__init__.py:128
msgid "About"
msgstr ""

#: wifiradar/gui/g2/__init__.py:131
msgid "Preferences"
msgstr ""

#: wifiradar/gui/g2/__init__.py:149
msgid "Access Point"
msgstr ""

#: wifiradar/gui/g2/__init__.py:155
msgid "Signal"
msgstr ""

#: wifiradar/gui/g2/__init__.py:161 wifiradar/gui/g2/profile.py:104
msgid "Mode"
msgstr ""

#: wifiradar/gui/g2/__init__.py:173 wifiradar/gui/g2/profile.py:118
msgid "Channel"
msgstr ""

#: wifiradar/gui/g2/__init__.py:189
msgid "_New"
msgstr ""

#: wifiradar/gui/g2/__init__.py:193
msgid "C_onfigure"
msgstr ""

#: wifiradar/gui/g2/__init__.py:198
msgid "_Delete"
msgstr ""

#: wifiradar/gui/g2/__init__.py:203
msgid "Co_nnect"
msgstr ""

#: wifiradar/gui/g2/__init__.py:206
msgid "D_isconnect"
msgstr ""

#: wifiradar/gui/g2/__init__.py:337
msgid "Not Connected."
msgstr ""

#: wifiradar/gui/g2/__init__.py:339
msgid ""
"Connected to {PROFILE}\n"
"IP Address {IP}"
msgstr ""

#: wifiradar/gui/g2/__init__.py:363 wifiradar/gui/g2/__init__.py:447
#: wifiradar/gui/g2/__init__.py:510 wifiradar/gui/g2/__init__.py:540
#: wifiradar/gui/g2/__init__.py:643
msgid "  Multiple APs"
msgstr ""

#: wifiradar/gui/g2/__init__.py:494
msgid "Cannot save empty ESSID"
msgstr ""

#: wifiradar/gui/g2/__init__.py:551
msgid "Are you sure you want to delete the {NAME} profile?"
msgstr ""

#: wifiradar/gui/g2/__init__.py:597
msgid ""
"This network does not have a profile configured.\n"
"\n"
"Would you like to create one now?"
msgstr ""

#: wifiradar/gui/g2/__init__.py:632
msgid "Canceling connection..."
msgstr ""

#: wifiradar/gui/g2/prefs.py:56
msgid "WiFi Radar Preferences"
msgstr ""

#: wifiradar/gui/g2/prefs.py:74
msgid "Auto-detect wireless device"
msgstr ""

#: wifiradar/gui/g2/prefs.py:76 wifiradar/gui/g2/prefs.py:95
#: wifiradar/gui/g2/prefs.py:102 wifiradar/gui/g2/prefs.py:363
#: wifiradar/gui/g2/prefs.py:365
msgid "auto_detect"
msgstr ""

#: wifiradar/gui/g2/prefs.py:78
msgid "Automatically select wireless device to configure"
msgstr ""

#: wifiradar/gui/g2/prefs.py:98
msgid "Wireless device"
msgstr ""

#: wifiradar/gui/g2/prefs.py:120
msgid "Commit required"
msgstr ""

#: wifiradar/gui/g2/prefs.py:122
msgid "Check this box if your card requires a \"commit\" command with iwconfig"
msgstr ""

#: wifiradar/gui/g2/prefs.py:127
msgid "Ifup required"
msgstr ""

#: wifiradar/gui/g2/prefs.py:129
msgid "Check this box if your system requires the interface to be brought up first"
msgstr ""

#: wifiradar/gui/g2/prefs.py:133 wifiradar/gui/g2/profile.py:142
msgid "General"
msgstr ""

#: wifiradar/gui/g2/prefs.py:143
msgid "The command to use to configure the network card"
msgstr ""

#: wifiradar/gui/g2/prefs.py:146
msgid "Network interface configure command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:156
msgid "The command to use to configure the wireless connection"
msgstr ""

#: wifiradar/gui/g2/prefs.py:159
msgid "Wireless connection configure command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:169
msgid "The command to use to scan for access points"
msgstr ""

#: wifiradar/gui/g2/prefs.py:172
msgid "Wireless scanning command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:182
msgid "The command to use to configure the network routing"
msgstr ""

#: wifiradar/gui/g2/prefs.py:185
msgid "Network route configure command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:195
msgid "The file in which to save logging info"
msgstr ""

#: wifiradar/gui/g2/prefs.py:198
msgid "Log file"
msgstr ""

#: wifiradar/gui/g2/prefs.py:211
msgid "How much detail to save in the log file.  Larger numbers provide less detail and smaller numbers, more detail."
msgstr ""

#: wifiradar/gui/g2/prefs.py:215
msgid "Log level"
msgstr ""

#: wifiradar/gui/g2/prefs.py:220
msgid "Advanced"
msgstr ""

#: wifiradar/gui/g2/prefs.py:229
msgid "The command to use for automatic network configuration"
msgstr ""

#: wifiradar/gui/g2/prefs.py:232 wifiradar/gui/g2/prefs.py:286
msgid "Command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:240
msgid "The start-up arguments to the DHCP command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:243 wifiradar/gui/g2/prefs.py:297
msgid "Arguments"
msgstr ""

#: wifiradar/gui/g2/prefs.py:249
msgid "The shutdown arguments to the DHCP command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:252
msgid "Kill arguments"
msgstr ""

#: wifiradar/gui/g2/prefs.py:258
msgid "The amount of time DHCP will spend trying to connect"
msgstr ""

#: wifiradar/gui/g2/prefs.py:261
msgid "DHCP connect timeout (seconds)"
msgstr ""

#: wifiradar/gui/g2/prefs.py:267
msgid "The file DHCP uses to store its PID"
msgstr ""

#: wifiradar/gui/g2/prefs.py:270 wifiradar/gui/g2/prefs.py:331
msgid "PID file"
msgstr ""

#: wifiradar/gui/g2/prefs.py:274
msgid "DHCP"
msgstr ""

#: wifiradar/gui/g2/prefs.py:283
msgid "The command to use for WPA encrypted connections"
msgstr ""

#: wifiradar/gui/g2/prefs.py:294
msgid "The start-up arguments to the WPA command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:303
msgid "The shutdown arguments to the WPA command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:306
msgid "Kill command"
msgstr ""

#: wifiradar/gui/g2/prefs.py:312
msgid "The WPA configuration file to use"
msgstr ""

#: wifiradar/gui/g2/prefs.py:314
msgid "Configuration file"
msgstr ""

#: wifiradar/gui/g2/prefs.py:320
msgid "The WPA driver to use"
msgstr ""

#: wifiradar/gui/g2/prefs.py:322
msgid "Driver"
msgstr ""

#: wifiradar/gui/g2/prefs.py:328
msgid "The file WPA uses to store its PID"
msgstr ""

#: wifiradar/gui/g2/prefs.py:335 wifiradar/gui/g2/profile.py:151
msgid "WPA"
msgstr ""

#: wifiradar/gui/g2/profile.py:53
msgid "WiFi Profile"
msgstr ""

#: wifiradar/gui/g2/profile.py:58
msgid "Ad-Hoc"
msgstr ""

#: wifiradar/gui/g2/profile.py:58
msgid "Managed"
msgstr ""

#: wifiradar/gui/g2/profile.py:58
msgid "Master"
msgstr ""

#: wifiradar/gui/g2/profile.py:58 wifiradar/gui/g2/profile.py:61
#: wifiradar/gui/g2/profile.py:333 wifiradar/gui/g2/profile.py:338
msgid "auto"
msgstr ""

#: wifiradar/gui/g2/profile.py:59
msgid "Monitor"
msgstr ""

#: wifiradar/gui/g2/profile.py:59
msgid "Repeater"
msgstr ""

#: wifiradar/gui/g2/profile.py:59
msgid "Secondary"
msgstr ""

#: wifiradar/gui/g2/profile.py:60
msgid "open"
msgstr ""

#: wifiradar/gui/g2/profile.py:60
msgid "restricted"
msgstr ""

#: wifiradar/gui/g2/profile.py:77
msgid "Network Name"
msgstr ""

#: wifiradar/gui/g2/profile.py:83
msgid "The name of the network with which to connect."
msgstr ""

#: wifiradar/gui/g2/profile.py:87
msgid "Network Address"
msgstr ""

#: wifiradar/gui/g2/profile.py:94
msgid "The address of the network with which to connect."
msgstr ""

#: wifiradar/gui/g2/profile.py:97
msgid "Roaming"
msgstr ""

#: wifiradar/gui/g2/profile.py:101
msgid "Use the AP in this network which provides strongest signal?"
msgstr ""

#: wifiradar/gui/g2/profile.py:115
msgid "Method to use for connection.  You probably want auto mode."
msgstr ""

#: wifiradar/gui/g2/profile.py:129
msgid "Channel the network uses.  You probably want auto mode."
msgstr ""

#: wifiradar/gui/g2/profile.py:134 wifiradar/gui/g2/profile.py:386
msgid "Security: {SEC}"
msgstr ""

#: wifiradar/gui/g2/profile.py:138
msgid "IP Configuration: DHCP"
msgstr ""

#: wifiradar/gui/g2/profile.py:140
msgid "IP Configuration: Manual"
msgstr ""

#: wifiradar/gui/g2/profile.py:149
msgid "None"
msgstr ""

#: wifiradar/gui/g2/profile.py:150
msgid "WEP"
msgstr ""

#: wifiradar/gui/g2/profile.py:166
msgid "Hex Key"
msgstr ""

#: wifiradar/gui/g2/profile.py:178
msgid "WEP key: Plain language or hex string to use for encrypted communication with the network."
msgstr ""

#: wifiradar/gui/g2/profile.py:181
msgid "WEP Mode"
msgstr ""

#: wifiradar/gui/g2/profile.py:194
msgid "Use Open to allow unencrypted communication and Restricted to force encrypted-only communication."
msgstr ""

#: wifiradar/gui/g2/profile.py:200
msgid "PSK"
msgstr ""

#: wifiradar/gui/g2/profile.py:207
msgid "Security"
msgstr ""

#: wifiradar/gui/g2/profile.py:212
msgid "Automatic Network Configuration (DHCP)"
msgstr ""

#: wifiradar/gui/g2/profile.py:225
msgid "IP"
msgstr ""

#: wifiradar/gui/g2/profile.py:229
msgid "Netmask"
msgstr ""

#: wifiradar/gui/g2/profile.py:233
msgid "Gateway"
msgstr ""

#: wifiradar/gui/g2/profile.py:237
msgid "Domain"
msgstr ""

#: wifiradar/gui/g2/profile.py:241
msgid "First DNS"
msgstr ""

#: wifiradar/gui/g2/profile.py:245
msgid "Second DNS"
msgstr ""

#: wifiradar/gui/g2/profile.py:251
msgid "IP Configuration"
msgstr ""

#: wifiradar/gui/g2/profile.py:258
msgid "Before Connection"
msgstr ""

#: wifiradar/gui/g2/profile.py:262
msgid "After Connection"
msgstr ""

#: wifiradar/gui/g2/profile.py:269
msgid "The local command to execute before trying to connect to the network."
msgstr ""

#: wifiradar/gui/g2/profile.py:274
msgid "The local command to execute after connecting to the network."
msgstr ""

#: wifiradar/gui/g2/profile.py:277
msgid "Before Disconnection"
msgstr ""

#: wifiradar/gui/g2/profile.py:280
msgid "After Disconnection"
msgstr ""

#: wifiradar/gui/g2/profile.py:287
msgid "The local command to execute before disconnecting from the network."
msgstr ""

#: wifiradar/gui/g2/profile.py:292
msgid "The local command to execute after disconnecting from the network."
msgstr ""

#: wifiradar/gui/g2/profile.py:294
msgid "Scripting"
msgstr ""

#: wifiradar/gui/g2/profile.py:324
msgid "Invalid hex WEP key."
msgstr ""

#: wifiradar/gui/g2/profile.py:349
msgid "none"
msgstr ""

#: wifiradar/gui/g2/transients.py:62
msgid "WiFi connection manager"
msgstr ""

#: wifiradar/gui/g2/transients.py:63
msgid ""
"Copyright 2004-2014 by various authors and contributors\n"
"Current Maintainer: Sean Robinson <robinson@tuxfamily.org>"
msgstr ""

#: wifiradar/gui/g2/transients.py:67
msgid ""
"\n"
"            This program is free software; you can redistribute it and/or modify\n"
"            it under the terms of the GNU General Public License as published by\n"
"            the Free Software Foundation; either version 2 of the License, or\n"
"            (at your option) any later version.\n"
"\n"
"            This program is distributed in the hope that it will be useful,\n"
"            but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"            GNU General Public License for more details.\n"
"\n"
"            You should have received a copy of the GNU General Public License\n"
"            along with this program; if not, write to:\n"
"            Free Software Foundation, Inc.\n"
"            51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA"
msgstr ""

#: wifiradar/gui/g2/transients.py:109
msgid "Browse"
msgstr ""

#: wifiradar/gui/g2/transients.py:141
msgid "Working"
msgstr ""

#: wifiradar/gui/g2/transients.py:145
msgid "Please wait..."
msgstr ""

#: wifiradar/pubsub.py:169
msgid "read on closed Pipe ({FD}), continuing..."
msgstr ""

