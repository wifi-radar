#!/usr/bin/python

from distutils.core import setup

from test.testcommand import test

setup(
    name = "WiFi Radar",
    version = 2.1,
    author = "Sean Robinson",
    author_email = "robinson@tuxfamily.org",
    description = """ """,
    url = "",
    packages = ['wifiradar', 'test'],
    provides = ['wifiradar'],

    platforms = "Linux",
    license = "GPL",
    keywords = "WiFi scanner",
    download_url = "",
    classifiers = [
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    cmdclass={'test': test},
)

