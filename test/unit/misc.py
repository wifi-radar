#   test.misc - tests for miscellaneous functions
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2014 Sean Robinson <robinson@tuxfamily.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

from subprocess import CalledProcessError
import unittest

import mock

import wifiradar.misc as misc


class TestFunctions(unittest.TestCase):

    @mock.patch.dict('wifiradar.misc.os.environ', clear=True)
    @mock.patch('wifiradar.misc.check_call')
    def test_shellcmd(self, mock_call):
        """ Test shellcmd function. """
        # Test successful return and custom environment.
        mock_call.return_value = 0
        self.assertEqual(0, misc.shellcmd('return0', {'WRv': '2.0'}))
        mock_call.assert_called_with('return0', shell=True,
            env={'WRv': '2.0'})
        # Test for check_call failure.
        mock_call.side_effect = CalledProcessError(2, 'return2')
        with self.assertRaises(CalledProcessError) as cm:
            misc.shellcmd('return2', {})
        self.assertEqual(2, cm.exception.returncode)
        self.assertEqual('return2', cm.exception.cmd)
