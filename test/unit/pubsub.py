#   test.pubsub - tests for publish/subscribe classes
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2014 Sean Robinson <robinson@tuxfamily.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

from multiprocessing import Pipe
import unittest

import mock

import wifiradar.pubsub as pubsub


class TestLimitedDispatcher(unittest.TestCase):
    """ The Dispatcher tested here does not process messages. """

    def setUp(self):
        self.dispatch = pubsub.Dispatcher(auto_start=False)

    def test_subscribe(self):
        """ Test subscribe method. """
        sub01 = self.dispatch.subscribe()
        # There should be only one subscriber in the Dispatcher.
        self.assertEqual(1, len(self.dispatch.pipes))

    def test_unsubscribe(self):
        """ Test unsubscribe method. """
        # Simulate the two ends of a multiprocessing.Pipe.
        a, b = mock.Mock(), mock.Mock()

        self.dispatch.pipes[a] = ['TOPIC', 'EXIT']
        self.dispatch._pairs[b] = a
        # There should be only one subscriber in the Dispatcher.
        self.assertEqual(1, len(self.dispatch.pipes))

        self.dispatch.unsubscribe(b)
        # There should be no subscribers in the Dispatcher.
        self.assertEqual(0, len(self.dispatch.pipes))
        # Check that both ends of the Pipe have been closed.
        a.close.assert_called_once_with()
        b.close.assert_called_once_with()

    def tearDown(self):
        self.dispatch.close()


class TestDispatcher(unittest.TestCase):
    def setUp(self):
        self.dispatch = pubsub.Dispatcher()

    def test_simple_msg(self):
        """ Test sending and receiving a message. """
        sub01 = self.dispatch.subscribe('TEST')
        sub02 = self.dispatch.subscribe('TEST')
        msg = pubsub.Message('TEST', 'Hello')
        sub01.send(msg)
        self.assertEqual(True, sub02.poll(0.25))
        recv_msg = sub02.recv()
        self.assertEqual(msg.topic, recv_msg.topic)
        self.assertEqual(msg.details, recv_msg.details)
        # Subscribers do not get a copy of their own messages.
        self.assertEqual(False, sub01.poll(0.25))

    def test_multi_msg(self):
        """ Test sending and receiving multiple messages. """
        subs = list()
        subs.append(self.dispatch.subscribe('TEST01'))
        subs.append(self.dispatch.subscribe('TEST02'))
        subs.append(self.dispatch.subscribe(('TEST01', 'TEST02')))
        subs.append(self.dispatch.subscribe())

        msgs = list()
        msgs.append(pubsub.Message('TEST01', 'Hello'))
        msgs.append(pubsub.Message('TEST02', 'Hello'))
        msgs.append(pubsub.Message('TEST03', 'Hello'))

        publisher = self.dispatch.subscribe()
        for m in msgs:
            publisher.send(m)

        self.assertEqual(True, subs[0].poll(0.25))
        recv_msg = subs[0].recv()
        self.assertEqual(msgs[0].topic, recv_msg.topic)
        self.assertEqual(msgs[0].details, recv_msg.details)
        # No more messages waiting.
        self.assertEqual(False, subs[0].poll(0.25))
        self.assertEqual(True, subs[1].poll(0.25))
        recv_msg = subs[1].recv()
        self.assertEqual(msgs[1].topic, recv_msg.topic)
        self.assertEqual(msgs[1].details, recv_msg.details)
        # No more messages waiting.
        self.assertEqual(False, subs[1].poll(0.25))
        # Check for two messages, one for each subscribed topic.
        self.assertEqual(True, subs[2].poll(0.25))
        recv_msg = subs[2].recv()
        self.assertEqual(msgs[0].topic, recv_msg.topic)
        self.assertEqual(msgs[0].details, recv_msg.details)
        self.assertEqual(True, subs[2].poll(0.25))
        recv_msg = subs[2].recv()
        self.assertEqual(msgs[1].topic, recv_msg.topic)
        self.assertEqual(msgs[1].details, recv_msg.details)
        # No more messages waiting.
        self.assertEqual(False, subs[2].poll(0.25))
        # This subscriber should not have received any messages.
        self.assertEqual(False, subs[3].poll(0.25))

        # Subscribers do not get a copy of their own messages.
        self.assertEqual(False, publisher.poll(0.25))

    def tearDown(self):
        self.dispatch.close()


class TestConnectors(unittest.TestCase):
    def setUp(self):
        self.local_dispatch = pubsub.Dispatcher()
        self.foreign_dispatch = pubsub.Dispatcher()

    def test_connector(self):
        """ Test sending through a chain of Dispatchers. """
        msg = pubsub.Message('TEST', 'Hello')
        # There should be no subscribers in the Dispatchers.
        self.assertEqual(0, len(self.local_dispatch.pipes))
        self.assertEqual(0, len(self.foreign_dispatch.pipes))
        # Setup a Pipe connecting the two Dispatchers.
        a, b = Pipe()
        self.local_dispatch.add_connector(a)
        self.foreign_dispatch.add_connector(b)
        # There should be one subscriber in the Dispatchers.
        self.assertEqual(1, len(self.local_dispatch.pipes))
        self.assertEqual(1, len(self.foreign_dispatch.pipes))
        # Subscribe to 'TEST' in both Dispatchers.
        local_sub = self.local_dispatch.subscribe('TEST')
        foreign_sub = self.foreign_dispatch.subscribe('TEST')
        # There should be two subscribers in the Dispatchers.
        self.assertEqual(2, len(self.local_dispatch.pipes))
        self.assertEqual(2, len(self.foreign_dispatch.pipes))
        # Send test message...
        local_sub.send(msg)
        # ...receive test message.
        self.assertEqual(True, foreign_sub.poll(0.25))
        recv_msg = foreign_sub.recv()
        self.assertEqual(msg.topic, recv_msg.topic)
        self.assertEqual(msg.details, recv_msg.details)
        # Remove link between the two Dispatchers.
        self.local_dispatch.remove_connector(a)
        self.foreign_dispatch.remove_connector(b)
        # Unsubscribe from both Dispatchers.
        self.local_dispatch.unsubscribe(local_sub)
        self.foreign_dispatch.unsubscribe(foreign_sub)
        # There should be no subscribers in the Dispatchers.
        self.assertEqual(0, len(self.local_dispatch.pipes))
        self.assertEqual(0, len(self.foreign_dispatch.pipes))

    def test_bridge(self):
        """ Test the bridge function. """
        # There should be no subscribers in the Dispatchers.
        self.assertEqual(0, len(self.local_dispatch.pipes))
        self.assertEqual(0, len(self.foreign_dispatch.pipes))
        # Create the link...
        local, foreign = pubsub.bridge(self.local_dispatch,
                                       self.foreign_dispatch)
        # There should be one subscriber in the Dispatchers.
        self.assertEqual(1, len(self.local_dispatch.pipes))
        self.assertEqual(1, len(self.foreign_dispatch.pipes))
        # Remove the link.
        self.local_dispatch.remove_connector(local)
        self.foreign_dispatch.remove_connector(foreign)
        # There should be no subscribers in the Dispatchers.
        self.assertEqual(0, len(self.local_dispatch.pipes))
        self.assertEqual(0, len(self.foreign_dispatch.pipes))

    def tearDown(self):
        self.local_dispatch.close()
        self.foreign_dispatch.close()
