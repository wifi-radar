#   test.connections - tests for connection manager
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2014 Sean Robinson <robinson@tuxfamily.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

from multiprocessing import Pipe
from signal import SIGTERM
from subprocess import CalledProcessError
from threading import Thread
import unittest

import mock

import wifiradar.connections
from wifiradar.config import ConfigManager
from wifiradar.misc import DeviceError, get_new_profile
from wifiradar.pubsub import Message


class TestScanner(unittest.TestCase):
    @mock.patch('wifiradar.connections.Popen')
    def test_scanner(self, mock_popen):
        """ Test scanner function. """
        # Prepare the profile information to test against mock scan data.
        profiles = list()
        profile = get_new_profile()
        profile.update({'protocol': 'bg', 'bssid': '10:15:54:9E:2E:AA',
            'encrypted': True, 'signal': '-58', 'essid': 'startingenlivening',
            'channel': '2', 'mode': 'Master'})
        profiles.append(profile)
        profile = get_new_profile()
        profile.update({'protocol': 'bg', 'bssid': 'B2:B0:03:1A:8F:6B',
            'encrypted': False, 'signal': '-55', 'essid': 'filtersgenuinely',
            'channel': '3', 'mode': 'Master'})
        profiles.append(profile)
        profile = get_new_profile()
        profile.update({'protocol': 'bg', 'bssid': '10:A8:08:4C:18:26',
            'encrypted': False, 'signal': '-54', 'essid': 'tractorsenslave',
            'channel': '11', 'mode': 'Managed'})
        profiles.append(profile)

        # Read mock scan data from file.
        mock_popen.return_value.stdout = open('./test/data/mock-scan-001.txt', 'r')

        # Create control Pipe
        a, b = Pipe()
        test_thread = Thread(None, wifiradar.connections.scanner, args=(mock.Mock(), b))
        test_thread.start()
        for profile in profiles:
            # assert on poll() ensures a timeout if any expected msgs are not sent.
            self.assertTrue(a.poll(5))
            msg = a.recv()
            self.assertEqual('ACCESSPOINT', msg.topic)
            self.assertEqual(profile, msg.details)
        a.send(Message('EXIT', ''))
        test_thread.join()


class TestConnectionManager(unittest.TestCase):
    def setUp(self):
        self.conn_manager = wifiradar.connections.ConnectionManager(mock.Mock())
        self.mock_config = mock.Mock()
        self.conn_manager.config = self.mock_config

    def test_set_config(self):
        """ Test set_config method. """
        self.assertRaises(TypeError, self.conn_manager.set_config, None)
        self.assertEqual(None, self.conn_manager.set_config(ConfigManager()))

    @mock.patch('wifiradar.connections.shellcmd')
    def test__run_script(self, mock_shellcmd):
        """ Test _run_script method. """
        script_name = 'con_script'
        device = 'wlan0'
        ip = '192.168.1.1'
        essid = 'WinterPalace'
        bssid = '01:02:03:04:05:06'
        profile = {
            'con_script': 'mysterious_bash_command',
            'essid': essid,
            'bssid': bssid,
            'use_wpa': False,
            'key': '',
            'security': ''}
        self.conn_manager.get_current_ip = mock.Mock(return_value=ip)
        self.conn_manager.get_current_essid = mock.Mock(return_value=essid)
        self.conn_manager.get_current_bssid = mock.Mock(return_value=bssid)

        self.assertEqual(None,
            self.conn_manager._run_script(script_name, profile, device))

        custom_env = {
            "WIFIRADAR_IP": ip,
            "WIFIRADAR_ESSID": essid,
            "WIFIRADAR_BSSID": bssid,
            "WIFIRADAR_PROFILE": '{}:{}'.format(essid, bssid),
            "WIFIRADAR_ENCMODE": 'none',
            "WIFIRADAR_SECMODE": '',
            "WIFIRADAR_IF": device}
        mock_shellcmd.assert_called_with(['mysterious_bash_command'],
            custom_env)

    @mock.patch('wifiradar.connections.shellcmd')
    def test__prepare_nic(self, mock_shellcmd):
        """ Test _prepare_nic method. """
        self.mock_config.get_opt.return_value = 'mock_iwconfig'
        self.mock_config.get_opt_as_bool.return_value = True
        essid = 'WinterPalace'
        bssid = '01:02:03:04:05:06'
        profile = {
            'essid': essid,
            'bssid': bssid,
            'key': '',
            'mode': 'managed',
            'channel': 11}
        device = 'wlan0'
        self.assertEqual(None,
            self.conn_manager._prepare_nic(profile, device))
        mock_shellcmd.assert_called_with(['mock_iwconfig', device,
            'essid', "'{}'".format(essid), 'key', 'off',
            'mode', 'managed', 'channel', 11, 'ap', bssid, 'commit'])

        # Test failure.
        self.mock_config.get_opt.return_value = 'mock_iwconfig'
        self.mock_config.get_opt_as_bool.return_value = True
        mock_shellcmd.side_effect = CalledProcessError(250, 'mock_iwconfig')
        self.assertRaises(DeviceError,
            self.conn_manager._prepare_nic, profile, device)

    @mock.patch('__builtin__.open', )
    @mock.patch('os.kill')
    @mock.patch('os.access')
    @mock.patch('wifiradar.connections.shellcmd')
    def test__stop_dhcp(self, mock_shellcmd, mock_access, mock_kill, mock_open):
        """ Test _stop_dhcp method. """
        dhcp_cmd = 'dhcpcd'
        kill_args = '-k -d'
        kill_args_list = ['-k', '-d']
        self.mock_config.get_opt.side_effect = ['mock_pidfile', kill_args,
            dhcp_cmd, kill_args]
        mock_access.return_value = True
        device = 'wlan0'

        self.assertEqual(None, self.conn_manager._stop_dhcp(device))
        call_list = [dhcp_cmd]
        call_list.extend(kill_args_list)
        call_list.append(device)
        mock_shellcmd.assert_called_with(call_list)

        self.mock_config.get_opt.side_effect = ['mock_pidfile', '',
            dhcp_cmd, '', 'mock_pidfile']
        PID = 42
        mock_open.return_value.readline.return_value = PID
        self.assertEqual(None, self.conn_manager._stop_dhcp(device))
        mock_kill.assert_called_with(PID, SIGTERM)

    @mock.patch('wifiradar.connections.Popen')
    def test__start_dhcp(self, mock_popen):
        """ Test _start_dhcp method. """
        dhcp_cmd = 'dhcpcd'
        dhcp_args = '-t %(timeout)s -w'
        dhcp_args_list = ['-t', '%(timeout)s', '-w']
        dhcp_timeout = 1
        device = 'wlan0'
        self.mock_config.get_opt.side_effect = [dhcp_cmd, dhcp_args]
        self.mock_config.get_opt_as_int.return_value = dhcp_timeout
        self.conn_manager.get_current_ip = mock.Mock(return_value='0.0.0.0')
        self.assertEqual(None, self.conn_manager._start_dhcp(device))
        call_list = [dhcp_cmd]
        call_list.extend(dhcp_args_list)
        call_list.append(device)
        mock_popen.assert_called_with(call_list, stderr=None, stdout=None)
        # Test missing DHCP command.
        self.mock_config.get_opt.side_effect = [dhcp_cmd, dhcp_args]
        mock_popen.side_effect = OSError(2, 'No such file or directory')
        self.assertEqual(None, self.conn_manager._start_dhcp(device))

    @mock.patch('__builtin__.open', )
    @mock.patch('os.kill')
    @mock.patch('os.access')
    @mock.patch('wifiradar.connections.shellcmd')
    def test__stop_wpa(self, mock_shellcmd, mock_access, mock_kill, mock_open):
        """ Test _stop_wpa method. """
        kill_cmd = ''
        self.mock_config.get_opt.side_effect = ['mock_pidfile', kill_cmd,
            'mock_pidfile']
        mock_access.return_value = True

        PID = 42
        mock_open.return_value.readline.return_value = PID
        self.assertEqual(None, self.conn_manager._stop_wpa())
        mock_kill.assert_called_with(PID, SIGTERM)

    @mock.patch('wifiradar.connections.shellcmd')
    def test__start_wpa(self, mock_shellcmd):
        """ Test _start_wpa method. """
        wpa_cmd = 'mock_wpa'
        wpa_args = '-B -i %(interface)s -c %(configuration)s'
        wpa_args_list = ['-B', '-i', '%(interface)s', '-c', '%(configuration)s']
        self.mock_config.get_opt.side_effect = [wpa_cmd, wpa_args]
        self.assertEqual(None,
            self.conn_manager._start_wpa())
        call_list = [wpa_cmd]
        call_list.extend(wpa_args_list)
        mock_shellcmd.assert_called_with(call_list)

    @mock.patch('__builtin__.open', )
    @mock.patch('wifiradar.connections.shellcmd')
    def test__start_manual_network(self, mock_shellcmd, mock_open):
        """ Test _start_manual_network method. """
        ifconfig = 'mock_ifconfig'
        route = 'mock_route'
        ip = '192.168.1.100'
        netmask = '192.168.1.255'
        gateway = '192.168.1.1'
        domain = 'winterpalace.org'
        dns1 = '192.168.1.200'
        dns2 = '192.168.1.201'
        profile = {
            'ip': ip,
            'netmask': netmask,
            'gateway': gateway,
            'domain': domain,
            'dns1': dns1,
            'dns2': dns2}
        device = 'wlan0'
        self.mock_config.get_opt.side_effect = [ifconfig, ifconfig, route]

        self.assertEqual(None,
            self.conn_manager._start_manual_network(profile, device))
        mock_shellcmd.assert_any_call([ifconfig, device, 'down'])
        mock_shellcmd.assert_any_call([ifconfig, device, ip,
            'netmask', netmask])
        mock_shellcmd.assert_any_call([route, 'add', 'default',
            'gw', gateway])
        # Test mock-writing the resolv file.
        mock_open.assert_called_with('/etc/resolv.conf', 'w')
        # Check that resolv_file.write was called with the proper args.
        mock_open.return_value.__enter__.return_value.write.assert_called_with(
            'domain {}\nnameserver {}\nnameserver {}\n'.format(domain, dns1, dns2))

    def test_connect(self):
        """ Test connect method. """
        profile = {
            'essid': 'WinterPalace',
            'bssid': '01:02:03:04:05:06',
            'use_wpa': True,
            'use_dhcp': True}
        device = 'wlan0'
        self.mock_config.get_network_device.return_value = device

        self.conn_manager._run_script = mock.Mock(name='_run_script')
        self.conn_manager._prepare_nic = mock.Mock(name='_prepare_nic')
        self.conn_manager._stop_dhcp = mock.Mock(name='_stop_dhcp')
        self.conn_manager._start_dhcp = mock.Mock(name='_start_dhcp')
        self.conn_manager._stop_wpa = mock.Mock(name='_stop_wpa')
        self.conn_manager._start_wpa = mock.Mock(name='_start_wpa')
        self.conn_manager._start_manual_network = mock.Mock(
            name='_start_manual_network')

        # Test DHCP and WPA network configuration.
        self.assertEqual(None, self.conn_manager.connect(profile))
        self.conn_manager._run_script.assert_any_call('con_prescript',
            profile, device)
        self.conn_manager._run_script.assert_any_call('con_postscript',
            profile, device)
        self.conn_manager._prepare_nic.assert_called_with(profile, device)
        self.conn_manager._stop_dhcp.assert_called_with(device)
        self.conn_manager._start_dhcp.assert_called_with(device)
        self.conn_manager._stop_wpa.assert_called_with()
        self.conn_manager._start_wpa.assert_called_with()

        # Test manual network configuration.
        profile['use_dhcp'] = False
        self.assertEqual(None, self.conn_manager.connect(profile))
        self.conn_manager._start_manual_network.assert_called_with(profile, device)

    @mock.patch('wifiradar.connections.shellcmd')
    def test_disconnect(self, mock_shellcmd):
        """ Test disconnect method. """
        iwconfig = 'mock_iwconfig'
        ifconfig = 'mock_ifconfig'
        profile = {}
        device = 'wlan0'
        self.mock_config.get_network_device.return_value = device
        self.mock_config.get_opt.side_effect = [iwconfig, ifconfig]
        self.conn_manager._run_script = mock.Mock(name='_run_script')
        self.conn_manager._stop_dhcp = mock.Mock(name='_stop_dhcp')
        self.conn_manager._stop_wpa = mock.Mock(name='_stop_wpa')
        self.conn_manager.if_change = mock.Mock(name='if_change')

        self.assertEqual(None, self.conn_manager.disconnect(profile))
        self.conn_manager._run_script.assert_any_call('dis_prescript',
            profile, device)
        self.conn_manager._run_script.assert_any_call('dis_postscript',
            profile, device)
        self.conn_manager._stop_dhcp.assert_called_with(device)
        self.conn_manager._stop_wpa.assert_called_with()

        mock_shellcmd.assert_any_call([iwconfig, device,
            'essid', 'any', 'key', 'off', 'mode', 'managed',
            'channel', 'auto', 'ap', 'off'])
        mock_shellcmd.assert_any_call([ifconfig, device, '0.0.0.0'])

    @mock.patch('wifiradar.connections.Popen')
    def test_if_change(self, mock_popen):
        """ Test if_change method. """
        self.mock_config.get_opt.return_value = 'mock_ifconfig'
        self.mock_config.get_network_device.return_value = 'wlan0'

        # Test normal operation.
        self.assertEqual(None, self.conn_manager.if_change('up'))
        self.assertEqual(None, self.conn_manager.if_change('down'))
        self.assertEqual(None, self.conn_manager.if_change('UP'))
        self.assertEqual(None, self.conn_manager.if_change('DOWN'))

        # Test unknown state.
        self.assertRaises(ValueError, self.conn_manager.if_change, 'sideways')

        # Test failed state change with found ifconfig.
        mock_popen_instance = mock.MagicMock()
        mock_popen_instance.stdout.__iter__.return_value = [
            'wlan0: ERROR while getting interface flags: No such device']
        mock_popen.return_value = mock_popen_instance
        self.assertRaises(DeviceError, self.conn_manager.if_change, 'up')

        # Test OSError.
        mock_popen.side_effect = OSError(2, 'No such file or directory')
        self.assertRaises(OSError, self.conn_manager.if_change, 'up')

    @mock.patch('wifiradar.connections.Popen')
    def test_get_current_ip(self, mock_popen):
        """ Test get_current_ip method. """
        self.mock_config.get_opt.return_value = 'mock_ifconfig'
        self.mock_config.get_network_device.return_value = 'wlan0'

        # Test connected and with an IP address.
        mock_popen_instance = mock.Mock()
        mock_popen_instance.stdout.read.return_value = \
            'wlan0: flags=4098<BROADCAST,MULTICAST>  mtu 1500\n' + \
            'inet addr:192.168.1.1  netmask 255.255.255.0  broadcast 192.168.1.255\n' + \
            'ether 00:00:00:00:00:00  txqueuelen 1000  (Ethernet)\n' + \
            'RX packets 246  bytes 259631 (253.5 KiB)\n' + \
            'RX errors 0  dropped 1  overruns 0  frame 0\n' + \
            'TX packets 123  bytes 140216 (136.9 KiB)\n' + \
            'TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0\n'
        mock_popen.return_value = mock_popen_instance
        self.assertEqual('192.168.1.1', self.conn_manager.get_current_ip())

        # Test not connected and without an IP address.
        mock_popen_instance.stdout.read.return_value = \
            'wlan0: flags=4098<BROADCAST,MULTICAST>  mtu 1500\n' + \
            'ether 00:00:00:00:00:00  txqueuelen 1000  (Ethernet)\n' + \
            'RX packets 246  bytes 259631 (253.5 KiB)\n' + \
            'RX errors 0  dropped 1  overruns 0  frame 0\n' + \
            'TX packets 123  bytes 140216 (136.9 KiB)\n' + \
            'TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0\n'
        mock_popen.return_value = mock_popen_instance
        self.assertEqual(None, self.conn_manager.get_current_ip())

    @mock.patch('wifiradar.connections.Popen')
    def test_get_current_essid(self, mock_popen):
        """ Test get_current_essid method. """
        self.mock_config.get_opt.return_value = 'mock_iwconfig'
        self.mock_config.get_network_device.return_value = 'wlan0'

        # Test associated interface.
        mock_popen_instance = mock.Mock()
        mock_popen_instance.stdout.read.return_value = \
            'wlan0     IEEE 802.11abgn  ESSID:"WinterPalace"\n' + \
            '          Mode:Managed  Frequency:5.26 GHz  Access Point: 00:00:00:00:00:00\n' + \
            '          Tx-Power=15 dBm\n' + \
            '          Retry  long limit:7   RTS thr:off   Fragment thr:off\n' + \
            '          Power Management:off\n'
        mock_popen.return_value = mock_popen_instance
        self.assertEqual('WinterPalace', self.conn_manager.get_current_essid())

        # Test not associated interface.
        mock_popen_instance = mock.Mock()
        mock_popen_instance.stdout.read.return_value = \
            'wlan0     IEEE 802.11abgn  ESSID:off/any\n' + \
            '          Mode:Managed  Frequency:5.26 GHz  Access Point: Not-Associated\n' + \
            '          Tx-Power=15 dBm\n' + \
            '          Retry  long limit:7   RTS thr:off   Fragment thr:off\n' + \
            '          Power Management:off\n'
        mock_popen.return_value = mock_popen_instance
        self.assertEqual(None, self.conn_manager.get_current_essid())

    @mock.patch('wifiradar.connections.Popen')
    def test_get_current_bssid(self, mock_popen):
        """ Test get_current_bssid method. """
        self.mock_config.get_opt.return_value = 'mock_iwconfig'
        self.mock_config.get_network_device.return_value = 'wlan0'

        # Test associated interface.
        mock_popen_instance = mock.Mock()
        mock_popen_instance.stdout.read.return_value = \
            'wlan0     IEEE 802.11abgn  ESSID:"WinterPalace"\n' + \
            '          Mode:Managed  Frequency:5.26 GHz  Access Point: 00:00:00:00:00:00\n' + \
            '          Tx-Power=15 dBm\n' + \
            '          Retry  long limit:7   RTS thr:off   Fragment thr:off\n' + \
            '          Power Management:off\n'

        mock_popen.return_value = mock_popen_instance
        self.assertEqual('00:00:00:00:00:00', self.conn_manager.get_current_bssid())

        # Test not associated interface.
        mock_popen_instance = mock.Mock()
        mock_popen_instance.stdout.read.return_value = \
            'wlan0     IEEE 802.11abgn  ESSID:off/any\n' + \
            '          Mode:Managed  Frequency:5.26 GHz  Access Point: Not-Associated\n' + \
            '          Tx-Power=15 dBm\n' + \
            '          Retry  long limit:7   RTS thr:off   Fragment thr:off\n' + \
            '          Power Management:off\n'
        mock_popen.return_value = mock_popen_instance
        self.assertEqual(None, self.conn_manager.get_current_bssid())
