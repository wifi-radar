#   test.config - tests for configuration management
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/Linux.
#
#   Copyright (C) 2014 Sean Robinson <robinson@tuxfamily.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from __future__ import unicode_literals

import codecs
from configparser import NoOptionError, NoSectionError
from io import StringIO
import unittest

import mock

from wifiradar.config import (make_section_name,
                              ConfigManager, ConfigFileManager)
from wifiradar.misc import PYVERSION, NoDeviceError


class TestConfigManager(unittest.TestCase):
    def setUp(self):
        self.config = ConfigManager()

    @mock.patch('wifiradar.config.Popen')
    def test_get_network_device(self, mock_popen):
        """ Test get_network_device. """
        self.config.set_opt('GENERAL', 'interface', 'wlan0')
        self.assertEqual('wlan0', self.config.get_network_device())

        # Test successful auto-detected interface.
        mock_popen_instance = mock.MagicMock()
        mock_popen_instance.stdout.__iter__.return_value = [
            'wlan0     IEEE 802.11abgn  ESSID:"WinterPalace"\n',
            '          Mode:Managed  Frequency:5.26 GHz  Access Point: 00:00:00:00:00:00\n',
            '          Tx-Power=15 dBm\n',
            '          Retry  long limit:7   RTS thr:off   Fragment thr:off\n',
            '          Power Management:off\n']
        mock_popen.return_value = mock_popen_instance
        self.config.set_opt('GENERAL', 'interface', 'auto_detect')
        self.config.set_opt('GENERAL', 'iwconfig_command', 'mock_iwconfig')
        self.assertEqual('wlan0', self.config.get_network_device())

        # Test failed auto-detected interface.
        mock_popen_instance = mock.MagicMock()
        mock_popen_instance.stdout.__iter__.return_value = [
            'lo        no wireless extensions.\n',
            '\n',
            'eth0      no wireless extensions.\n',
            '\n']
        mock_popen.return_value = mock_popen_instance
        self.config.set_opt('GENERAL', 'interface', 'auto_detect')
        self.config.set_opt('GENERAL', 'iwconfig_command', 'mock_iwconfig')
        self.assertRaises(NoDeviceError,
            self.config.get_network_device)

    def test_set_get_opt(self):
        """ Test set_opt and get_opt methods. """
        self.assertEqual(self.config.sections(), [])
        self.config.set_opt('TEST_SECTION', 'str_opt', 'str_value')
        self.assertEqual('str_value',
            self.config.get_opt('TEST_SECTION', 'str_opt'))

        # Check for expected exceptions in set_opt.
        self.assertRaises(TypeError, self.config.set_opt,
            'TEST_SECTION', 'str_opt', True)
        self.assertRaises(TypeError, self.config.set_opt,
            'TEST_SECTION', 'str_opt', 0)
        self.assertRaises(TypeError, self.config.set_opt,
            'TEST_SECTION', 'str_opt', 1.0)

        # Check for expected exceptions in get_opt.
        self.assertRaises(NoSectionError, self.config.get_opt,
            'UNKNOWN_SECTION', 'str_opt')
        self.assertRaises(NoOptionError, self.config.get_opt,
            'TEST_SECTION', 'unknown_opt')

    def test_set_get_bool_opt(self):
        """ Test set_bool_opt and get_opt_as_bool methods. """
        self.assertEqual(self.config.sections(), [])
        # Alternate True/False tests in case set_bool_opt does not change
        # the value in 'bool_opt'

        # Check boolean conversion.
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', True)
        self.assertEqual(True,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', False)
        self.assertEqual(False,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))

        # Check successful integer conversion.
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', 1)
        self.assertEqual(True,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', 0)
        self.assertEqual(False,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))

        # Check string conversion.
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', 'True')
        self.assertEqual(True,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', 'False')
        self.assertEqual(False,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))

        # extra possible values
        self.config.set_bool_opt('TEST_SECTION', 'bool_opt', 2)
        self.assertEqual(True,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))

        # Check for expected exceptions in set_bool_opt.
        self.assertRaises(ValueError, self.config.set_bool_opt,
            'TEST_SECTION', 'bool_opt', -1)
        self.assertRaises(ValueError, self.config.set_bool_opt,
            'TEST_SECTION', 'bool_opt', 'Not False')
        self.assertRaises(ValueError, self.config.set_bool_opt,
            'TEST_SECTION', 'bool_opt', 1.0)

        # Check for expected exceptions in get_opt_as_bool.
        self.config.set_opt('TEST_SECTION', 'bool_opt', 'spam')
        self.assertRaises(ValueError, self.config.get_opt_as_bool,
            'TEST_SECTION', 'bool_opt')

    def test_set_get_int_opt(self):
        """ Test set_int_opt and get_opt_as_int methods. """
        self.assertEqual(self.config.sections(), [])
        self.config.set_int_opt('TEST_SECTION', 'int_opt', 42)
        self.assertEqual(42,
            self.config.get_opt_as_int('TEST_SECTION', 'int_opt'))

        # Check for expected exceptions in set_int_opt.
        # Boolean values are integers, so do not try to find an exception.
        self.assertRaises(TypeError, self.config.set_int_opt,
            'TEST_SECTION', 'int_opt', 'eggs')
        self.assertRaises(TypeError, self.config.set_int_opt,
            'TEST_SECTION', 'int_opt', 1.0)

        # Check for expected exceptions in get_opt_as_int.
        self.config.set_opt('TEST_SECTION', 'int_opt', 'spam')
        self.assertRaises(ValueError, self.config.get_opt_as_int,
            'TEST_SECTION', 'int_opt')

    def test_set_get_float_opt(self):
        """ Test set_float_opt and get_opt_as_float methods. """
        self.assertEqual(self.config.sections(), [])
        self.config.set_float_opt('TEST_SECTION', 'float_opt', 42)
        self.assertEqual(42.0,
            self.config.get_opt_as_float('TEST_SECTION', 'float_opt'))
        self.config.set_float_opt('TEST_SECTION', 'float_opt', 3.0)
        self.assertEqual(3.0,
            self.config.get_opt_as_float('TEST_SECTION', 'float_opt'))

        # Check for expected exceptions in set_float_opt.
        # Boolean values are integers, so do not try to find an exception.
        self.assertRaises(TypeError, self.config.set_float_opt,
            'TEST_SECTION', 'float_opt', 'eggs')

        # Check for expected exceptions in get_opt_as_float.
        self.config.set_opt('TEST_SECTION', 'float_opt', 'spam')
        self.assertRaises(ValueError, self.config.get_opt_as_float,
            'TEST_SECTION', 'float_opt')

    def test_set_section(self):
        """ Test set_section method. """
        self.assertEqual(self.config.sections(), [])

        options = {'bool_opt': True, 'int_opt': 42, 'float_opt': 3.1415,
                   'str_opt': 'eggs and spam'}
        self.config.set_section('TEST_SECTION', options)
        self.assertEqual(True,
            self.config.get_opt_as_bool('TEST_SECTION', 'bool_opt'))
        self.assertEqual(42,
            self.config.get_opt_as_int('TEST_SECTION', 'int_opt'))
        self.assertEqual(3.1415,
            self.config.get_opt_as_float('TEST_SECTION', 'float_opt'))
        self.assertEqual('eggs and spam',
            self.config.get_opt('TEST_SECTION', 'str_opt'))

    def test_profiles(self):
        """ Test profiles method. """
        self.assertEqual(self.config.sections(), [])

        self.config.add_section('TEST_SECTION')
        self.config.add_section('TEST_AP01:00:01:02:03:04:05')
        self.config.add_section('spam.com:AA:BB:CC:DD:EE:FF')

        self.assertEqual(['TEST_AP01:00:01:02:03:04:05',
            'spam.com:AA:BB:CC:DD:EE:FF'],
            self.config.profiles())

    def test_update(self):
        """ Test update method. """
        items_orig = dict((('eggs', 'spam'), ('ham', 'spam and spam')))
        items_copy = dict((('eggs', 'spam and toast'),
            ('ham', 'spam and spam and toast')))
        # Set up an original ConfigManager.
        self.config.set_section('DEFAULT', items_orig)
        self.config.set_section('SECT01', items_orig)
        self.config.set_section('SECT02', items_orig)
        self.config.set_section('SECT03', items_orig)
        self.config.set_section('WinterPalace:00:00:00:00:00:00', items_orig)
        self.config.set_section('SummerKingdom:', items_orig)
        # Set up a copy ConfigManager.
        config_copy = ConfigManager(defaults=items_copy)
        config_copy.set_section('SECT01', items_copy)
        config_copy.set_section('SECT02', items_copy)
        # Update the original from the copy.
        self.config.update(config_copy)
        # Check that sections in config_copy have changed.
        for section in config_copy.sections():
            self.assertEqual(self.config.items(section), items_copy)
        # Check that the profiles and extra sections are unchanged.
        for section in self.config.profiles():
            self.assertEqual(self.config.items(section), items_orig)
        self.assertEqual(self.config.items('SECT03'), items_orig)

    def test_copy(self):
        """ Test copy method. """
        # Copy just the DEFAULT section.
        defaults = [('eggs', 'spam'), ('ham', 'spam and spam')]
        orig = ConfigManager(defaults=dict(defaults))
        copy = orig.copy()
        self.assertEqual(copy.items('DEFAULT'), orig.items('DEFAULT'))
        # Copy multiple sections with profiles.
        items = dict((('eggs', 'spam'), ('ham', 'spam and spam')))
        orig = ConfigManager()
        orig.set_section('SECT01', items)
        orig.set_section('SECT02', items)
        orig.set_section('WinterPalace:00:00:00:00:00:00', items)
        orig.set_section('SummerKingdom:', items)
        copy = orig.copy(profiles=True)
        for section in orig.sections():
            self.assertEqual(copy.items(section), orig.items(section))
        # Copy multiple sections without profiles.
        copy = orig.copy(profiles=False)
        orig.remove_section('WinterPalace:00:00:00:00:00:00')
        orig.remove_section('SummerKingdom:')
        for section in orig.sections():
            self.assertEqual(copy.items(section), orig.items(section))


class TestConfigFileManager(unittest.TestCase):
    def setUp(self):
        self.test_conf_file = './test/data/wifi-radar-test.conf'
        self.GENERAL = {
            'auto_profile_order': "[u'WinterPalace:00:09:5B:D5:03:4A']",
            'commit_required': 'False',
            'ifconfig_command': '/sbin/ifconfig',
            'ifup_required': 'False',
            'interface': 'auto_detect',
            'iwconfig_command': '/sbin/iwconfig',
            'iwlist_command': '/sbin/iwlist',
            'logfile': '/var/log/wifi-radar.log',
            'loglevel': '50',
            'route_command': '/sbin/route',
            'version': '0.0.0'}
        self.DHCP = {'args': '-D -o -i dhcp_client -t ${timeout}',
            'command': '/sbin/dhcpcd',
            'kill_args': '-k',
            'pidfile': '/etc/dhcpc/dhcpcd-${GENERAL:interface}.pid',
            'timeout': '30'}
        self.WPA = {'args': '-B -i ${GENERAL:interface} -c ${configuration} '
                '-D ${driver} -P ${pidfile}',
            'command': '/usr/sbin/wpa_supplicant',
            'configuration': '/etc/wpa_supplicant.conf',
            'driver': 'wext',
            'kill_command': '',
            'pidfile': '/var/run/wpa_supplicant.pid'}
        self.WINTERPALACE = {'available': 'False',
            'bssid': '00:09:5B:D5:03:4A',
            'channel': 'auto',
            'con_postscript': '',
            'con_prescript': '',
            'dis_postscript': '',
            'dis_prescript': '',
            'dns1': '',
            'dns2': '',
            'domain': '',
            'encrypted': 'True',
            'essid': 'WinterPalace',
            'gateway': '',
            'ip': '',
            'key': '',
            'known': 'True',
            'mode': 'auto',
            'netmask': '',
            'protocol': 'g',
            'roaming': 'False',
            'security': 'none',
            'signal': '-193',
            'use_dhcp': 'True',
            'wep_mode': 'none',
            'wpa_psk': ''}

    def test_read(self):
        """ Test read method. """
        self.config_file = ConfigFileManager(self.test_conf_file)
        config = self.config_file.read()
        self.assertEqual(config.items('GENERAL', raw=True), self.GENERAL)
        self.assertEqual(config.items('DHCP', raw=True), self.DHCP)
        self.assertEqual(config.items('WPA', raw=True), self.WPA)
        self.assertEqual(config.items('WinterPalace:00:09:5B:D5:03:4A',
            raw=True), self.WINTERPALACE)
        self.assertEqual(config.auto_profile_order,
            [u'WinterPalace:00:09:5B:D5:03:4A'])

    def test_read_failures(self):
        """ Test failure modes in the read method. """
        # Missing file.
        self.config_file = ConfigFileManager(
            self.test_conf_file.replace('wifi-radar-test', 'missing_file'))
        with self.assertRaises(IOError) as e:
            config = self.config_file.read()
            self.assertEqual(2, e.exception)
        # File whose permissions do not allow reading.
        self.config_file = ConfigFileManager(
            self.test_conf_file.replace('-test', '-unreadable'))
        with self.assertRaises(IOError) as e:
            config = self.config_file.read()
            self.assertEqual(13, e.exception)

    def test_read_string(self):
        """ Test read_string method. """
        with codecs.open(self.test_conf_file, 'r', encoding='utf8') as f:
            test_string = f.read()
        config_file = ConfigFileManager('')
        config = config_file.read_string(test_string)
        self.assertEqual(config.items('GENERAL', raw=True), self.GENERAL)
        self.assertEqual(config.items('DHCP', raw=True), self.DHCP)
        self.assertEqual(config.items('WPA', raw=True), self.WPA)
        self.assertEqual(config.items('WinterPalace:00:09:5B:D5:03:4A',
            raw=True), self.WINTERPALACE)
        self.assertEqual(config.auto_profile_order,
            [u'WinterPalace:00:09:5B:D5:03:4A'])

    @mock.patch('wifiradar.config.move')
    @mock.patch('codecs.open')
    @mock.patch('tempfile.mkstemp')
    def test_write(self, mock_mkstemp, mock_open, mock_move):
        """ Test write method. """
        mock_temp_file = 'mock_file_tempXXXXXX'
        test_file = StringIO()
        test_file._close = test_file.close
        test_file.close = lambda: None
        mock_mkstemp.return_value = (1, mock_temp_file)
        mock_open.return_value = test_file
        # Set up a copy ConfigManager.
        config_copy = ConfigManager()
        config_copy.set_section('GENERAL', self.GENERAL)
        config_copy.set_section('DHCP', self.DHCP)
        config_copy.set_section('WPA', self.WPA)
        config_copy.set_section('WinterPalace:00:09:5B:D5:03:4A',
            self.WINTERPALACE)
        config_copy.auto_profile_order = [u'WinterPalace:00:09:5B:D5:03:4A']
        self.config_file = ConfigFileManager('mock_file')
        self.config_file.write(config_copy)
        with open(self.test_conf_file, 'r') as f:
            test_data = f.read()
        self.assertEqual(test_file.getvalue(), test_data)
        test_file._close()
        mock_mkstemp.assert_called_once_with(prefix='wifi-radar.conf.')
        mock_open.assert_called_once_with(mock_temp_file, 'w', encoding='utf8')
        mock_move.assert_called_once_with(mock_temp_file, 'mock_file')


class TestConfigFunctions(unittest.TestCase):

    def test_make_section_name(self):
        """ Test make_section_name function. """
        # Check single AP profiles.
        self.assertEqual('essid:bssid',
            make_section_name('essid', 'bssid'))
        self.assertEqual('TEST_AP01:00:01:02:03:04:05',
            make_section_name('TEST_AP01', '00:01:02:03:04:05'))
        self.assertEqual('spam.com:AA:BB:CC:DD:EE:FF',
            make_section_name('spam.com', 'AA:BB:CC:DD:EE:FF'))
        self.assertEqual('eggs_and_ham:11:BB:CC:DD:EE:FF',
            make_section_name('eggs_and_ham', '11:BB:CC:DD:EE:FF'))
        self.assertEqual('eggs:and:spam:22:BB:CC:DD:EE:FF',
            make_section_name('eggs:and:spam', '22:BB:CC:DD:EE:FF'))
        # Check roaming profiles.
        self.assertEqual('essid:',
            make_section_name('essid', ''))
        self.assertEqual('TEST_AP01:',
            make_section_name('TEST_AP01', ''))
        self.assertEqual('spam.com:',
            make_section_name('spam.com', ''))
        self.assertEqual('eggs_and_ham:',
            make_section_name('eggs_and_ham', ''))
        self.assertEqual('eggs:and:spam:',
            make_section_name('eggs:and:spam', ''))
