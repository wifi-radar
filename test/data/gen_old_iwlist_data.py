#   gen_old_iwlist_data.py - generate fake iwlist output
#
#   Part of WiFi Radar: A utility for managing WiFi profiles on GNU/LinuX.
#
#   Copyright (C) 2014 Sean Robinson <robinson@tuxfamily.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License in LICENSE.GPL for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to:
#      Free Software Foundation, Inc.
#      51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#


from linecache import getline
from random import randint as r


HEADER = 'wlan0     Scan completed :'

CELL = """          Cell {cell:02d} - Address: {:02X}:{:02X}:{:02X}:{:02X}:{:02X}:{:02X}
                    ESSID:"{essid}"
                    Protocol:{prot}
                    Mode:{mode}
                    Channel:{chan}
                    Encryption key:{enc_key}
                    Bit Rates:{bit_rates}
                    Quality={qual}/100  Signal level={sig} dBm  Noise level={noise} dBm
                    Extra: Last beacon: {beacon}ms ago
"""

def get_essid():
    """ Make an ESSID from two random words.
    """
    # get the number of words (1 word per line) in words file
    i = 0
    for line in open('/usr/share/dict/words', 'r'):
        i = i + 1
    ESSID = ''
    for w in range(2):
        ESSID = ESSID + getline('/usr/share/dict/words', r(1, i+1)).rstrip('\n')
    return ESSID


MODES = ['Master', 'Managed']
ENC_MODES = ['on', 'off']
BIT_RATES = [1, 2, 5.5, 6, 9, 11, 12, 18, 24, 36, 48, 54]

def gen_bit_rates_string():
    rates = ''
    for i, rate in enumerate(BIT_RATES, 1):
        rates = rates + '{} Mb/s; '.format(rate)
        if i % 5 == 0:
            rates = rates + '\n                              '
    return rates

print HEADER
for i in range(1, 12):
    print CELL.format(cell=i, *[r(0,255) for X in range(6)],
        essid=get_essid(),
        prot='IEEE 802.11bg',
        mode=MODES[r(0, len(MODES)-1)],
        chan=r(1, 11),
        enc_key=ENC_MODES[r(0, len(ENC_MODES)-1)],
        bit_rates=gen_bit_rates_string(),
        qual=r(0, 100),
        sig=-20 - r(0, 50),
        noise=-20 - r(0, 50),
        beacon=r(0, 30000))
