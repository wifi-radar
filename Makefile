#
# Makefile for WiFi Radar
#

SHELL = /bin/sh
DESTDIR =

#
# These settings are what I would expect for most modern Linux distros,
# and are what work for me unmodified on Ubuntu. -BEF-
#
package 	= wifi-radar
prefix		= /usr
sbindir 	= $(prefix)/sbin
sysconfdir 	= /etc/
initdir 	= $(sysconfdir)/init.d
mandir 		= $(prefix)/share/man
pixmapsdir 	= $(prefix)/share/pixmaps
appsdir 	= $(prefix)/share/applications

srcdir = .
top_srcdir = .
top_builddir = .

VERSION = $(shell cat $(srcdir)/docs/VERSION)

BRANCH = $(shell echo v$(shell cat $(srcdir)/docs/VERSION | cut -d. -f1-2).x)

TOPDIR := $(CURDIR)


.PHONY: all install tarball dev-docs changelog pot-file test clean distclean

all:	wifi-radar.localized su-wifiradar

install:	all
	test -d $(DESTDIR)/${sysconfdir} || install -d -m 755 $(DESTDIR)/${sysconfdir}

	test -d $(DESTDIR)/${sbindir} || install -d -m 755 $(DESTDIR)/${sbindir}
	install -m 755 wifi-radar.localized $(DESTDIR)/${sbindir}/wifi-radar
	install -m 755 su-wifiradar $(DESTDIR)/${sbindir}/su-wifiradar

	test -d $(DESTDIR)/${mandir}/man1 || install -d -m 755 $(DESTDIR)/${mandir}/man1
	install -m 644 man/man1/wifi-radar.1 $(DESTDIR)/${mandir}/man1

	test -d $(DESTDIR)/${mandir}/man5 || install -d -m 755 $(DESTDIR)/${mandir}/man5
	install -m 644 man/man5/wifi-radar.conf.5 $(DESTDIR)/${mandir}/man5

	test -d $(DESTDIR)/${pixmapsdir} || install -d -m 755 $(DESTDIR)/${pixmapsdir}
	install -m 644 pixmaps/wifi-radar.svg $(DESTDIR)/${pixmapsdir}
	install -m 644 pixmaps/wifi-radar.png $(DESTDIR)/${pixmapsdir}

	test -d $(DESTDIR)/${appsdir} || install -d -m 755 $(DESTDIR)/${appsdir}
	install -m 644 wifi-radar.desktop $(DESTDIR)/${appsdir}

su-wifiradar: su-wifiradar.in
	sed "s/@ICONPATH@/$pixmapsdir/" su-wifiradar.in > su-wifiradar

wifi-radar.localized:	wifi-radar
	cp wifi-radar wifi-radar.tmp
	perl -pi -e 's#^CONF_FILE\s+=.*#CONF_FILE = "$(DESTDIR)/${sysconfdir}/wifi-radar.conf"#' wifi-radar.tmp
	mv wifi-radar.tmp wifi-radar.localized

tarball: $(TOPDIR)/tmp/${package}-$(VERSION).tar.bz2.sign $(TOPDIR)/tmp/${package}-$(VERSION).tar.bz2.sha256

$(TOPDIR)/tmp/${package}-$(VERSION).tar.bz2.sign: $(TOPDIR)/tmp/${package}-$(VERSION).tar.bz2
	cd $(TOPDIR)/tmp && gpg --detach-sign -a --output ${package}-$(VERSION).tar.bz2.asc ${package}-$(VERSION).tar.bz2
	cd $(TOPDIR)/tmp && chmod 644 ${package}-$(VERSION).tar.bz2.asc
	cd $(TOPDIR)/tmp && gpg --verify ${package}-$(VERSION).tar.bz2.asc

$(TOPDIR)/tmp/${package}-$(VERSION).tar.bz2.sha256:
	cd $(TOPDIR)/tmp && sha256sum ${package}-$(VERSION).tar.bz2 > ${package}-$(VERSION).tar.bz2.sha256

$(TOPDIR)/tmp/${package}-$(VERSION).tar.bz2:
	rm -fr $(TOPDIR)/tmp
	mkdir -p $(TOPDIR)/tmp/
	git archive --format=tar --prefix=${package}-$(VERSION)/ $(BRANCH) | (cd $(TOPDIR)/tmp/ && tar xf -)
	find $(TOPDIR)/tmp/${package}-$(VERSION) -type f -exec chmod ug+r  {} \;
	find $(TOPDIR)/tmp/${package}-$(VERSION) -type d -exec chmod ug+rx {} \;
	perl -pi -e 's#^WIFI_RADAR_VERSION\s+=.*#WIFI_RADAR_VERSION = "${VERSION}"#' $(TOPDIR)/tmp/${package}-$(VERSION)/wifi-radar
	chmod 755 $(TOPDIR)/tmp/${package}-$(VERSION)/wifi-radar
	cd $(TOPDIR)/tmp && tar -ch ${package}-$(VERSION) | bzip2 > ${package}-$(VERSION).tar.bz2
	cd $(TOPDIR)/tmp && chmod 644 ${package}-$(VERSION).tar.bz2
	ls -l $(TOPDIR)/tmp/

dev-docs:
	mkdir -p docs/developer
	( cd docs/developer ; pydoc -w ../../wifiradar/*.py ; rename "" wifiradar. * ; pydoc -w ../../wifi-radar.py )

changelog:
	mkdir -p $(TOPDIR)/tmp/
	git log | perl -pi -e 's/</&lt;/g; s/>/&gt;/g; s/@/@<!-- com.com -->/g;' > $(TOPDIR)/tmp/CHANGELOG

pot-file:
	[ -d po ] || mkdir po
	pygettext.py -d wifi-radar -p po wifi-radar `find wifiradar -name "*.py"`

test:
	PYTHONPATH=. python -B ./setup.py test -s test/unit/ -p "*.py"

clean:
	rm -f  wifi-radar.localized
	rm -fr tmp/

distclean: clean

