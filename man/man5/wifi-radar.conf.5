.\" Author: Sean Robinson <robinson@tuxfamily.org>
.\" Copyright 2009,2014 by Sean Robinson
.\" Made available under the Creative Commons Attribution License 3.0
.\" -----------------------------------------------------------------
.
.TH wifi-radar.conf 5 "July 2009" "WiFi Radar 2.0"
.
.SH NAME
.
wifi-radar.conf \- configuration file for WiFi Radar
.
.SH SYNOPSIS
.
.B wifi-radar.conf
.
.SH DESCRIPTION
.
wifi-radar.conf is the configuration file for
.B WiFi Radar
.I (wifi-radar(1))
a PyGTK2 utility for managing WiFi profiles in
GNU/Linux.
.PP
The file is an ini-type format with three global sections:
.B [DEFAULT], [DHCP],
and
.B [WPA].
These are followed by
zero or more profile sections.
.PP
The settings contained in the configuration file can all be edited
from within
.B WiFi Radar,
either through the
.B Preferences button
or the
.B Edit button.
But you are free to edit the configuration file manually if you like.
.
.SH Interpolated Strings
.
It is possible to use configuration options in the value of other
options. To use interpolated strings, surround any option from the
configuration file with
.B %(
and
.B )s.
So, to use the
.I interface
option from the
.B [DEFAULT]
section in the value of any other option, use
\fB%(\fIinterface\fB)s\fR
in the option's value.
.
.SH Global Settings
.
.SS [DEFAULT]
General options.
.
.TP 4
.B auto_profile_order
Comma-separated list of profiles, surrounded by square brackets,
specifying the order of profiles in the main window.  An example value:
['test:00:00:00:00:00:00', 'WinterPalace:']
.IP
.B (default: [])
.
.TP 4
.B commit_required
Set to
.B True
if
.I iwconfig(8)
commit should be used after each iwconfig command.
.IP
.B (default: False)
.
.TP 4
.B ifconfig_command
Specify the path to
.I ifconfig(8),
the command to use when manipulating the IP settings of the NIC.
.IP
.B (default: /sbin/ifconfig)
.
.TP 4
.B ifup_required
Set to
.B True
if the NIC must be activated (i.e. ifconfig wlan0 up) before scanning
will work.
.IP
.B (default: False)
.
.TP 4
.B interface
Specifies the interface to scan. Set to the name of your NIC (e.g.
eth1 or wlan0). The special value 'auto_detect' will use the
first-found WiFi interface.
.IP
.B (default: auto_detect)

.TP 4
.B iwconfig_command
Specify the path to
.I iwconfig(8),
the command to use when trying to associate with a network.
.IP
.B (default: /sbin/iwconfig)
.
.TP 4
.B iwlist_command
Specify the path to
.I iwlist(8),
the command to use for scanning for access points.
.IP
.B (default: /sbin/iwlist)
.
.TP 4
.B logfile
Sets the location of the log file.
.IP
.B (default: /var/log/wifi-radar.log)
.
.TP 4
.B loglevel
This sets the verbosity of messages sent to the logfile.  The number
given here is a threshold value, only those messages emitted by
.B WiFi Radar
which exceed loglevel will be written to the logfile.  The maximum
(and default) value of 50 means to only save the most critical
messages and a loglevel of 0 means to save all messages to the
logfile.
.IP
.B (default: 50)
.
.TP 4
.B route_command
Specify the path to
.I route(8),
the command to use to set up network routing if DHCP is not used.
.IP
.B (default: /sbin/route)
.
.TP 4
.B speak_command
Specify the path to the command to use to speak status messages.
This can be any program which takes a string on the command line.
.IP
.B (default: /usr/bin/say)
.
.TP 4
.B speak_up
Set to
.B True
to use the speak command.
.IP
.B (default: False)
.
.TP 4
.B version
The version of
.B WiFi Radar
which created this configuration file. This value is overwritten by
.B WiFi Radar
each time the configuration file is saved.
.
.SS [DHCP]
Options for the DHCP client used by
.B WiFi Radar.
.
.TP 4
.B args
The parameters to pass to the DHCP client when acquiring a lease
(i.e. setting up a connection).
.IP
.B (default: -D -o -i dhcp_client -t %(timeout)s)
.
.TP 4
.B command
The command to use to automatically set up the IP networking.  This
can be any DHCP client, like
.I dhcpcd(8)
or
.I dhclient(8),
which forks into the background or exits when it acquires an IP address
or fails.
.IP
.B (default: /sbin/dhcpcd)
.
.TP 4
.B kill_args
The parameters to pass to the DHCP client when disconnecting from a
network.
.IP
.B (default: -k)
.
.TP 4
.B pidfile
Specify where the DHCP client saves its state info.  This file is
used if the standard DHCP disconnect does not work and
.B WiFi Radar
must kill the client on its own.
.IP
.B (default: /etc/dhcpc/dhcpcd-%(interface)s.pid)
.
.TP 4
.B timeout
The time (in seconds) to allow the DHCP client to try to acquire a
lease.  If the DHCP client does not stop itself after this length of
time plus five seconds,
.B WiFi Radar
will force the client to end.
.IP
.B (default: 30)
.
.SS [WPA]
Options for the WPA supplicant used by
.B WiFi Radar.
.
.TP 4
.B args
The parameters to pass to the WPA supplicant when associating with the
network.
.IP
.B (default: -B -i %(interface)s -c %(configuration)s -D %(driver)s -P %(pidfile)s)
.
.TP 4
.B command
Specify the command to use as the WPA supplicant.
.IP
.B (default: /usr/sbin/wpa_supplicant)
.
.TP 4
.B configuration
The WPA supplicant's configuration file.
.IP
.B (default: /etc/wpa_supplicant.conf)
.
.TP 4
.B driver
The WPA supplicant driver to use.
.IP
.B (default: wext)
.
.TP 4
.B kill_command
The command to use to end the WPA supplicant.
.IP
.B The default is an empty value.
.
.TP 4
.B pidfile
Specify where the WPA supplicant saves its state info.
.IP
.B (default: /var/run/wpa_supplicant.pid)
.
.SH Per-profile Settings
Each profile header consists of the network name (a.k.a. ESSID),
followed by a colon, and optionally followed by the network address
(a.k.a. BSSID).  The BSSID may be blank if the profile is a roaming
profile.  So a sample roaming profile section name could look like
[WinterPalace:].
.
.TP 4
.B available
Used internally by
.B WiFi Radar
to indicate whether an access point is currently detected, it should
always be False in the configuration file.
.IP
.B (default: False)
.
.TP 4
.B bssid
A copy of the BSSID in the section name, it is used to calculate the
section name.  The BSSID is the network address, it usually matches the
AP address.  It may be blank in a roaming profile.
.IP
.B (default: 00:00:00:00:00:00)
.
.TP 4
.B channel
The channel to use to connect with the network.
.IP
.B (default: auto)
.
.TP 4
.B con_postscript
The command to run after connecting to the network.
.IP
.B The default is an empty value.
.
.TP 4
.B con_prescript
The command to run before connecting to the network.
.IP
.B The default is an empty value.
.
.TP 4
.B dis_postscript
The command to run after disconnecting from the network.
.IP
.B The default is an empty value.
.
.TP 4
.B dis_prescript
The command to run before disconnecting from the network.
.IP
.B The default is an empty value.
.
.TP 4
.B dns1
The primary DNS server.  Part of the static configuration to use when
not using DHCP.
.IP
.B The default is an empty value.
.
.TP 4
.B dns2
The secondary DNS server.  Part of the static configuration to use when
not using DHCP.
.IP
.B The default is an empty value.
.
.TP 4
.B domain
The domain (e.g. winterpalace.org) of the network.  Part of the static
configuration to use when not using DHCP.
.IP
.B The default is an empty value.
.
.TP 4
.B encrypted
Whether the network is encrypted.  The value for this option is
determined by scanning and will be overwritten if changed manually in
the configuration file.
.IP
.B (default: False)
.
.TP 4
.B essid
The network name.  This is a copy of the ESSID in the section name and
is used to calculate the section name.
.IP
.B The default is an empty value.
.
.TP 4
.B gateway
The IP address of the gateway to other networks.  Part of the static
configuration to use when not using DHCP.
.IP
.B The default is an empty value.
.
.TP 4
.B ip
The fixed IP address to use on this network.  Part of the static
configuration to use when not using DHCP.
.IP
.B The default is an empty value.
.
.TP 4
.B key
The WEP encryption key.  This is not used with WPA.
.IP
.B The default is an empty value.
.
.TP 4
.B known
This is used internally by
.B WiFi Radar
to indicate whether an access point has a configured profile, it
should always be True in the configuration file.
.IP
.B (default: True)
.
.TP 4
.B mode
This is the association mode to use.  This is not the same as the mode
reported by the AP.  In fact, this should be a reflection of the AP
mode (i.e. Master mode AP should be Managed mode here).
.IP
.B (default: auto)
.
.TP 4
.B netmask
The netmask (e.g. 192.168.1.0/255) to use.  Part of the static
configuration to use when not using DHCP.
.IP
.B The default is an empty value.
.
.TP 4
.B protocol
The WiFi protocol used by the access point (AP).  This is only used as
a place to store the protocol read from the AP.  This will be
overwritten the next time the AP is scanned.
.IP
.B (default: g)
.
.TP 4
.B roaming
Set to True if this is a roaming profile.
.IP
.B (default: False)
.
.TP 4
.B security
This should/will be the security mode (i.e. open or restricted), but
as it currently causes crashes, it is not used.
.IP
.B The default is an empty value.
.
.TP 4
.B signal
The signal level read from the access point (AP).  This is used
internally by
.B WiFi Radar,
the value will be overwritten each time the configuration file is saved.
.IP
.B (default: 0)
.
.TP 4
.B use_dhcp
When set to True,
.B WiFi Radar
will ask the DHCP client to configure the IP settings.
.IP
.B (default: True)
.
.TP 4
.B use_wpa
When set to True,
.B WiFi Radar
will ask the WPA supplicant to handle associating with the access point.
.IP
.B (default: False)
.
.TP 4
.B wpa_driver
The card driver the WPA supplicant should use.
.IP
.B The default is an empty value.
.
.SH EXAMPLE
.
.nf
[DEFAULT]
auto_profile_order = ['test:00:00:00:00:00:00', 'WinterPalace:']
commit_required = False
ifconfig_command = /sbin/ifconfig
ifup_required = True
interface = auto_detect
iwconfig_command = /sbin/iwconfig
iwlist_command = /sbin/iwlist
logfile = /var/log/wifi-radar.log
loglevel = 50
route_command = /sbin/route
speak_command = /usr/bin/say
speak_up = False
version = 2.0.s02
.PP
[DHCP]
args = -D -o -i dhcp_client -t %(timeout)s
command = /sbin/dhcpcd
kill_args = -k
pidfile = /etc/dhcpc/dhcpcd-%(interface)s.pid
timeout = 30
.PP
[WPA]
args = -B -i %(interface)s -c %(configuration)s -D %(driver)s -P %(pidfile)s
command = /usr/sbin/wpa_supplicant
configuration = /etc/wpa_supplicant.conf
driver = wext
kill_command =
pidfile = /var/run/wpa_supplicant.pid
.PP
[test:00:00:00:00:00:00]
available = False
bssid = 00:00:00:00:00:00
channel = auto
con_postscript =
con_prescript =
dis_postscript =
dis_prescript =
dns1 =
dns2 =
domain =
encrypted = False
essid = test
gateway =
ip =
key =
known = True
mode = auto
netmask =
protocol = g
roaming = False
security =
signal = 0
use_dhcp = True
use_wpa = False
wpa_driver =
.PP
[WinterPalace:]
available = False
bssid =
channel = auto
con_postscript =
con_prescript =
dis_postscript =
dis_prescript =
dns1 =
dns2 =
domain =
encrypted = True
essid = WinterPalace
gateway =
ip =
key = 123456789ABCDEF
known = True
mode = auto
netmask =
protocol = g
roaming = True
security =
signal = 0
use_dhcp = True
use_wpa = False
wpa_driver =
.
.SH FILES
.
.TP 8
.B /etc/wifi-radar/wifi-radar.conf
.
.SH BUGS
.
Probably lots!
.
.SH SEE ALSO
.
.I wifi-radar(1)
.I wpa_supplicant.conf(5)
.
