How To Report A Bug
===================

If you think you've discovered a bug, please read through these guidelines
before reporting it.

First, make sure it's a new bug:

* Are you using the very latest version of WiFi Radar?  The bug may have
  already been fixed.  Please get the latest version of WiFi Radar from
  the web site and check again.  Even if your bug has not been fixed,
  others probably have, and you're better off with the most up-to-date code.

  If you don't have time to check the latest version, please report the bug
  anyway.  I'd rather tell you that it's already fixed than miss reports of
  unfixed bugs.

* If WiFi Radar does not behave the way you expect, look in the documentation
  (don't forget the FAQ) and, especially, the mailing list archives for
  evidence that it should behave the way you expect.

If you're not sure, please ask on the WiFi Radar mailing list first.

If it's a new bug, the most important thing you can do is to write a simple
description and a recipe that reproduces the bug.  The easier you make it to
understand and track down the bug, the more likely a fix will be.

Now you're ready to write the bug report.  Please include:

* A clear description of the bug.  Describe how you expected WiFi Radar to
  behave, and contrast that with how it actually behaved.  While the bug may
  seem obvious to you, it may not be so obvious to someone else, so it's best
  to avoid a guessing game.

* A complete description of the environment in which you reproduced the bug:

  - Your operating system & version.
  - The version of Python ("python -V").
  - The version of WiFi Radar (use the "-v" option).
  - Decrease the logging threshold to 0 and send the resulting wifi-radar.log
    file.
  - Any private modifications you made to WiFi Radar.
  - Anything else that could possibly be relevant.  Err on the side of too
    much information, rather than too little.

* A literal transcript of the *exact* command you ran, and the *exact* output.

* If you also want to include speculation as to the cause, and even a patch to
  fix the bug, that would be great!

--

This document was blatantly stolen from Docutils 0.5.  The original version
was written by David Goodger <goodger@python.org> and placed in the public
domain.  This version, edited by Sean Robinson, is licensed under the
Creative Commons Attribution License.  Please attribute both authors if you
re-use it.
