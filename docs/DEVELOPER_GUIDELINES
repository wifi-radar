#
#   WiFi Radar
#
#   DEVELOPER_GUIDELINES 2014-05-19
#

Try to mimick the style of the code that you are working in.

Include a usage docstring with every function/method, no matter how
simple and self-explanatory that function is.  Example:

    def make_section_name(essid, bssid):
        """ Returns the combined 'essid' and 'bssid' to make a config file
            section name.  'essid' and 'bssid' are strings.
        """
        return essid + ':' + bssid

Always give people credit in the CREDITS file.

Have fun!


--------------------------------------------------------------------------------
GIT
--------------------------------------------------------------------------------
The current development branch is "master".  Unless you're just fixing a bug
in the stable release, or updating the stable documentation, this is what
you want.

  Primary repo: git://gitorious.org/wifi-radar/wifi-radar.git
  Repo mirror: git://repo.or.cz/wifi-radar.git


--------------------------------------------------------------------------------
Tests
--------------------------------------------------------------------------------
To run the available tests against WiFi Radar, use:

$ make test

from the source root directory.

Currently, only unit tests, and only for some of the source code, are
ready.


--------------------------------------------------------------------------------
Submitting Patches
--------------------------------------------------------------------------------
Please submit patches through email or request a git pull from a public
repository.


--------------------------------------------------------------------------------
Version Numbering
--------------------------------------------------------------------------------
WiFI Radar uses a three stage version numbering (X.Y.Z):

    * X = major architectural change
    * Y = feature release
      no special meaning is given to an odd or even number in feature releases
    * Z = sub-revision: [abs]nn
          o [abs] = alpha, beta, or stable
          o nn  : 2-digit serial number
          o ann : no stability guarantees between releases
          o bnn : new features are possible, but less wild than alpha
          o snn : bug fixes only, no non-bug-related development will occur
                  in this line

e.g. 2.0.b01 is the first beta release for 2.0; 2.0.s05 is the fifth stable
release for 2.0; 2.3.s10 is the tenth stable release for 2.3
